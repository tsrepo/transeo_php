<?php

use Transeo\Helpers\Arrays;

class ArraysTest extends \PHPUnit\Framework\TestCase
{
    public function test_filterKeys()
    {
        $array = ['a' => 1, 'b' => 2, 'c' => 3];
        $allowed = ['a', 'c', 'd'];

        $result = Arrays::filterKeys($array, $allowed);

        $this->assertSame(['a' => 1, 'c' => 3], $result);
    }

    public function test_addQuotes_notArray_returnsEmptyArray()
    {
        $result = Arrays::addQuotes('not an array');

        $this->assertSame([], $result);
    }

    public function test_addQuotes_validArray_returnsArrayWithItemsWrappedInSingleQuotes()
    {
        $result = Arrays::addQuotes(['a', 1, 'b']);

        $this->assertEquals(["'a'", "'1'", "'b'"], $result);
    }

    public function test_toArrayOfObject_notAnArray_returnsEmptyArray()
    {
        $result = Arrays::toArrayOfObject('not an array', 'property');

        $this->assertSame([], $result);
    }

    public function test_toArrayOfObject_validArray_returnsArrayOfObjects()
    {
        $array = [
            ['prop1' => 'value1', 'prop2' => 'value2'],
            ['prop3' => 'value3', 'prop4' => 'value4']
        ];

        $result = Arrays::toArrayOfObject($array, 'property');

        $expected = [];
        $expected[] = new STDClass();
        $expected[] = new STDClass();

        $expected[0]->property = ['prop1' => 'value1', 'prop2' => 'value2'];
        $expected[1]->property = ['prop3' => 'value3', 'prop4' => 'value4'];

        $this->assertSame(2, count($result));
        $this->assertEquals($expected[0], $result[0]);
        $this->assertEquals($expected[1], $result[1]);
    }

    public function test_changeKey_keyDoesNotExist_returnsOriginalArray()
    {
        $array = ['a' => 1, 'b' => 2];
        
        $result = Arrays::changeKey($array, 'd', 'c');

        $this->assertSame(['a' => 1, 'b' => 2], $result);
    }

    public function test_changeKey_keyExists_returnsArrayWithKeyRenamed()
    {
        $array = ['a' => 1, 'b' => 2];
        
        $result = Arrays::changeKey($array, 'b', 'c');

        $this->assertSame(['a' => 1, 'c' => 2], $result);
    }

    public function test_rekeySingle_notAnArray_returnsEmptyArray()
    {
        $result = Arrays::rekeySingle('not an array');

        $this->assertSame([], $result);
    }

    public function test_rekeySingle_validArray_returnsArrayRekeyedWithValues()
    {
        $array = ['key1' => 'value1', 'key2' => 'value2'];

        $result = Arrays::rekeySingle($array);

        $this->assertSame(['value1' => 'value1', 'value2' => 'value2'], $result);
    }

    public function test_hasInterSect_arraysIntersect_returnsTrue()
    {
        $array1 = array('a' => 'green', 'red', 'blue');
        $array2 = array('b' => 'green', 'yellow', 'red');

        $result = Arrays::hasIntersect($array1, $array2);

        $this->assertTrue($result);
    }

    public function test_hasInterSect_arraysDoNotIntersect_returnsFalse()
    {
        $array1 = array('a' => 'puple', 'red', 'blue');
        $array2 = array('b' => 'green', 'yellow');

        $result = Arrays::hasIntersect($array1, $array2);

        $this->assertFalse($result);
    }

    public function test_arrayKeysExist_allKeysMatch_returnsTrue()
    {
        $keys = array('a', 'b');
        $array = array('a' => 11, 'b' => 12);

        $result = Arrays::arrayKeysExist($keys, $array);

        $this->assertTrue($result);
    }

    public function test_arrayKeysExist_notAllKeysMatch_returnsFalse()
    {
        $keys = array('a', 'b', 'c');
        $array = array('a' => 11, 'b' => 12);

        $result = Arrays::arrayKeysExist($keys, $array);

        $this->assertFalse($result);
    }
}
