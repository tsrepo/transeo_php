<?php

use Transeo\Helpers\ArrayDatasets;

class ArrayDatasetsTest extends \PHPUnit\Framework\TestCase
{
    public function test_sum_notAnArray_returnsZero()
    {
        $array = 'a string, not an array';

        $result = ArrayDatasets::sum($array, 'a');

        $this->assertSame(0, $result);
    }

    public function test_sum_array_returnsSum()
    {
        $array = [
            [ 'a' => 10, 'b' => 100],
            [ 'b' => 200],
            [ 'a' => 40, 'b' => 300],
            [ 'a' => 20, 'b' => 400]
        ];

        $result = ArrayDatasets::sum($array, 'a');

        $this->assertSame(70, $result);
    }

    public function test_max_notAnArray_returnsNull()
    {
        $array = 'a string, not an array';

        $result = ArrayDatasets::max($array, 'a');

        $this->assertSame(NULL, $result);
    }

    public function test_max_array_returnsMax()
    {
        $array = [
            [ 'a' => 10, 'b' => 100],
            [ 'b' => 200],
            [ 'a' => 40, 'b' => 300],
            [ 'a' => 20, 'b' => 400]
        ];

        $result = ArrayDatasets::max($array, 'a');

        $this->assertSame(40, $result);
    }

    public function test_min_notAnArray_returnsNull()
    {
        $array = 'a string, not an array';

        $result = ArrayDatasets::min($array, 'a');

        $this->assertSame(NULL, $result);
    }

    public function test_min_array_returnsMin()
    {
        $array = [
            [ 'a' => 40, 'b' => 100],
            [ 'b' => 200],
            [ 'a' => 10, 'b' => 300],
            [ 'a' => 20, 'b' => 400]
        ];

        $result = ArrayDatasets::min($array, 'a');

        $this->assertSame(10, $result);
    }
}
