<?php

use Transeo\Helpers\Filesystem;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\visitor\vfsStreamPrintVisitor;

class FilesystemTest extends \PHPUnit\Framework\TestCase
{
    public function test_recursiveDelete_directoryWithFilesAndSubdirectories_deletesDirectory()
    {
        $root = vfsStream::setup('root');
        
        vfsStream::create([
            'testDirectory' => [
                'testFile.txt' => 'some content',
                'testSubdirectory' => [
                    'testFileTwo.txt' => 'some other content'
                ]
            ]
        ]);

        // debug code to view folder structure, run phpunit w/ --debug
        // vfsStream::inspect(new vfsStreamPrintVisitor());

        // just to make sure our mock filesystem is setup as intended
        $this->assertTrue(is_dir(vfsStream::url('root/testDirectory')));
        $this->assertTrue(is_dir(vfsStream::url('root/testDirectory/testSubdirectory')));
        $this->assertTrue(is_file(vfsStream::url('root/testDirectory/testFile.txt')));
        $this->assertTrue(is_file(vfsStream::url('root/testDirectory/testSubdirectory/testFileTwo.txt')));

        $result = Filesystem::recursiveDelete(vfsStream::url('root/testDirectory'));

        $this->assertTrue($result);
        $this->assertFalse(is_dir(vfsStream::url('root/testDirectory')));
    }

    public function test_recursiveDelete_nonExistantDirectory_returnsFalse()
    {
        $root = vfsStream::setup('root');
        
        vfsStream::create([
            'testDirectory' => [
                'testFile.txt' => 'some content',
                'testSubdirectory' => [
                    'testFileTwo.txt' => 'some other content'
                ]
            ]
        ]);

        // debug code to view folder structure, run phpunit w/ --debug
        // vfsStream::inspect(new vfsStreamPrintVisitor());

        // just to make sure our mock filesystem is setup as intended
        $this->assertTrue(is_dir(vfsStream::url('root/testDirectory')));
        $this->assertTrue(is_dir(vfsStream::url('root/testDirectory/testSubdirectory')));
        $this->assertTrue(is_file(vfsStream::url('root/testDirectory/testFile.txt')));
        $this->assertTrue(is_file(vfsStream::url('root/testDirectory/testSubdirectory/testFileTwo.txt')));

        $result = Filesystem::recursiveDelete(vfsStream::url('root/doesNotExist'));

        $this->assertFalse($result);

        // make sure nothing got deleted
        $this->assertTrue(is_dir(vfsStream::url('root/testDirectory')));
        $this->assertTrue(is_dir(vfsStream::url('root/testDirectory/testSubdirectory')));
        $this->assertTrue(is_file(vfsStream::url('root/testDirectory/testFile.txt')));
        $this->assertTrue(is_file(vfsStream::url('root/testDirectory/testSubdirectory/testFileTwo.txt')));
    }
}
