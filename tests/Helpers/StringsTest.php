<?php

use Transeo\Helpers\Strings;

class StringsTest extends \PHPUnit\Framework\TestCase
{
    public function test_randomKey_returnsStringOfProperLength()
    {
        $result = Strings::randomKey();

        $this->assertSame(64, strlen($result));
    }

    public function test_getStringBetween_validDelimiters_returnsStringBetweenDelimiters()
    {
        $result = Strings::getStringBetween('some,more;text', ',', ';');

        $this->assertSame('more', $result);
    }

    public function test_getStringBetween_firstDelimiterIsSpace_returnsEmptyString()
    {
        $result = Strings::getStringBetween('some,more;text', ' ', ';');

         $this->assertSame('', $result);
    }

    public function test_getStringBefore_singleCharacterDelimiter_returnsTextBeforeDelimiter()
    {
        $result = Strings::getStringBefore('some ,more,text', ',');

        $this->assertSame('some ', $result);
    }

    public function test_getStringBefore_multiCharacterDelimiter_returnsTextBeforeDelimiter()
    {
        $result = Strings::getStringBefore('some ,more,text', ' ,');

        $this->assertSame('some', $result);
    }

    public function test_getStringBefore_delimiterIsFirstCharacter_returnsEmptyString()
    {
        $result = Strings::getStringBefore(',more,text', ',');

        $this->assertSame('', $result);
    }

    public function test_getStringBefore_delimiterDoesNotExist_returnsEmptyString()
    {
        $result = Strings::getStringBefore('some,text', ';');

        $this->assertSame('', $result);
    }

    public function test_possessive_wordNotEndingWithLetterS_returnsWordEndingWithApostropheAndLetterS()
    {
        $result = Strings::possessive('Word');

        $this->assertSame('Word\'s', $result);
    }

    public function test_possessive_wordEndingWithLetterS_returnsWordEndingWithApostrophe()
    {
        $result = Strings::possessive('Words');

        $this->assertSame('Words\'', $result);
    }

    public function test_possessive_uppercaseWordNotEndingWithLetterS_returnsUppercasedWordEndingWithApostrophe()
    {
        $result = Strings::possessive('WORD', TRUE);

        $this->assertSame('WORD\'S', $result);
    }
}
