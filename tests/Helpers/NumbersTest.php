<?php

use Transeo\Helpers\Numbers;

class NumbersTest extends \PHPUnit\Framework\TestCase
{
    public function test_ordinalIndicatorWords_validIntegers_returnsLowercaseOrdinalStrings()
    {
        $this->assertEquals('first', Numbers::ordinalIndicatorWords(1));
        $this->assertEquals('third', Numbers::ordinalIndicatorWords(3));
        $this->assertEquals('tenth', Numbers::ordinalIndicatorWords(10));
        $this->assertEquals('twenty-first', Numbers::ordinalIndicatorWords(21));
    }

    public function test_randomDigits_validLength_returnsRandomStringOfDesiredLength()
    {
        $result = Numbers::randomDigits(5);

        $this->assertSame(5, strlen($result));
        $this->assertTrue(is_numeric($result));
    }

    public function test_randomDigits_noLength_returnsRandomStringOfDefaultLength()
    {
        $result = Numbers::randomDigits();

        $this->assertSame(10, strlen($result));
        $this->assertTrue(is_numeric($result));
    }

    public function test_randomDigits_zeroLength_returnsEmptyString()
    {
        $result = Numbers::randomDigits(0);

        $this->assertSame(0, strlen($result));
        $this->assertSame('', $result);
    }

    public function test_randomDigits_negativeLength_returnsEmptyString()
    {
        $result = Numbers::randomDigits(-1);

        $this->assertSame(0, strlen($result));
        $this->assertSame('', $result);
    }
}
