<?php

use Transeo\Helpers\Colors;

class ColorsTest extends \PHPUnit\Framework\TestCase
{
    public function test_hexToRGB_hexStringWithHash_returnsRGBArray()
    {
        $result = Colors::hexToRGB('#BADA55');

        $this->assertSame([186, 218, 85], $result);
    }

    public function test_hexToRGB_hexStringWithoutHash_returnsRGBArray()
    {
        $result = Colors::hexToRGB('BADA55');

        $this->assertSame([186, 218, 85], $result);
    }

    public function test_hexToRGB_shorthandHexString_returnsRGBArray()
    {
        $result = Colors::hexToRGB('ABC');

        $this->assertSame([170, 187, 204], $result);
    }

    public function test_rgbToHex_validRGBArray_returnsHexString()
    {
        $result = Colors::rgbToHex([186, 218, 85]);

        $this->assertSame('#bada55', $result);
    }
}
