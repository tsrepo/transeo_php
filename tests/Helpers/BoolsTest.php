<?php

use Transeo\Helpers\Bools;

class BoolsTest extends \PHPUnit\Framework\TestCase
{
    public function test_toYesNo_true_returnsYes()
    {
        $this->assertSame('yes', Bools::toYesNo(TRUE));
    }

    public function test_toYesNo_false_returnsNo()
    {
        $this->assertSame('no', Bools::toYesNo(FALSE));
    }
}
