<?php

use Transeo\Integrations\ZipTax;
use Transeo\Loggers\EchoLogger;

class ZipTaxTest extends \PHPUnit\Framework\TestCase
{
    public function test_getTaxes_validZip_returnsTaxes()
    {
        $calc = new ZipTax([
            'api_url' => getenv('ZipTax_Url'),
            'api_key' => getenv('ZipTax_Key')
        ]);
        
        // uncomment if you wanna see the log statements in phpunit output
        //$calc->setLogger(new EchoLogger());

        $result = $calc->getTaxes(96734);

        $this->assertSame(0.045, $result['total_sales']);
        $this->assertSame('KAILUA', $result['city']);
    }

    public function test_getTaxes_invalidUrl_returnsFalse()
    {
        $calc = new ZipTax([
            'api_url' => 'http://nonexistantdomainasd1231.name',
            'api_key' => getenv('ZipTax_Key')
        ]);

        // uncomment if you wanna see the log statements in phpunit output
        //$calc->setLogger(new EchoLogger());

        $result = $calc->getTaxes(96734);

        $this->assertFalse($result);
    }

    public function test_getTaxes_emptyApiKey_returnsFalse()
    {
        $calc = new ZipTax([
            'api_url' => getenv('ZipTax_Url'),
            'api_key' => ''
        ]);

        // uncomment if you wanna see the log statements in phpunit output
        //$calc->setLogger(new EchoLogger());

        $result = $calc->getTaxes(96734);

        $this->assertFalse($result);
    }
}
