<?php

// needed because every file we want to test has "defined('BASEPATH') or exit()"
define('BASEPATH', __DIR__ . '/../');

// use composer's autoloader for remaining stuff
require_once __DIR__ . '/../vendor/autoload.php';
