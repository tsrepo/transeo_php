<?php

namespace Transeo\Loggers;

class EchoLogger extends BaseLogger
{
    public function log($level, $message, array $context = array())
    {
        if (\Transeo\Helpers\Http::isCLI()) {
            return $this->cli_log($level, $message, $context);
        }

        self::echo($level, $this->interpolate($message, $context));
    }

    public function cli_log($level, $message, array $context = array())
    {
        if ($level === 'progress') {
            return $this->cli_progress($level, $message);
        }

        echo sprintf('[' . gethostname() . '] [' . getmypid() . '] [%s] %s' . PHP_EOL, $level, $this->interpolate($message, $context));
        flush();
        if (ob_get_level() > 0) {
            ob_flush();
        }
    }

    public function cli_progress($level, $message, $length = 100)
    {
        // echo "\033";
        echo "\r";
        echo str_pad(sprintf('[%s] %s', $level, substr($message, 0, 100)), 100, ' ', STR_PAD_RIGHT);
        echo "\r";
        flush();
        if (ob_get_level() > 0) {
            ob_flush();
        }

    }

    public static function echo($level, $message)
    {
        // super hacky way to exclude it in json responses, too
        $isjson = (isset($_SERVER['HTTP_ACCEPT']) && strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== FALSE);
        if ($isjson) {
            return;
        }
        
        // hide messages unless in dev
        if (ENVIRONMENT !== 'development' && !(defined('DEBUG') && DEBUG === TRUE)) {
            return;
        }

        // if its an info-progress message, show a dot unless we're in dev
        if ($level === 'info-progress') {
            echo '.';    
        }
        else {
            echo sprintf('[%s] %s' . PHP_EOL, $level, $message);
        }
    }
}
