<?php

namespace Transeo\Loggers;

trait ApiLoggerAwareTrait
{
    protected $api_logger;

    public function setApiLogger(ApiLoggerInterface $logger)
    {
        $this->api_logger = $logger;
    }
}
