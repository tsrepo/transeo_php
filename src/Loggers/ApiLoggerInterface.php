<?php

namespace Transeo\Loggers;

/**
 * Abstraction to log external API calls (request/response)
 */
interface ApiLoggerInterface
{
    public function log($api_name, $request, $response);
}
