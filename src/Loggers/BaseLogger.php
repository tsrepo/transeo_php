<?php

namespace Transeo\Loggers;

abstract class BaseLogger extends \Psr\Log\AbstractLogger
{
    /**
     * Interpolates context values into the message placeholders.
     *
     * Reference implementation from PSR-3 spec
     *
     * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
     *
     * @param   string  $message
     * @param   array   $context    Optional, associative array of context values
     *
     * @return  string  $message w/ $context values replaced
     */
    protected function interpolate($message, array $context = array())
    {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            // check that the value can be casted to string
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }
}
