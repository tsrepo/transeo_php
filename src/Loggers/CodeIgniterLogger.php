<?php

namespace Transeo\Loggers;

class CodeIgniterLogger extends BaseLogger
{
    public function log($level, $message, array $context = array())
    {
        log_message($level, $this->interpolate($message, $context));
    }
}
