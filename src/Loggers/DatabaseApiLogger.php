<?php

namespace Transeo\Loggers;

use Transeo\CodeIgniter\Model;

class DatabaseApiLogger implements ApiLoggerInterface
{
    public function __construct()
    {
        $this->load->model('api_logs_model');
    }

    // Allows access CI's loaded classes using the same syntax as controllers.
    // e.g. $this->load->model('') will work now
    public function __get($key)
    {
        return get_instance()->$key;
    }

    public function log($api_name, $request, $response)
    {
        $this->api_logs_model->log($api_name, $request, $response);
    }
}
