<?php

namespace Transeo\Loggers;

class NullApiLogger implements ApiLoggerInterface
{
    public function log($api_name, $request, $response)
    {
        // do nothing
    }
}
