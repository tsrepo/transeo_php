<?php

namespace Transeo\Helpers;

class Filesystem
{
    /**
     * Delete a file or recursively delete a directory
     *
     * @param   string  $str    Path to file or directory
     *
     * @return  boolean         Returns true if the file or directory was deleted
     */
    public static function recursiveDelete($str)
    {
        if (is_file($str)) {
            return @unlink($str);
        }

        if (is_dir($str)) {
            $scan = self::streamSafeGlob(rtrim($str, '/'), '*');

            foreach ($scan as $index => $path) {
                self::recursiveDelete($path);
            }

            return @rmdir($str);
        }

        return FALSE;
    }

    /**
     * Glob that is safe with streams (vfs for example)
     *
     * @param   string  $directory      Directory to search in
     * @param   string  $file_pattern   Pattern to match
     *
     * @return  array                   Array of filenames in $directory matching $file_pattern
     */
    public static function streamSafeGlob($directory, $file_pattern)
    {
        $files = scandir($directory);
        $found = [];

        foreach ($files as $filename) {
            if (in_array($filename, ['.', '..'])) {
                continue;
            }

            if (fnmatch($file_pattern, $filename)) {
                $found[] = "{$directory}/{$filename}";
            }
        }

        return $found;
    }

    /**
     * Get a name to a new temp file
     *
     * @return  string  The name
     *
     */
    public static function fileGetTempName()
    {
        $fs = stream_get_meta_data(tmpfile());
        return $fs['uri'];
    }

    /**
     * Remove lines from top of file that start with a string
     *
     * @param   string  $filename       Filename/path of the file
     * @param   array   $remove         Array of strings
     *
     */
    public static function fileRemoveTopLinesStartingWith($filename, $remove)
    {
        $tmp = self::fileGetTempName();

        $new_file = new \SplFileObject($tmp, 'w');

        foreach (new \SplFileObject($filename) as $line) {
            $write = true;
            foreach ($remove as $ex) {
                if (strpos($line, $ex) === 0) $write = false;
            }
            if ($write) $new_file->fwrite($line);
        }

        copy($tmp, realpath($filename));
    }

    /**
     * Remove lines from top of file that are blank
     *
     * @param   string  $filename       Filename/path of the file
     *
     */
    public static function fileRemoveTopLinesBlank($filename)
    {
        $tmp = self::fileGetTempName();

        $new_file = new \SplFileObject($tmp, 'w');

        $start = false;
        foreach (new \SplFileObject($filename) as $line) {
            if (!$start && preg_replace('/\s/','',$line) == '') {
                continue;
            }
            else {
                $start = true;
            }
            $new_file->fwrite($line);
        }

        copy($tmp, realpath($filename));
    }


    /**
     * Returns a "page" of lines from a file
     *
     * @param   string  $file           Path to file
     * @param   int     $page           Page numer to retrieve (starts from bottom of file)
     * @param   int     &$total_pages   Reference variable used to return total number of pages in file
     * @param   int     $lines_per_page How many lines to return per page
     *
     * @return string[]                 An array of strings containing the desired lines from the file
     */
    public static function getPagedLines($file, $page = 1, &$total_pages = NULL, $lines_per_page = 20)
    {
        $file = new \SplFileObject($file);
        
        // move to end of document so we can calculate how many lines are in it
        $file->seek($file->getSize());
        $lines = $file->key() - 1;

        // by default (e.g. file is small), read entire thing
        $start = 0;
        $end = $lines;

        // calc # of pages for UI
        $total_pages = ceil($lines / $lines_per_page);

        // file is larger than how many lines we want, do some paging
        if ($lines > $lines_per_page) {
            $end = $lines - (($page - 1) * $lines_per_page);
            $start = $end - $lines_per_page + 1;

            // don't go beyond the beginning of file
            if ($start < 0) {
                $start = 0;
            }
        }

        $file->seek($start);

        $lines = [];

        while ($start++ <= $end) {
            try {
                $line = $file->fgets();
                $lines[] = $line;
            } catch (\Exception $ex) {
                echo $ex->getMessage();
                die();
            }
        }

        return $lines;
    }
}
