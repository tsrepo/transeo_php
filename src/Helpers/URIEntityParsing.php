<?php

namespace Transeo\Helpers;

defined('BASEPATH') or exit('No direct script access allowed');

class URIEntityParsing
{
    /**
    * Maximum number of records on a page
    */
    const PAGE_THRESHOLD = 10000;
    const PAGE_DEFAULT = 50;

    /**
    * The number of results that will be returned.
    * 
    * @var  int
    */
    public $limit = self::PAGE_DEFAULT;

    /**
    * The record to start at when paging.
    * @var  int
    */
    public $offset = 0;

    /**
    * The fields to be returned in the resultset.  Should be a comma-delimited list of fields and must correlate to
    * the object type being queried.
    * @var      string
    * @default  All fields for the object type being queried will be returned.
    */
    public $fields = '*';
    
    /**
    * The fields to be grouped by.  Should be a comma-delimited list of fields and must correlate to
    * the object type being queried.
    * @var      string
    * @default  All fields for the object type being grouped will be returned.
    */
    public $group = [];

    /**
    * The fields to be oordered by.  Should be a comma-delimited list of fields and must correlate to
    * the object type being queried.  An optional " DESC" can be added if the field should be ordered descending.
    * @var      string
    * @default  All field orders for the object type being queried will be returned.
    */
    public $order = [];
    
    /**
    * The linked entities to include in the resultset.  Should be a comma-delimited list of entities.
    * @var      string
    * @default  None
    */
    public $expand = [];

    /**
    * The included / composite entities to include in the resultset.  Should be a comma-delimited list of entities.
    * @var      string
    * @default  None
    */
    public $include = [];
    
    /**
    * A structured set of comma-delimited filters for the data being requested.
    * The filter structure follows {fieldname}.operator(value) and can be chained together using commas.
    * Supported operators are in the OPERATORS attribute
    * Examples:
    *   - order_id.eq(12345) will filter the results to only rows where order_id = 12345
    *   - invoice_time.gt('2017-06-01') will filter the results to only records with an invoice_time >= 6/1/2016
    *   - invoice_time.gt('2017-06-01'),invoice_time.lt('2017-07-01') will filter the results to only records between 6/1/2017 and 7/1/2017 (inclusive)
    * @var      string
    * @default  No filters
    */
    public $filters = [];

    /**
    * Custom parameters to modify the query.  Should be a comma-delimited list.
    * @var      string
    * @default  None
    */
    public $adjustments = [];

    /**
    * The version of the API being requested.
    * @var      string
    * @default  1
    */
    public $version = 1;
    
    /**
    * The format of returned data: ES, JSON
    * @var      string
    * @default  json
    */
    public $format = 'json';
    

    protected static function mapOperatorToDatabase($operator)
    {
        // support null* for any of these, which will
        // force an IFNULL on the field
        if (substr($operator, 0, 4) == 'null') {
            $operator = self::mapOperatorToDatabase(substr($operator, 4));
            
            if (!empty($operator)) {
                return 'null' . $operator;
            }
        }

        switch ($operator) {
            case 'eq':
                return '=';
            case 'neq':
                return '!=';
            case 'lte':
                return '<=';
            case 'lt':
                return '<';
            case 'gte':
                return '>=';
            case 'gt':
                return '>';
            case 'in':
                return 'in';
            case 'like':
                return 'like';
            case 'any':
                return 'or_like';
            case 'oeq':
                return 'or_where';
        }

        return FALSE;
    }

    public function setFromInputArray($input_data)
    {
        if (!empty($input_data['limit'])) {
            $this->limit = $input_data['limit'];
        }
        if (!empty($input_data['offset'])) {
            $this->offset = $input_data['offset'];
        }
        if (!empty($input_data['fields'])) {
            $this->fields = explode(',', $input_data['fields']);
        }
        if (!empty($input_data['group'])) {
            $this->group = explode(',', $input_data['group']);
        }
        if (!empty($input_data['order'])) {
            $this->order = explode(',', $input_data['order']);
        }
        if (!empty($input_data['expand'])) {
            $this->expand = explode(',', $input_data['expand']);
        }
        if (!empty($input_data['include'])) {
            $this->include = explode(',', $input_data['include']);
        }
        if (!empty($input_data['adjustments'])) {
            $this->adjustments = explode(',', $input_data['adjustments']);
        }
        if (!empty($input_data['version'])) {
            $this->version = $input_data['version'];
        }
         
        if (!empty($input_data['format'])) {
            $this->format = strtolower($input_data['format']);
        }
         
        if ($this->limit > self::PAGE_THRESHOLD) {
            throw new \Exception('Maximum results per page exceeded; pages must limited to ' . self::PAGE_THRESHOLD . ' results');
        }

        $this->setFilterParams($input_data['filters'] ?? []);
    }

    protected function setFilterParams($filters)
    {
        if (empty($filters)) {
            $this->filters = [];
            return;
        }

        $fields = [];

        // preg_match_all('/\.(eq|gt|lt)(?<=\()(.*?)(?=\))/', $params['fields'], $fields);
        $filters = str_replace('\)', '&eparen;', $filters);
        $filters = str_replace('\(', '&bparen;', $filters);
        preg_match_all('/(?:(((\w*?)(\.)?)(\w*?))(?:\.)(\w{2,8}))\((.*?)(?=\))/', $filters, $fields);

        $field_params = count($fields)-1;

        if ($field_params < 2) {
            throw new \Exception('Error parsing field selector');
        }

        // idx
        // 1 is the field name
        // $field_params-1 is the operator
        // $field_params is the value        

        if (sizeof($fields[1]) !== sizeof($fields[$field_params-1]) || sizeof($fields[$field_params-1]) !== sizeof($fields[$field_params])) {
            throw new \Exception('Error parsing field selector');
        }

        $filters = [];
        foreach ($fields[$field_params-1] as $idx => $field) {
            if (empty(self::mapOperatorToDatabase($fields[$field_params-1][$idx]))) {
                throw new \Exception('Unknown operator ' . $fields[$field_params-1][$idx]);
            }

            $value = str_replace('&eparen;', ')', $fields[$field_params][$idx]);
            $value = str_replace('&bparen;', '(', $value);

            $filters[] = [
                'field'     => $fields[1][$idx],
                'operator'  => self::mapOperatorToDatabase($fields[$field_params-1][$idx]),
                'value'     => $value,
            ];
        }

        $this->filters = $filters;
    }

    public function toArray()
    {
        return [
            'limit'         => $this->limit,
            'offset'        => $this->offset,
            'fields'        => $this->fields,
            'filters'       => $this->filters,
            'group'         => $this->group,
            'order'         => $this->order,
            'expand'        => $this->expand,
            'include'       => $this->include,
            'adjustments'   => $this->adjustments,
            'format'        => $this->format,
            'version'       => $this->version,
        ];
    }
}
