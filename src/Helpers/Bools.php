<?php

namespace Transeo\Helpers;

class Bools
{
    /**
     * To yes or no
     *
     * @param   bool    $bool       The boolean
     *
     * @return  string              Yes or No
     */
    public static function toYesNo($bool)
    {
        return ($bool) ? 'yes' : 'no';
    }
}
