<?php

namespace Transeo\Helpers;

class Http
{
    /**
     * Identify if this is a CLI request
     *
     * @return  bool              	Whether or not this is a CLI request
     */
    public static function isCLI()
    {
        return PHP_SAPI === 'cli' || !isset($_SERVER['HTTP_HOST']);
    }
}
