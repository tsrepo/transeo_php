<?php

namespace Transeo\Helpers;

class Numbers
{
    /**
     * Returns the english ordinal word representation of a given integer
     *
     * Examples: 1 => 'first', 3 => 'third', 21 => 'twenty-first'
     *
     * @param   int     $number     A number
     *
     * @return  string              The lowercased english ordinal word representation of the integer specified
     */
    public static function ordinalIndicatorWords($number)
    {
        // NumberFormatter adds dependency on the PHP extension php-intl >= 1.0.0
        $formatter = new \NumberFormatter('en_US', \NumberFormatter::SPELLOUT);
        $formatter->setTextAttribute(\NumberFormatter::DEFAULT_RULESET, '%spellout-ordinal');
        return $formatter->format($number);
    }

    /**
     * Returns a random string of numbers
     *
     * @param   int     $length     (optional) Desired string length
     *
     * @return  string              Random string of numbers
     */
    public static function randomDigits($length = 10)
    {
        return Strings::randomString($length, '0123456789');
    }

    /**
     * Returns a number in a nice currency format
     *
     * @param   decimal $number         The number to format
     * @param   bool    $display_zeros  To display the zero vs. a dash if the number is zero
     * @param   int     $precision      The number of decimals to display
     *
     * @return  string              Random string of numbers
     */
    public static function toCurrency($number, $display_zero = FALSE, $precision = 2)
    {
        $_html = '$';
        if ($number > 0) {
            $_html .= number_format($number, $precision);
        }
        elseif ($number < 0) {
            $_html .= '(' . number_format(abs($number), $precision) . ')';
        }
        else {
            $_html = $display_zero ? ($_html . '0') : '-';
        }

        return $_html;
    }

    /**
     * Format bytes in a user-friendly way
     *
     * @param   int     $bytes      Number of bytes
     * @param   int     $decimals   Number of digits after the decimal to display
     *
     * @return  string              User friendly formated string
     */
    public static function formatBytes($bytes, $decimals = 2)
    {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    }
}
