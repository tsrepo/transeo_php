<?php

namespace Transeo\Helpers;

class Arrays
{
    /**
     * Filters the keys of a single-dimensional array to those in the allowed
     *
     * @param   array   $array      An array
     * @param   array   $allowed    An array of allowed keys
     *
     * @return  array               Returns $array with any keys NOT in $allowed removed
     */
    public static function filterKeys($array, $allowed)
    {
        return array_intersect_key($array, array_flip($allowed));
    }

    /**
     * Filters the keys of an associative array to those in the allowed
     *
     * @param   array   $array      An array
     * @param   array   $allowed    An array of allowed keys
     *
     * @return  array               Returns $array with any keys NOT in $allowed removed
     */
    public static function filterAssociativeKeys($array, $allowed)
    {
        foreach ($array as $key => $val) {
            if (!in_array($key, $allowed)) {
                unset($array[$key]);
            }
        }
        
        return $array;
    }

    /**
     * Adds single quotes around all elements of the array
     *
     * @param   array   $array  An array
     *
     * @return  array           An array of quoted elements
     */
    public static function addQuotes($array)
    {
        if (!is_array($array)) {
            return [];
        }

        return array_map(
            function ($str) {
                return sprintf("'%s'", $str);
            },
            $array
        );
    }

    /**
     * Turns a multi-dimensional array into an array of named objects
     *
     * @param   array   $array          An array of associative arrays - e.g. array( array( $key => $value ) )
     * @param   strimg  $property_name  Name of the propery on the object that'll be used for each row
     *
     * @return  array                   Array of objects
     */
    public static function toArrayOfObject($array, $property_name)
    {
        $_array = [];

        if (!is_array($array)) {
            return $_array;
        }
        
        foreach ($array as $row) {
            $_array[] = (object)[$property_name => $row];
        }

        return $_array;
    }

    /**
     * Changes the keys of an array and preserves order
     * If you don't want to preserve order, use $ar[$new_key] $ar[$key]; unset($ar[$key])
     *
     * @param   array   $array      An array of things
     * @param   string  $old_key    Old key name
     * @param   string  $new_key    New key name
     *
     * @return  array               Array of things
     */
    public static function changeKey($array, $old_key, $new_key)
    {
        if (!array_key_exists($old_key, $array)) {
            return $array;
        }

        $keys = array_keys($array);
        $keys[array_search($old_key, $keys)] = $new_key;

        return array_combine($keys, $array);
    }

    /**
     * Rekeys an array with the values
     * Used most commonly for CI's dropdown form helper, which needs key/value
     *
     * @param   array   $array          An array of things
     * @param   string  $set_default    If set, the default value will be used instead of the key name
     * @param   string  $default_value  The default to be used (separate in case we want to default to NULL)
     *
     * @return  array               Array of of things
     */
    public static function rekeySingle($array, $set_default = FALSE, $default_value = NULL)
    {
        $_array = [];

        if (!is_array($array)) {
            return $_array;
        }

        foreach ($array as $val) {
            $_array[$val] = $set_default ? $default_value : $val;
        }

        return $_array;
    }

   /**
    * Determines if there is any intersection between two arrays
    *
    * @param   array   $array1         An array of things
    * @param   array   $array2         An array of things
    *
    * @return  bool                    If array1 intersects with array2
    */
    public static function hasIntersect($array1, $array2) {
        $_in = array_intersect($array1, $array2);
        return !empty($_in);
    }

    /**
     * A version of array_key_exists that checks for the presence of multiple keys (an array of keys)
     */
    public static function arrayKeysExist(array $keys, array $array) {
        return count(self::filterKeys($array, $keys)) === count($keys);
    }
}
