<?php

namespace Transeo\Helpers;

class Dates
{
    /**
     * Converts to YYYY-MM-DD for MySQL
     *
     * @param   string  $time   String or timestamp that can be parsed by strtotime
     *
     * @return  string          Date in YYYY-MM-DD format
     */
    public static function toMySQL($time = NULL)
    {
        if (empty($time)) {
            $time = time();
        }

        if (!is_numeric($time)) {
            $time = strtotime($time);
        }

        return date('Y-m-d H:i:s', $time);

    }

    /**
     * Converts to YYYYW## for a consistent xwv format STARTING WITH SUNDAY with MySQL %XW%V, even though PHP doesn't have native support of it
     *
     * @param   string  $date   String or timestamp that can be parsed by strtotime
     *
     * @return  string          Date in YYYYW## format
     */
    public static function toXWV($date = NULL)
    {
        if (empty($date)) {
            $date = time();
        }

        if (!is_numeric($date)) {
            $date = strtotime($date);
        }

        $fmt = (date('w',$date) == 0 && strftime('%U', $date) == 01) ? (date('o',$date)+1) : date('o',$date);
        return $fmt . strftime('W%U', $date);
    }

    /**
     * Gets the day at the start of the week of the date provided
     *
     * @param   string  $date   String or timestamp that can be parsed by strtotime
     * @param   int     $day    The day code (0 = sunday, 1 = monday, etc.)
     *
     * @return  string          Date in YYYY-MM-DD for the starting day of the weeek of the date provided
     */
    public static function toFirstDayOfWeek($date = NULL, $day = 0)
    {
        if (empty($date)) {
            $date = time();
        }

        if (!is_numeric($date)) {
            $date = strtotime($date);
        }

        // if it's sunday and we want a weekday, get the weekday BEFORE the sunday
        if (date('w',$date) == 0 && $day > 0) $day += 7;

        return date('Y-m-d',strtotime(date('Y-m-d',$date) . ' -' . (date('w',$date)-$day) . ' days'));
    }

    /**
     * Generates a range of dates (or times) between first and last by increment in a specified format
     *
     * @param   string  $first      The first datetime
     * @param   string  $last       The last datetime
     * @param   string  $increment  The increment to use to get from first to last
     * @param   string  $format     The format of the dates generated
     *
     * @return  array               Array of dates
     */
    public static function range($first, $last, $increment = '+1 day', $output_format = 'Y-m-d' )
    {
        $dates = [];
        $current = is_numeric($first) ? $first : strtotime($first);
        $last = is_numeric($last) ? $last : strtotime($last);

        while( $current <= $last ) {

            switch ($output_format) {
                case 'xwv':
                    $dates[] = self::toXWV($current);
                    break;
                case 'startofwksun':
                    $dates[] = self::toFirstDayOfWeek($current);
                    break;
                default:
                    $dates[] = date($output_format, $current);
                    break;
            }

            $current = strtotime($increment, $current);
        }

        return $dates;
    }    

    /**
     * Outputs a date formatted as a standard, short, human-readable date
     *
     * @param   string  $date   String or timestamp that can be parsed by strtotime
     *
     * @return  string          Date in n/j/Y format or - if no date
     */
    public static function toShortHuman($date = NULL, $show_blank = TRUE)
    {
        if (empty($date) || $date == '1970-01-01 00:00:00') {
            return $show_blank ? '-' : NULL;
        }

        if (!is_numeric($date)) {
            $date = strtotime($date);
        }

        return date('n/j/Y', $date);
    }

    /**
     * Outputs a date formatted as a standard, short, human-readable date with time
     *
     * @param   string  $date   String or timestamp that can be parsed by strtotime
     *
     * @return  string          Date in n/j/Y format or - if no date
     */
    public static function toShortHumanWithTime($date = NULL, $show_blank = TRUE)
    {
        if (empty($date) || $date == '1970-01-01 00:00:00') {
            return $show_blank ? '-' : NULL;
        }

        if (!is_numeric($date)) {
            $date = strtotime($date);
        }

        return date('n/j/Y h:ia', $date);
    }

    /**
     * Outputs a description of the difference between two datetimes
     *
     * @param   string  $time1          String or timestamp that can be parsed by strtotime
     * @param   string  $time2          String or timestamp that can be parsed by strtotime
     * @param   bool    $descriptors    TRUE to include descriptors that are colloquial non-numerics
     *
     * @return  string                  A description
     */
    public static function differenceToString($time1, $time2, $descriptors = TRUE)
    {
        $time1 = is_numeric($time1) ? $time1 : strtotime($time1);
        $time2 = is_numeric($time2) ? $time2 : strtotime($time2);

        $text = '';
        $diff = abs($time1-$time2);

        if ($descriptors) {
            if ($diff < 5) {
                return 'a few seconds';
            }
        }

        $h = $diff > 3600 ? floor($diff/3600) : 0;
        $m = $diff > 60 ? floor(($diff-$h*3600)/60) : 0;
        $s = $diff%60;

        if ($h > 1) {
            $text .= $h . ' hour' . ($h > 1 ? 's' : '');
        }
        if ($m > 1 && $h < 3) {
            $text .= (empty($text) ? '' : ' ') . $m . ' minute' . ($m > 1 ? 's' : '');
        }
        if ($s > 1 && $m < 10 && $h < 1) {
            $text .= (empty($text) ? '' : ' ') . $s . ' second' . ($s > 1 ? 's' : '');
        }

        return $text;
    }

    /**
     * Evaluates a string as an empty date; empty strings and epoch 0 dates evaluate to TRUE
     *
     * @param   string  $date           String of a date
     *
     * @return  bool                    If it is empty
     */
    public static function isEmpty($date)
    {
        return (empty($date) || $date == '0000-00-00' || $date == '1970-01-01' || $date == '1969-12-31');
    }

    /**
     * Changes a date from a specfic timezone to a date in the utc timezone
     *
     * @param   string  $datestring           String of a date
     * @param   string  $timezone             A PHP timezone (https://www.php.net/manual/en/timezones.php)
     * @param   string  $format               Format of the date to be returned
     *
     * @return  string                        Date string adjusted to the UTC timezone in $format
     */
    public static function local_to_utc($datestring, $timezone, $format = 'Y-m-d H:i:s')
    {
        if (strtotime($datestring) !== FALSE && !empty($timezone)) {
            $datetime = new \DateTime( $datestring, new \DateTimeZone( $timezone ) );
            $datetime->setTimezone( new \DateTimeZone( 'UTC' ) );
            return $datetime->format($format);
        }
        return '';
    }

    /**
     * Changes a utc date to a date in a specific timezone
     *
     * @param   string  $datestring           String of a UTC date
     * @param   string  $timezone             A PHP timezone (https://www.php.net/manual/en/timezones.php)
     * @param   string  $format               Format of the date to be returned
     *
     * @return  string                        Date string adjusted to the $timezone in $format
     */
    public static function utc_to_local($datestring, $timezone, $format = 'Y-m-d H:i:s')
    {
        if (strtotime($datestring) !== FALSE && !empty($timezone)) {
            $datetime = new \DateTime( $datestring, new \DateTimeZone( 'UTC' ) );
            $datetime->setTimezone( new \DateTimeZone( $timezone ) );
            return $datetime->format($format);
        }
        return $datestring;
    }

    /**
     * Converts php DateTime format to Javascript Moment format.
     * @param string $phpFormat
     * @return string
     */
    public static function phpToJSMoment($php_format)
    {
        $replacements = [
            'A' => 'A',      // for the sake of escaping below
            'a' => 'a',      // for the sake of escaping below
            'B' => '',       // Swatch internet time (.beats), no equivalent
            'c' => 'YYYY-MM-DD[T]HH:mm:ssZ', // ISO 8601
            'D' => 'ddd',
            'd' => 'DD',
            'e' => 'zz',     // deprecated since version 1.6.0 of moment.js
            'F' => 'MMMM',
            'G' => 'H',
            'g' => 'h',
            'H' => 'HH',
            'h' => 'hh',
            'I' => '',       // Daylight Saving Time? => moment().isDST();
            'i' => 'mm',
            'j' => 'D',
            'L' => '',       // Leap year? => moment().isLeapYear();
            'l' => 'dddd',
            'M' => 'MMM',
            'm' => 'MM',
            'N' => 'E',
            'n' => 'M',
            'O' => 'ZZ',
            'o' => 'YYYY',
            'P' => 'Z',
            'r' => 'ddd, DD MMM YYYY HH:mm:ss ZZ', // RFC 2822
            'S' => 'o',
            's' => 'ss',
            'T' => 'z',      // deprecated since version 1.6.0 of moment.js
            't' => '',       // days in the month => moment().daysInMonth();
            'U' => 'X',
            'u' => 'SSSSSS', // microseconds
            'v' => 'SSS',    // milliseconds (from PHP 7.0.0)
            'W' => 'W',      // for the sake of escaping below
            'w' => 'e',
            'Y' => 'YYYY',
            'y' => 'YY',
            'Z' => '',       // time zone offset in minutes => moment().zone();
            'z' => 'DDD',
        ];

        // Converts escaped characters.
        foreach ($replacements as $from => $to) {
            $replacements['\\' . $from] = '[' . $from . ']';
        }

        return strtr($php_format, $replacements);
    }

    // return TRUE if date is 0000-00-00 00:00:00 format or empty.
    public static function isDateEmpty($date)
    {
        return (empty($date) || $date === '0000-00-00 00:00:00');
    }
}
