<?php

namespace Transeo\Helpers;

class Colors
{
    /**
     * Converts a 6-character hex color string to an array of RGB values
     *
     * Must use a full 6-digit hex color, short-hand notation is not currently supported
     *
     * @param   string  $hex    A hex color string. With or without a leading #
     *
     * @return  array           An array of RGB values
     */
    public static function hexToRGB($hex)
    {
        $rgb = [0, 0, 0];
        
        // remove leading hash if it was supplied
        $hex = ltrim($hex, '#');

        // handle shorthand notation, e.g. turn #ABC into #AABBCC
        if (strlen($hex) == 3) {
            $hex = vsprintf('%1$s%1$s%2$s%2$s%3$s%3$s', str_split($hex));
        }

        $rgb = sscanf($hex, '%02x%02x%02x');

        return $rgb;
    }

    /**
     * Converts array of RGB values to a HEX string
     *
     * @param   array   $rgb    An array of RGB
     *
     * @return  string          Hex color string, lowercased and starting with #
     */
    public static function rgbToHex($rgb)
    {
        return vsprintf('#%02x%02x%02x', $rgb);
    }

    /**
     * Modifies a hex color according to rules
     *
     * @param   string  $color      Hex color (i.e. #FFFFFF)
     * @param   string  $modify     What to do to the color (lighter|darker)
     *
     * @return           string   Hex of the modified color
     */
    public static function modify($color, $modify) {
        $hexpieces = str_split(trim($color, '# '), 2);

        if (!is_numeric($modify)) {
            switch ($modify) {
                // lighter is a higher hex
                case 'lighter':
                    $diff = 7.5;
                    break;
                // darker is a lower hex
                case 'darker':
                    $diff = -7.5;
                    break;
                default:
                    $diff = 0;
                    break;
            }
        }
        else {
            $diff = $modify;
        }

        foreach ($hexpieces as &$hex) {
            $dec = hexdec($hex);

            if ($diff >= 0) {
                $dec += $diff;
            }
            else {
                $dec -= abs($diff); 
            }

            $dec = max(0, min(255, $dec));
            $hex = str_pad(dechex($dec), 2, '0', STR_PAD_LEFT);
        }

        return '#' . implode($hexpieces);
    }
}
