<?php

namespace Transeo\Helpers;

class ArrayDatasets
{
    /**
     * Extracts from a dataset array based on a value matching a prefix
     * So if array[$column][$val] = 'foo_bar', extractMPrefix($array, 'foo') will return that row
     * Maintains keys of original array
     *
     * @param   array      $array          An array of things
     * @param   string     $column         The key to look in
     * @param   string     $prefix         The prefix to match
     *
     * @return  array                      Array of of things
     */
    public static function extractDatasetValuePrefix($array, $column, $prefix)
    {
        $_array = [];
     
        if (!is_array($array)) {
            return [];
        }

        foreach ($array as $idx => $row) {
            if (array_key_exists($column, $row)) {
                if (strpos($row[$column], $prefix) === 0) {
                    $_array[$idx] = $row;
                }
            }
        }

        return $_array;
    }

    /**
     * Gets all values from a sub-array key and flattens them into a single-dimension array
     *
     * @param   array   $array          An array of associative arrays - e.g. array( array( $key => $value ) )
     * @param   string  $key            The key of the key/value pair that will be matched
     * @param   string  $value          The value of the key/value pair that will be matched, if we want one
     * @param   bool    $preserve_keys  If true, the original keys will be used
     *
     * @return  array                   numerical array
     */
    public static function extractValuesAsArray($array, $key, $value = NULL, $preserve_keys = FALSE)
    {
        $_array = [];

        if (!is_array($array)) {
            return $_array;
        }
        
        foreach ($array as $idx => $row) {
            if (isset($row[$key]) && ($value === NULL || $row[$key] == $value)) {
                if ($preserve_keys) {
                    $_array[$idx] = $row[$key];
                }
                else {
                    $_array[] = $row[$key];
                }
            }
        }
        return $_array;
    }

    /**
     * Gets an array with only the values that match the filters specified
     *
     * @param   array   $array          An array of data
     * @param   string  $key            The key that will be filtered
     * @param   string  $value          The value that will be filtered (or \* to include anything with a non-blank value); array is okay
     * @param   string  $filter         = or !=
     *
     * @return  array                   Returns an array of filtered data
     */
    public static function filterValues($array, $key = NULL, $value = NULL, $filter = '=')
    {
        $_array = [];

        if (!is_array($array) || !isset($key) || !isset($value)) {
            return $_array;
        }

        foreach ($array as $idx => $row) {

            // key isn't there, so don't add it
            if (!array_key_exists($key, $row)) {
                continue;
            }

            // value is in the array or filter is !=, but not both
            if (is_array($value)) {
                if (in_array($row[$key], $value) ^ $filter == '!=') {
                    $_array[] = $row;
                }
            }
            else {
                if ($row[$key] == $value ^ $filter == '!=' || $value === '\*' && !empty($row[$key])) {
                    $_array[] = $row;
                }
            }
        }

        return $_array;
    }

    /**
     * Gets an array with only the keys in the filter keys array (all rows)
     *
     * @param   array   $array          An array of associative arrays - e.g. array( array( $key => $value ) )
     * @param   array   $allowed        An array of key names
     * @param   bool    $add_missing    If set to true, will add keys that are missing
     *
     * @return  array                   Returns $array with any keys NOT in $allowed removed
     */
    public static function filterKeys($array, $allowed, $add_missing = TRUE)
    {
        $_array = [];

        if (!is_array($array) || !is_array($allowed)) {
            return $_array;
        }

        foreach ($array as $key => $row) {
            $_array[$key] = array();

            if ($add_missing) {
                foreach ($allowed as $rowkey) {
                    $_array[$key][$rowkey] = null;
                }
            }

            foreach ($row as $rowkey => $val) {
                if (in_array($rowkey, $allowed)) {
                    $_array[$key][$rowkey] = $val;
                }
            }
        }

        return $_array;
    }

    /**
     * Removes the keys from the dataset array
     *
     * @param   array   $array          An array of associative arrays - e.g. array( array( $key => $value ) )
     * @param   array   $keys           An array of key names to be removed
     *
     * @return  array                   Returns $array with less keys
     */
    public static function removeKeys($array, $keys)
    {
        if (!is_array($array)) {
            return [];
        }

        $_array = $array;

        if (!is_array($keys)) {
            $keys = [$keys];
        }

        foreach ($_array as $idx => $row) {
            foreach ($keys as $key) {
                if (array_key_exists($key, $row)) {
                    unset($_array[$idx][$key]);
                }
            }
        }

        return $_array;
    }

    /**
     * Re-keys an array based on a column in each row
     * Returns an array of datasets (i.e. $data[$key] => $dataset, where $dataset is a set of rows)
     *
     * @param   array   $array      An array of associative arrays - e.g. array( array( $key => $value ) )
     * @param   string  $key        The key of the child arrays that will be used for the new array keys
     * @param   string  $key2       The secondary key of the child arrays that will be used for the 2nd level of hierarchy
     * @param   string  $key3       The tertiary key of the child arrays that will be used for the 3rd level of hierarchy
     * 
     * @return  array               Array of arrays (same format as $array) with the format of array[$value] = array( array() )
     */
    public static function rekeyAsDatasetArray($array, $key, $key2 = NULL, $key3 = NULL, $key4 = NULL)
    {
        $_array = [];

        if (!is_array($array)) {
            return $_array;
        }

        foreach ($array as $row) {
            if (is_array($row) && array_key_exists($key, $row)) {
                if (!isset($_array[$row[$key]])) {
                    $_array[$row[$key]] = [];
                }
                if (!empty($key2) && array_key_exists($key2, $row)) {
                    if (!isset($_array[$row[$key]][$row[$key2]])) {
                        $_array[$row[$key]][$row[$key2]] = [];
                    }
                    if (!empty($key3) && array_key_exists($key3, $row)) {
                        if (!isset($_array[$row[$key]][$row[$key2]][$row[$key3]])) {
                            $_array[$row[$key]][$row[$key2]][$row[$key3]] = [];
                        }
                        if (!empty($key4) && array_key_exists($key4, $row)) {
                            if (!isset($_array[$row[$key]][$row[$key2]][$row[$key3]][$row[$key4]])) {
                                $_array[$row[$key]][$row[$key2]][$row[$key3]][$row[$key4]] = [];
                            }
                            $_array[$row[$key]][$row[$key2]][$row[$key3]][$row[$key4]][] = $row;
                        }
                        else {
                            $_array[$row[$key]][$row[$key2]][$row[$key3]][] = $row;

                        }
                    }
                    else {
                        $_array[$row[$key]][$row[$key2]][] = $row;
                    }
                }
                else {
                    $_array[$row[$key]][] = $row;
                }
            }
        }

        return $_array;
    }

    /**
     * Re-keys an array based on a column in each row
     * Returns a dataset (i.e. $data[$key] => $row) and returns an but as an array of keys, not an array of ararys
     * NOTE that if there are multiple occurrences of $key in the source array, the LAST one will be preserved
     *
     * @param   array   $array      An array of associative arrays - e.g. array( $key => array() )
     * @param   string  $key        The key of the child arrays that will be used for the new array keys
     * @param   string  $key2       The secondary key of the child arrays that will be used for the 2nd level of hierarchy
     * @param   string  $key3       The tertiary key of the child arrays that will be used for the 3rd level of hierarchy
     * @param   string  $key4       The fourth key of the child arrays that will be used for the 4th level of hierarchy
     *
     * @return  array               Array of arrays (same format as $array) with the format of array[$value] = array()
     */
    public static function rekeyAsDataset($array, $key, $key2 = NULL, $key3 = NULL, $key4 = NULL)
    {
        $_array = [];

        if (!is_array($array)) {
            return $_array;
        }

        foreach ($array as $row) {
            if (is_array($row) && array_key_exists($key, $row)) {
                if (!empty($key2) && array_key_exists($key2, $row) && !empty($key3) && array_key_exists($key3, $row) && !empty($key4) && array_key_exists($key4, $row)) {
                    $_array[$row[$key]][$row[$key2]][$row[$key3]][$row[$key4]] = $row;
                }
                elseif (!empty($key2) && array_key_exists($key2, $row) && !empty($key3) && array_key_exists($key3, $row)) {
                    $_array[$row[$key]][$row[$key2]][$row[$key3]] = $row;
                }
                elseif (!empty($key2) && array_key_exists($key2, $row)) {
                    $_array[$row[$key]][$row[$key2]] = $row;
                }
                else {
                    $_array[$row[$key]] = $row;
                }
            }
        }

        return $_array;
    }

    /**
     * Re-keys as a single-dimension array with the keys specified and values from the column specified
     * NOTE that if there are multiple occurrences of $key in the source array, the LAST one will be preserved
     *
     * @param   array   $array      An array of associative arrays - e.g. array( $key => array() )
     * @param   string  $key        The key of the child arrays that will be used for the new array keys
     * @param   string  $value_key  The key of the dimension that will be used for the value; if null, $key will be used
     *
     * @return  array               Array of arrays (same format as $array) with the format of array[$value] = array()
     */
    public static function rekeyAsArray($array, $key, $value_key = NULL)
    {
        $_array = [];

        if (!is_array($array)) {
            return $_array;
        }

        if (!isset($value_key)) {
            $value_key = $key;
        }

        foreach ($array as $row) {
            if (array_key_exists($key, $row)) {
                $_array[$row[$key]] = $row[$value_key];
            }
        }

        return $_array;
    }

    /**
     * Ads a column ($new_key) specified with values from the column ($source) or static value
     * NOTE that if there are multiple occurrences of $key in the source array, the LAST one will be preserved
     *
     * @param   array   $array      An array of associative arrays - e.g. array( $key => array() )
     * @param   string  $source     The key to take the new value from (if $static = FALSE) or the value to put in the new key (if $static = TRUE) or 
     * @param   array   $new_key    The new keys that will be created with the new value; if a string is passed, that's okay
     * @param   bool    $static     If $source is a static value rather than a lookup
     *
     * @return  array               Array with the new key/value in it
     */
    public static function addValue($array, $source, $new_key, $static = FALSE)
    {
        $_array = $array;

        if (!is_array($array)) {
            return [];
        }
        
        if (!is_array($new_key)) {
            $new_key = [$new_key];
        }

        foreach ($_array as &$row) {
            foreach ($new_key as $key) {
                $row[$key] = $static ? $source : (isset($row[$source]) ? $row[$source] : NULL);
            }
        }

        return $_array;
    }

    /**
     * Gets the superset of keys for all rows
     *
     * @param   array   $array      An array of data
     *
     * @return  array               An array of keys
     */
    public static function keys($array)
    {
        $_array = [];

        if (!is_array($array)) {
            return $_array;
        }

        foreach ($array as $row) {
            $keys = array_keys($row);
            $_array += array_diff($keys, $_array);
        }

        return $_array;
    }

    /**
     * Gets the sum of values in a specific key
     *
     * @param   array   $array      An array of associative arrays - e.g. array( $key => array() )
     * @param   string  $key        The key to sum values from
     *
     * @return  decimal             The sum
     */
    public static function sum($array, $key)
    {
        if (!is_array($array) || empty($array)) {
            return 0;
        }

        return array_sum(array_column($array, $key));
    }

    /**
     * Gets the max of values in a specific key
     *
     * @param   array   $array      An array of associative arrays - e.g. array( $key => array() )
     * @param   string  $key        The key to get max values from
     *
     * @return  decimal             The max
     */
    public static function max($array, $key)
    {
        if (!is_array($array) || empty($array)) {
            return NULL;
        }

        return max(array_column($array, $key));
    }

    /**
     * Gets the min of values in a specific key
     *
     * @param   array   $array      An array of associative arrays - e.g. array( $key => array() )
     * @param   string  $key        The key to get min values from
     *
     * @return  decimal             The min
     */
    public static function min($array, $key)
    {
        if (!is_array($array) || empty($array)) {
            return NULL;
        }

        return min(array_column($array, $key));
    }

    /**
     * Combines the values in two associative arrays to be the combination of the sub-arrays based on the key
     * The $key must match between the two ararys in order for the contents to be combined
     * If the values of the array are NOT arrays, the 2nd one will be used
     * If the same sub-key exists in both arrays, the 2nd one will override
     *
     * @param   array   $array              An array of associative arrays - e.g. array( $key => array() )
     * @param   array2  $array              An array of associative arrays - e.g. array( $key => array() )
     * @param   bool    $keep_only_matches  if set to true, only elements with matching $keys from each array will be returned
     *
     * @return  array                       The combined arrays
     */
    public static function merge($array, $array2, $keep_only_matches = false) {
        $_array = [];

        if (!is_array($array)) {
            return $_array;
        }

        if (!is_array($array2)) {
            return $array;
        }


        foreach ($array as $key => $row) {
            if (array_key_exists($key, $array2)) {
                if (is_array($row) && is_array($array2[$key])) {
                    $_array[$key] = array_merge($row, $array2[$key]);
                }
                else {
                    $_array[$key] = $array2[$key];
                }
            }
            elseif (!$keep_only_matches) {
                $_array[$key] = $row;
            }
        }
        if (!$keep_only_matches) {
            foreach ($array2 as $key => $row) {
                if (!array_key_exists($key, $array)) {
                    $_array[$key] = $array2[$key];
                }
            }
        }
        
        return $_array;
    }

    /**
     * Sets a new key in an array to a looked-up matched value based on the key of the array and a value in the lookup array
     * Requries that both arrays are indexed the same
     * If the lookup key does not exist, NULL will be set
     * $array[$idx][$key] will be set to $lookup[$idx][$lookup_key] for each row of $array
     * If lookup_key is an array, the entire array will be put in the new key
     *
     * @param   array   $array          An array of associative arrays - e.g. array( $key => array() )
     * @param   lookup  $array          An array of associative arrays - e.g. array( $key => array() )
     * @param   string  $key            The key that will be set in the array
     * @param   string  $lookup_key     The key that will be used in the lookup array
     * @param   string  $fixed_value    If set, this value will be used for $array[$idx][$key] when the lookup succeeds instead of the lookup value
     *
     * @return  array                   The original array with looked up values
     */
    public static function addKeyFromLookup($array, $lookup, $key, $lookup_key, $fixed_value = NULL) {
        $_array = [];

        if (!is_array($array)) {
            return $_array;
        }

        foreach ($array as $idx => $row) {
            $_array[$idx] = $row;
            if (is_array($lookup) && isset($lookup[$idx]) && (is_array($lookup_key) || array_key_exists($lookup_key, $lookup[$idx]))) {
                if (isset($fixed_value)) {
                    $_array[$idx][$key] = $fixed_value;
                }
                elseif (is_array($lookup_key)) {
                    $_array[$idx][$key] = $lookup[$idx];
                }
                else {
                    $_array[$idx][$key] = $lookup[$idx][$lookup_key];
                }
            }
            else {
                $_array[$idx][$key] = NULL;
            }
        }
    
        return $_array;
    }
   
   /**
     * Gets the count of all sub-arrays in a dataset
     *
     * @param   array   $array      An array of associative arrays - e.g. array( $key => array() )
     *
     * @return  decimal             The size
     */
    public static function sizeof($array)
    {
        $size = 0;

        if (!is_array($array)) {
            return $size;
        }

        foreach ($array as $row) {
            if (is_array($row)) {
                $size += sizeof($row);
            }
        }
        
        return $size;
    }
}
