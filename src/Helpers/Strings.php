<?php

namespace Transeo\Helpers;

class Strings
{
    /**
     * Returns a random string
     *
     * @param   int     $length     (optional) Desired string length
     * @param   string  $characters (optional) String of characters to compose the random string from
     *
     * @return  string              Random string
     */
    public static function randomString($length = 10, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $characters_length = strlen($characters);
        $random = '';

        for ($i = 0; $i < $length; $i++) {
            $random .= $characters[rand(0, $characters_length - 1)];
        }

        return $random;
    }

    /**
     * Returns a random 64 character key
     *
     * @return  string              Random key
     */
    public static function randomKey()
    {
        return Strings::randomString(64, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()-_+={}[];:<>,.?/');
    }

    /**
     * Returns a string between two delimiters
     *
     * @param   string  $string
     * @param   string  $left_delimiter
     * @param   string  $right_delimiter
     *
     * @return  string                      The string between $left_delimeter and $right_delimiter
     */
    public static   function getStringBetween($string, $left_delimiter, $right_delimiter)
    {
        $start = strpos($string, $left_delimiter);

        // if the left delimiter isn't in the string, return a blank string
        if ($start === FALSE) {
            return '';
        }

        $string = substr($string, $start + strlen($left_delimiter));
        $end = strpos($string, $right_delimiter);

        if ($end === FALSE) {
            return $string;
        }

        $string = substr($string, 0, $end);

        return $string;
    }
    
    /**
     * Returns a string before the specified delimiter
     *
     * @param   string  $string
     * @param   string  $delimiter
     *
     * @return  string                      The string before $delimiter
     */
    public static function getStringBefore($string, $delimiter)
    {
        return substr($string, 0, strpos($string, $delimiter));
    }

    /**
     * Returns a string with 's unless it ends in an s in which case it just returns string with '
     *
     * @param   string  $string
     *
     * @return  string                      The string to own
     * @return  bool                        If an upper-case S should be used
     */
    public static function possessive($string, $upper = FALSE)
    {
        $end = strtolower(substr($string, -1)) == 's' ? "'" : "'s";
        return $string . ($upper ? strtoupper($end) : $end);
    }

    /**
     * Returns a human-readable, uc-firsted space string (This And That) from a snake-case string (this_and_that) to a 
     * Includes support for common column naming conventions (id, utc, dtm, etc.)
     *
     * @param   string  $string
     *
     * @return  string                      The string
     */
    public static function snakeColumnNameToHuman($string)
    {
        $return = '';
        foreach (explode('_', $string) as $part) {
            switch ($part) {
                case 'id':
                    $part = 'ID';
                    break;
                case 'utc':
                    $part = 'GMT';
                    break;
                case 'dtm':
                    $part = 'Date/Time';
                    break;
                case 'bln':
                    $part = NULL;
                    break;
                default:
                    $part = ucfirst($part);
            }
            if (!empty($part)) {
                $return .= $part . ' ';
            }
        }
        
        return trim($return);
    }

    /**
     * Returns a human-readable value based on the column it is for
     * Includes support for common column naming conventions (id, utc, dtm, etc.)
     * Defaults to just stringifying it
     *
     * @param   string  $column_name
     * @param   string  $value
     *
     * @return  string  The string
     */
    public static function snakeColumnValueToHuman($column_name, $value)
    {
        if (strpos($column_name, 'dtm') !== FALSE) {
            return \Transeo\Helpers\Dates::toShortHumanWithTime($value);
        }

        if (strpos($column_name, 'amount') !== FALSE && is_numeric($value)) {
            return \Transeo\Helpers\Numbers::toCurrency($value);
        }

        if (strpos($column_name, 'bln') !== FALSE) {
            return $value ? 'Yes' : 'No';
        }

        return $value;
    }

    // formats a phone number into international or national format
    // national format:  xxx-xxx-xxxx
    // international format varies, but one format is:  44 20 7183 8750
    // uses https://github.com/giggsey/libphonenumber-for-php for international numbers
    // requires at least 7 numbers to format, otherwise original number is returned.
    public static function phoneNumberFormat($raw_number, $force_simple = FALSE)
    {
        $starts_with_plus = (strpos($raw_number, '+') === 0);
        $only_numbers = preg_replace('/[^0-9]/', '', $raw_number );
        if (!$force_simple && strlen($only_numbers) > 10) {        
            // libphone needs the plus to determine international numbers
            if (!$starts_with_plus) {
                $raw_number = '+' . $raw_number;
            }

            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            try {
                $phone_number_obj = $phoneUtil->parse($raw_number, 'US');
            } 
            catch (\libphonenumber\NumberParseException $e) {
                return format_phone_number($raw_number, TRUE);
            }

            if (!$phoneUtil->isPossibleNumber($phone_number_obj)) {
                return format_phone_number($raw_number, TRUE);
            }
            if ($phoneUtil->getRegionCodeForNumber($phone_number_obj) != 'US') {
                $formated_number = $phoneUtil->format($phone_number_obj, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);
            }
            else {
                $formated_number = $phoneUtil->format($phone_number_obj, \libphonenumber\PhoneNumberFormat::NATIONAL);
            }

            // take out anything non digit or - (no parenthesis and plus signs)
            return preg_replace('/[^0-9 \-]/', '', $formated_number);
        }

        // if format is 10 numbers or less, or not valid, do simple formatting.
        preg_match('/([0-9]*)?([0-9]{3})?([0-9]{3})([0-9]{4})$/', $only_numbers, $matches ); 
        if (empty($matches)) {
            return $raw_number;
        }
        // get rid of original string
        array_shift($matches);

        // get rid of empty values
        $matches = array_filter($matches);

        return implode('-', $matches);
    }

}
