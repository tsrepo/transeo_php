<?php

namespace Transeo;

use Composer\Script\Event;

/**
 * tsrepo/transeo_php composer package installer
 */
class Installer
{
    /**
     * Composer post install script
     *
     * @param Event $event
     */
    public static function postInstall(Event $event = NULL)
    {
        // update config.php to enable composer autoloader
        $file = 'application/config/config.php';
        $contents = file_get_contents($file);
        $contents = str_replace(
            '$config[\'composer_autoload\'] = FALSE;',
            '$config[\'composer_autoload\'] = \'vendor/autoload.php\';',
            $contents
        );
        file_put_contents($file, $contents);
        
        // copy index.php from CI framework into root of site & alter system path
        self::copy_index_php();

        // Update PhpSpreadsheet to handle bad titles
        self::update_php_spreadsheet();
    }

    /**
     * Composer post update script
     *
     * @param Event $event
     */
    public static function postUpdate(Event $event = NULL)
    {
        // incase index.php has been updated in a codeigniter patch, recopy it to the root
        self::copy_index_php();

        // Update PhpSpreadsheet to handle bad titles
        self::update_php_spreadsheet();

    }

    /**
     * Copy index.php from codeigniter/framework/index.php to /index.php and
     * change the $system_path variable to point at codeigniter/framework/system
     *
     * @return void
     */
    private static function copy_index_php()
    {
        // copy index.php from framework into project
        copy('vendor/codeigniter/framework/index.php', 'index.php');

        // change system path to point at codeigniter/framework's system
        $file = 'index.php';
        $contents = file_get_contents($file);
        $contents = str_replace(
            '$system_path = \'system\';',
            '$system_path = \'vendor/codeigniter/framework/system\';',
            $contents
        );
        
        // add demo as an acceptable env
        $contents = str_replace(
            "case 'testing':",
            "case 'demo': case 'testing':",
            $contents
        );

        file_put_contents($file, $contents);
    }

    /**
     * Update PhpSpreadsheet to handle bad titles
     * Requires an update to php spreadsheet to support worksheet titles that are bad.  This is not being recognized as a bug and
     * didn't feel like forking the repo to our own, but that might be better...
     *
     * @return void
     */
    private static function update_php_spreadsheet()
    {
        // the php spreadsheet file
        $file = 'vendor/phpoffice/phpspreadsheet/src/PhpSpreadsheet/Worksheet/Worksheet.php';

        // change system path to point at codeigniter/framework's system
        $contents = file_get_contents($file);
        $contents = str_replace(
            'throw new Exception(\'Invalid character found in sheet title\');',

            'return str_replace(self::$invalidCharacters, \'\', $pValue);',
            
            $contents
        );
        
        file_put_contents($file, $contents);
    }

}
