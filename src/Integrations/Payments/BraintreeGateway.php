<?php

namespace Transeo\Integrations\Payments;

use Braintree\Configuration;
use Braintree\Subscription;

use Transeo\Loggers\NullApiLogger;

class BraintreeGateway extends AbstractGateway
{
    use \Transeo\Loggers\ApiLoggerAwareTrait;
    use \Psr\Log\LoggerAwareTrait;

    public function __construct()
    {
        $this->setApiLogger(new NullApiLogger());
        $this->setLogger(new \Psr\Log\NullLogger());
    }

    public function getName()
    {
        return 'braintree';
    }

    public function canDoRecurring()
    {
        return TRUE;
    }

    public function getRequiredParameters()
    {
        return [
            'environment',
            'merchant_id',
            'public_key',
            'private_key'
        ];
    }

    public function initialize(array $parameters = array())
    {
        parent::initialize($parameters);

        Configuration::environment($parameters['environment']);
        Configuration::merchantId($parameters['merchant_id']);
        Configuration::publicKey($parameters['public_key']);
        Configuration::privateKey($parameters['private_key']);
    }

    public function createSubscription($payment_method_token)
    {
        $request = [
            'paymentMethodToken' => $payment_method_token,
            'planId' => $this->settings['subscription_plan_id']
        ];

        $result = Subscription::create($request);

        $this->api_logger->log('braintree', print_r($request, TRUE), print_r($result, TRUE));

        if (!$result->success) {
            $this->logger->error('Error creating subscription: ' . $result->message);
            return FALSE;
        }

        return [
            'subscription_id'       => $result->subscription->id,
            'subscription_status'   => $result->subscription->status,
            'first_billing_date'    => $result->subscription->firstBillingDate->format('Y-m-d H:i:s')
        ];
    }

    public function updateSubscription($subscription_id, $payment_token)
    {
        $result = Subscription::update($subscription_id, [
            'paymentMethodToken' => $payment_token,
        ]);

        $this->api_logger->log('braintree', print_r([$subscription_id, $payment_token], TRUE), print_r($result, TRUE));

        if (!$result->success) {
            $this->logger->error('Error updating subscription: ' . $result->message);
            return FALSE;
        }

        return [
            'subscription_id'       => $result->subscription->id,
            'subscription_status'   => $result->subscription->status,
            'first_billing_date'    => $result->subscription->firstBillingDate->format('Y-m-d H:i:s')
        ];
    }

    public function cancelSubscription($subscription_id)
    {
        $result = NULL;

        try {
            $result = Subscription::cancel($subscription_id);
        } catch (Exception $ex) {
            $this->logger->error('Error canceling subscription: ' . $ex->getMessage());
            $this->api_logger->log('braintree', print_r($subscription_id, TRUE), print_r($ex, TRUE));
            return FALSE;
        }

        $this->api_logger->log('braintree', print_r($subscription_id, TRUE), print_r($result, TRUE));

        return TRUE;
    }
}
