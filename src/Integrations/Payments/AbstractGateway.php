<?php

namespace Transeo\Integrations\Payments;

use Transeo\Helpers\Arrays;

abstract class AbstractGateway
{
    protected $parameters = [];

    abstract public function getName();
    
    public function canDoPayout()
    {
        return FALSE;
    }
    
    public function canDoRecurring()
    {
        return FALSE;
    }

    public function getDefaultParameters()
    {
        return [];
    }

    public function getRequiredParameters()
    {
        return [];
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function initialize(array $parameters = [])
    {
        // merge parameters w/ the defaults
        $this->parameters = array_merge($this->getDefaultParameters(), $parameters);

        // check if there are any required parameters
        $required_parameters = $this->getRequiredParameters();
        if (is_null($required_parameters) || empty($required_parameters)) {
            return;
        }

        // enforce required parameters exist
        foreach ($required_parameters as $required_parameter) {
            if (!isset($this->parameters[$required_parameter])) {
                throw new Exception("Required PayoutGateway parameter '{$required_parameter}' has not been specified.");
            }
        }
    }
}
