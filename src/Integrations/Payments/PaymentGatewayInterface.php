<?php

namespace Transeo\Integrations\Payments;

interface PaymentGatewayInterface
{
    public function getView();
    public function getAccessToken($code);
    public function getUserInfo($access_token);
}
