<?php

namespace Transeo\Integrations\Payments;

/**
 * A generic Payout Gateway interface used to send payments in batch
 */
interface PayoutGatewayInterface
{
    /**
     * $payments is an array where each $item is an associative array of the following format:
     *      $item = [
     *          'payment_id'    => '',      // a local payment_id to identify payout across local system and payout vendor
     *          'email'         => '',      // recipients paypal email address
     *          'payout_amount' => 0.01,    // amount in USD to payout
     *          'note'          => ''       // a note attached to payout for recipient to read
     *      ];
     *
     * Returns:
     *  On failure, returns FALSE
     *  On success, returns the original $payments array that has been modified to add the following values to each item
     *      $item = [
     *          'processor_fee'     => 0.00,    // amount of the processor's fee
     *          'batch_id'          => '',      // processor's id of payout batch
     *          'batch_status'      => '',      // status of entire payout batch (TODO: possible values)
     *          'batch_item_id'     => '',      // processor's id of the individual payout item in the batch
     *          'batch_item_status' => ''       // status of individual payout in batch (TODO: possible values)
     *      ];
     */
    public function doBatchPayout(array $payments);
}
