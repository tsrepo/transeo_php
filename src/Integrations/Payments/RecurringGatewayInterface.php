<?php

namespace Transeo\Integrations\Payments;

use Braintree\ClientToken;
use Braintree\Configuration;
use Braintree\Customer;
use Braintree\PaymentMethod;
use Braintree\Subscription;
use Braintree\Transaction;
use Braintree\WebhookNotification;

/**
 * A generic Recurring Payments Gateway interface used to send payments in batch
 */
interface RecurringGatewayInterface
{

    public function createSubscription($email, $plan_id, $shipping_address);

    public function updateSubscription();
    
    public function cancelSubscription();
}
