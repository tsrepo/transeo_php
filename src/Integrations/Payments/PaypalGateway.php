<?php

namespace Transeo\Integrations\Payments;

use PayPal\Api\OpenIdTokeninfo;
use PayPal\Api\OpenIdUserinfo;
use Paypal\Api\PayerInfo;
use PayPal\Api\Payout;
use PayPal\Api\PayoutItem;
use PayPal\Api\PayoutSenderBatchHeader;
use PayPal\Api\VerifyWebhookSignature;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Common\PayPalModel;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Rest\ApiContext;

use Transeo\Loggers\NullApiLogger;

// TODO: better error handling + logging (https://developer.paypal.com/docs/api/payments/#errors)
// TODO: add webhook validation method support
class PaypalGateway extends AbstractGateway implements PayoutGatewayInterface, RecurringGatewayInterface
{
    use \Transeo\Loggers\ApiLoggerAwareTrait;
    use \Psr\Log\LoggerAwareTrait;

    private $api_context = NULL;

    public function __construct()
    {
        $this->setApiLogger(new NullApiLogger());
        $this->setLogger(new \Psr\Log\NullLogger());
    }

    public function getName()
    {
        return 'paypal';
    }

    public function canDoPayout()
    {
        return TRUE;
    }

    public function getRequiredParameters()
    {
        return [
            'client_id',
            'client_secret'
        ];
    }

    public function initialize(array $parameters = array())
    {
        parent::initialize($parameters);

        // initialize api context w/ credentials
        $this->api_context = new ApiContext(
            new OAuthTokenCredential(
                $this->parameters['client_id'],
                $this->parameters['client_secret']
            )
        );

        // configure api context if specified (logging, caching, curl and mode=live)
        // see https://github.com/paypal/PayPal-PHP-SDK/wiki/Logging
        // see https://github.com/paypal/PayPal-PHP-SDK/wiki/Caching
        // see https://github.com/paypal/PayPal-PHP-SDK/wiki/Curl-Options
        if (isset($this->parameters['config'])) {
            $this->api_context->setConfig($this->parameters['config']);
        }
    }

    public function doBatchPayout(array $payments)
    {
        if (empty($payments)) {
            return TRUE;
        }

        $batch_header = new PayoutSenderBatchHeader();
        $batch_header
            ->setSenderBatchId(uniqid())
            ->setEmailSubject($this->parameters['payout_email_subject']);

        $payouts = new Payout();
        $payouts->setSenderBatchHeader($batch_header);

        // we rekey $payments from an array to an associative array that uses payment_id as the key
        // this is required because the paypal create batch payout api call doesn't return all of the data
        // we need so we have to make another API call to fetch the batch payout details later. To lineup the data
        // from the first create batch payout API call and the 2nd get batch payout details, we need a reliable common key
        $data = array_column($payments, NULL, 'payment_id');

        foreach ($payments as $payment) {
            $payout = new PayoutItem([
                'recipient_type' => 'EMAIL',
                'receiver' => $payment['email'],
                'note' => $payment['note'],
                'sender_item_id' => $payment['payment_id'],
                'amount' => [
                    'value' => $payment['payout_amount'],
                    'currency' => 'USD'
                ]
            ]);

            $payouts->addItem($payout);
        }

        $request = clone $payouts;

        try {
            $response = $payouts->create(NULL, $this->api_context);
        } catch (Exception $ex) {
            $this->logger->error('Error creating paypal batch: ' . $ex->getMessage());
            $this->api_logger->log('paypal', print_r($request, TRUE), print_r($ex, TRUE));
            return FALSE;
        }

        $this->api_logger->log('paypal', print_r($request, TRUE), print_r($response, TRUE));
        
        $batch_id = $response->getBatchHeader()->getPayoutBatchId();
        $batch_status = $response->getBatchHeader()->getBatchStatus();

        // the batch payout response doesn't contain the item details we need
        // so we have to make a separate call to get the batch details
        try {
            $batch_details = Payout::get($batch_id, $this->api_context);
        } catch (Exception $ex) {
            $this->logger->error('Error getting paypal batch details for #' . $batch_id . ' error: ' . $ex->getMessage());
            $this->api_logger->log('paypal', $batch_id, print_r($ex, TRUE));
            return FALSE;
        }

        $this->api_logger->log('paypal', $batch_id, print_r($batch_details, TRUE));

        // for each batch item, update our payout data w/ detailed info
        $items = $batch_details->getItems();
        foreach ($items as $item) {
            $sender_item_id = $item->getPayoutItem()->getSenderItemId();

            // add batch info to data
            $data[$sender_item_id] += [
                'processor'         => $this->getName(),
                'processor_fee'     => $item->getPayoutItemFee()->getValue(),
                'batch_id'          => $batch_details->getBatchHeader()->getPayoutBatchId(),
                'batch_status'      => $batch_details->getBatchHeader()->getBatchStatus(),
                'batch_item_id'     => $item->getPayoutItemId(),
                'batch_item_status' => $item->getTransactionStatus(),
            ];
        }

        // we don't need the payment_id keys anymore, just the values
        return array_values($data);
    }

    // In Paypal you create a "billing plan" and then the subscriptions to those plans are called "billing agreements"
    // https://developer.paypal.com/docs/integration/direct/billing-plans-and-agreements/
    // https://developer.paypal.com/docs/api/payments.billing-agreements
    // https://github.com/paypal/PayPal-PHP-SDK/blob/master/sample/billing/CreateBillingAgreementWithPayPal.php
    public function createSubscription($email, $plan_id, $shipping_address)
    {
        $plan = new Plan();
        $plan->setId($plan_id);

        $payer_info = new PayerInfo();
        $payer_info
            ->setEmail($email);

        $payer = new Payer();
        $payer
            ->setPaymentMethod('paypal')
            ->setPayerInfo($payer_info);

        $address = new ShippingAddress();
        $address
            ->setLine1($shipping_address['address1'])
            ->setCity($shipping_address['city'])
            ->setState($shipping_address['state'])
            ->setPostalCode($shipping_address['zip'])
            ->setCountryCode('US');

        $agreement = new Agreement();
        $agreement
            ->setName($this->parameters['recurring_agreement_name'])
            ->setDescription($this->parameters['recurring_agreement_desc'])
            ->setStartDate(date(DateTime::RFC3339))
            ->setPlan($plan)
            ->setPayer($payer)
            ->setShippingAddress($address);
        
        $request = clone $agreement;

        try {
            // Please note that as the agreement has not yet activated, we wont be receiving the ID just yet.
            $agreement = $agreement->create($this->api_context);

            $approval_url = $agreement->getApprovalLink();

        } catch (Exception $ex) {
            $this->logger->error('Error getting creating Paypal Billing Agreement: ' . $ex->getMessage());
            $this->api_logger->log('paypal', $batch_id, print_r($ex, TRUE));
            return FALSE;
        }

        // TODO: expecting.....
        // $subscription['subscription_id']
        // $subscription['subscription_status']
        // $subscription['first_billing_date']

        return [
            'approval_url' => $approval_url
        ];
    }

    public function updateSubscription()
    {
    }
    
    public function cancelSubscription()
    {
    }

    // fetches a paypal access token using the authorization code we got as part of oauth login
    public function getAccessToken($code)
    {
        try {
            $params = ['code' => $code];
            $access_token = OpenIdTokeninfo::createFromAuthorizationCode($params, null, null, $this->api_context);
        } catch (PayPalConnectionException $ex) {
            $this->logger->error('Error getting access token. Error: ' . $ex->getMessage());
            $this->api_logger->log('paypal', print_r($params, TRUE), print_r($ex, TRUE));
            return FALSE;
        }

        $this->api_logger->log('paypal', print_r($params, TRUE), print_r($access_token, TRUE));

        return $access_token->getAccessToken();
    }

    // get user's identity from their access token
    public function getUserInfo($access_token)
    {
        try {
            $params = ['access_token' => $access_token];
            $user_info = OpenIdUserinfo::getUserinfo($params, $this->api_context);
        } catch (PayPalConnectionException $ex) {
            $this->logger->error('Error getting user info. Error: ' . $ex->getMessage());
            $this->api_logger->log('paypal', print_r($params, TRUE), print_r($ex, TRUE));
            return FALSE;
        }

        $this->api_logger->log('paypal', print_r($params, TRUE), print_r($user_info, TRUE));

        return $user_info;
    }
}
