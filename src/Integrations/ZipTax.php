<?php

namespace Transeo\Integrations;

// see http://docs.zip-tax.com/en/latest/index.html
class ZipTax
{
    use \Psr\Log\LoggerAwareTrait;

    const API_VERSION = 'v20';
    const API_FORMAT = 'JSON';
    const API_ACTION_GET_TAXES = 'request';

    const API_RESPONSE_CODES = [
        '100' => 'SUCCESS',
        '101' => 'INVALID_KEY',
        '102' => 'INVALID_STATE',
        '103' => 'INVALID_CITY',
        '104' => 'INVALID_POSTAL_CODE',
        '105' => 'INVALID_FORMAT',
    ];

    private $api_key;
    private $api_url;
    private $api_common_params = [];

    public function __construct($settings)
    {
        $this->api_key = $settings['api_key'];
        $this->api_url = $settings['api_url'];

        $this->api_common_params = [
            'key'       => $this->api_key,
            'format'    => self::API_FORMAT
        ];

        // consumers of this class can manually call setLogger() to inject their preferred logger
        $this->setLogger(new \Psr\Log\NullLogger());
    }

    public function getTaxes($zip_code)
    {
        $params = [
            'postalcode' => $zip_code
        ];

        $response = $this->request(self::API_ACTION_GET_TAXES, $params);

        if ($response === false || $response->rCode != 100 || count($response->results) < 1) {
            return false;
        }

        return [
            'zip'               => $response->results[0]->geoPostalCode,
            'city'              => $response->results[0]->geoCity,
            'county'            => $response->results[0]->geoCounty,
            'state'             => $response->results[0]->geoState,
            'total_sales'       => $response->results[0]->taxSales,
            'total_use'         => $response->results[0]->taxUse,
            'taxable_service'   => $response->results[0]->txbService,
            'taxable_freight'   => $response->results[0]->txbFreight,
            'state_sales'       => $response->results[0]->stateSalesTax,
            'state_use'         => $response->results[0]->stateUseTax,
            'city_sales'        => $response->results[0]->citySalesTax,
            'city_use'          => $response->results[0]->cityUseTax,
            'city_tax_code'     => $response->results[0]->cityTaxCode,
            'county_sales'      => $response->results[0]->countySalesTax,
            'county_use'        => $response->results[0]->countyUseTax,
            'county_tax_code'   => $response->results[0]->countyTaxCode,
            'district_sales'    => $response->results[0]->districtSalesTax,
            'district_use'      => $response->results[0]->districtUseTax,
        ];
    }

    private function request($action, $params)
    {
        // add common params (apikey, etc)
        $params += $this->api_common_params;

        // construct API url
        $query_string = http_build_query($params);
        $url = sprintf('%s/%s/%s?%s', $this->api_url, $action, self::API_VERSION, $query_string);
        
        // fetchy
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($ch);
        
        if ($response === false) {
            $error = curl_error($ch);
            curl_close($ch);

            $this->logger->error('zip-tax API request to ' . $url . ' had following curl_error: ' . $error);

            return false;
        }
        
        curl_close($ch);

        $json = json_decode($response);

        $this->logger->debug('zip-tax API request to ' . $url . ' returned ' . self::API_RESPONSE_CODES[$json->rCode]);

        return $json;
    }
}
