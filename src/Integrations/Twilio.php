<?php

namespace Transeo\Integrations;

use Transeo\Tools\Curl;

class Twilio
{
    protected $account_sid = NULL;
    protected $auth_token = NULL;
    protected $sms_from = NULL;
    protected $sms_url = NULL;
    protected $voice_from = NULL;
    protected $voice_url = NULL;

    public $response = NULL;

    public function __construct($config)
    {
        // set settings
        $this->account_sid = isset($config['account_sid']) ? $config['account_sid'] : NULL;
        $this->auth_token = isset($config['auth_token']) ? $config['auth_token'] : NULL;
        $this->sms_from = isset($config['sms_from']) ? $config['sms_from'] : NULL;
        $this->voice_from = isset($config['voice_from']) ? $config['voice_from'] : NULL;

        $this->sms_url = 'https://api.twilio.com/2010-04-01/Accounts/' . $this->account_sid . '/Messages.json';
        $this->voice_url = 'https://api.twilio.com/2010-04-01/Accounts/' . $this->account_sid . '/Calls.json';
        $this->deactivations_url = 'https://messaging.twilio.com/v1/Deactivations';
    }

    // note that Twilio automatically handles STOP, STOPALL, UNSUBSCRIBE, CANCEL, END, and QUIT to unsubscribe
    // and START, YES and UNSTOP will to re-subscribe
    // and HELP and INFO for that info
    // see https://support.twilio.com/hc/en-us/articles/223134027-Twilio-support-for-STOP-BLOCK-and-CANCEL-SMS-STOP-filtering-
    public function sendSMS($to, $message, $status_callback = '')
    {
        $from = $this->sms_from;

        $params = array(
            'To'    => $to,
            'From'  => $from,
            'Body'  => $message,
        );

        if (!empty($status_callback)) {
            $params['statusCallback'] = $status_callback;
        }

        $twilio = new Curl();
        $twilio->setBasicAuth($this->account_sid, $this->auth_token);
        $twilio->URLFetch(
            $this->sms_url,
            $params
        );

        $this->response = $twilio->response;

        return $this->response;
    }

    public function call($to, $instructions_url, $handler_url = NULL)
    {
        $from = $this->sms_from;
        $handler_url = isset($handler_url) ? site_url($handler_url) : NULL;
        $instructions_url = site_url($instructions_url);

        $params = array(
            'To'                => $to,
            'From'              => $from,
            'Url'               => $instructions_url,
        );
        if (isset($handler_url)) {
            $params['Method']                   = 'GET';
            $params['StatusCallback']           = $handler_url;
            $params['StatusCallbackEvent'][]    = 'initiated';
            $params['StatusCallbackEvent'][]    = 'ringing';
            $params['StatusCallbackEvent'][]    = 'answered';
            $params['StatusCallbackEvent'][]    = 'completed';
            $params['StatusCallbackEvent'][]    = 'no-answer';
            $params['StatusCallbackEvent'][]    = 'failed';
            $params['StatusCallbackEvent'][]    = 'busy';
        }

        // allow local debugging
        if (isset($params['StatusCallback']) && strpos($params['StatusCallback'], 'localhost') !== FALSE) {
            $params['StatusCallback'] = str_replace('localhost', 'www.transeosolutions.com', $params['StatusCallback']);
        }
        if (isset($params['Url']) && strpos($params['Url'], 'localhost') !== FALSE) {
            $params['Url'] = str_replace('localhost', 'www.transeosolutions.com', $params['Url']);
        }

        $twilio = new Curl();
        $twilio->setBasicAuth($this->account_sid, $this->auth_token);
        $twilio->URLFetch(
            $this->voice_url,
            $params
        );

        $this->response = $twilio->response;

        return $this->response;
    }

    // date_string in Y-m-d format
    public function getDeactivatedPhoneNumbers($date_string)
    {
        $twilio = new Curl();
        $twilio->setBasicAuth($this->account_sid, $this->auth_token);
        $twilio->URLFetch(
            $this->deactivations_url . '?Date=' . $date_string
        );

        if (strpos($twilio->response, '{') !== FALSE) {
            return json_decode($twilio->response, TRUE);
        }

        $this->response = $twilio->response;

        return $this->response;
    }
}
