<?php

namespace Transeo\Integrations;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

use Aws\AutoScaling\AutoScalingClient;

class AWS
{
    public $client = NULL;
    private $bucket = NULL;

    const SERVICE_S3 = 'S3';
    const SERVICE_EC2 = 'EC2';

    const REGION = 'us-east-1';

    public function __construct($config = [], $service = 'S3')
    {
        if ($this->client == NULL) {
            switch ($service) {
                case self::SERVICE_S3:
                    $this->client = new S3Client([
                        'region' => $config['region'] ?? self::REGION,
                        'version' => 'latest'
                    ] + $config);
                    
                    break;
                
                case self::SERVICE_EC2:
                    $this->client = new AutoScalingClient([
                        'region' => $config['region'] ?? self::REGION,
                        'version' => 'latest'
                    ] + $config);
                
                    break;
            }
        }
    }

    //upload file to S3 bucket with the specified key
    public function uploadFile($bucket, $file, $key, $options = [])
    {
        try {
            $this->client->putObject(array_merge([
                'Bucket' => $bucket,
                'Key' => $key,
                'SourceFile' => $file,
            ], $options));
        } catch (S3Exception $e) {
            // Catch an S3 specific exception.
            echo $e->getMessage();
        }
    }

    //get file from S3 bucket and download it
    public function cacheFile($bucket, $key)
    {
        try {
            $file = $this->client->getObject([
                'Bucket'=>$bucket,
                'Key' => $key,
            ]);

            $file_path = stream_get_meta_data(tmpfile());
            $file_path = $file_path['uri'];

            $fh = fopen($file_path, 'w'); // if we don't re-open it, the tempfile will be deleted as soon as this script finishes
            fwrite($fh, $file['Body']);

            fclose($fh);

            return $file_path;
        } catch (S3Exception $e) {
            return FALSE;
        }
    }

    public function getFileMetadata($bucket, $key) {
        try {
            $list = $this->client->listObjects([
                'Bucket'=>$bucket,
                'Prefix' => $key,
            ]);

            return $list['Contents'];
        }
        catch (S3Exception $e) {
            return NULL;
        }
    }

    //get file from S3 bucket and download it
    public function getFile($bucket, $key)
    {
        $file = $this->getFileWithMetadata($bucket, $key);

        if ($file === NULL) {
            return NULL;
        }

        header("Content-Type: {$file['ContentType']}");

        echo $file['Body'];
    }

    public function getFileWithMetadata($bucket, $key)
    {
        try {
            $file = $this->client->getObject([
                'Bucket'=>$bucket,
                'Key' => $key,
            ]);

            return $file;
        }
        catch (S3Exception $e) {
            return NULL;
        }
    }

    public function getFileContents($bucket, $key)
    {
        $file = $this->getFileWithMetadata($bucket, $key);

        if ($file === NULL) {
            return NULL;
        }

        return $file['Body'];
    }

    public function getSignedFilePath($bucket, $key) 
    {
        try {
            $cmd = $this->client->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key' => $key
            ]);
            $request = $this->client->createPresignedRequest($cmd, '+60 minutes');
            $presignedUrl = (string)$request->getUri();
            return !empty($presignedUrl) ? $presignedUrl : '';
        } catch (S3Exception $e) {
            return FALSE;
        }
    }

    public function deleteFile($bucket, $key)
    {
        try {
            $file = $this->client->deleteObject([
                'Bucket' => $bucket,
                'Key' => $key,
            ]);
            return TRUE;
        } catch (S3Exception $e) {
            return FALSE;
        }
    }

    public function listFiles($bucket, $key) {
        try {
            return $this->client->listObjectsV2([
                'Bucket' => $bucket,
                'Prefix' => $key,
            ]);
        } catch (S3Exception $e) {
            return FALSE;
        }
    }

    public static function isEC2()
    {
        return file_exists('/var/lib/cloud/data/instance-id');
    }

    public function terminateInstance($instance_id, $decrease = FALSE)
    {
        return $this->client->terminateInstanceInAutoScalingGroup([
            'InstanceId' => $instance_id,
            'ShouldDecrementDesiredCapacity' => $decrease,
        ]);
    }
}
