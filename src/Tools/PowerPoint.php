<?php

namespace Transeo\Tools;

class PowerPoint
{
    public $oZip;
    public $slides = array();
    public $slide_count = 0;
    public $metadata = array();
    private $_filename;

    const THEME_PATH = 'ppt/theme/theme1.xml';
    
    public function __construct($file, $outputfile = null) {
        $tempname = empty($outputfile) ? \Transeo\Helpers\Filesystem::fileGetTempName() : $outputfile;

        $this->_filename = $tempname;

        if (file_exists($tempname)) {
            unlink($tempname);
        }
        copy($file, $tempname);
        
        $this->oZip = new \ZipArchive();


        // #TODO the slide[x].xmls don't need to be in numeric order
        // PPT will re-number and save them, but if we've programmatically added a file, we should get the order
        // from the slide list xml (see adding a slide) instead of assuming the order matches the file names

        if ($this->oZip->open($tempname)) {
            
            for($i = 0; $i < $this->oZip->numFiles; $i++) { 
                $stat = $this->oZip->statIndex($i); 
                if (strpos($stat['name'],'ppt/slides/slide') !== false) {
                    $this->slide_count++;
                }
            } 
            for($i = 0; $i < $this->slide_count; $i++) { 
                $this->slides[$i+1] = array(
                    'name'  => 'ppt/slides/slide' . ($i+1) . '.xml',
                    'xml'   => $this->oZip->getFromName('ppt/slides/slide' . ($i+1) . '.xml'),
                );
            } 
        }
        else {
            new \Exception('Cannot open file');
        }
    }
    
    public function getMetadata($path) {
        if (!isset($this->metadata[$path])) {
            $this->metadata[$path] = $this->oZip->getFromName($path);
        }
        return $this->metadata[$path];
    }

    public function setMetadata($path, $xml) {
        $this->metadata[$path] = $xml;
    }

    public function getSlide($index) {
        return $this->slides[$index]['xml'];
    }

    public function getTheme() {
        return $this->getMetadata(self::THEME_PATH);
    }
    
    public function setTheme($xml) {
        $this->setMetadata(self::THEME_PATH, $xml);
    }

    public function getSlideRel($index) {
        return $this->oZip->getFromName('ppt/slides/_rels/slide' . $index . '.xml.rels');
    }
    
    public function setSlide($index, $xml, $xml_file_name = null) {
        $this->slides[$index]['xml'] = $xml;
        if (!empty($xml_file_name)) {
            $this->slides[$index]['name'] = 'ppt/slides/' . $xml_file_name;
        }
    }

    public function moveSlide($index_from, $index_to) {
        $this->slides[$index_to] = $this->slides[$index_from];
    }

    public function insertSlide($new_slide_xml, $new_slide_rel_xml, $after_index = 'end') {
        if ($after_index == 'end') { 
            $after_index = $this->slide_count; 
        }
        $slide_index = $after_index + 1;
        $slide_xml_file = 'slide' . ($this->slide_count+1) . '.xml';

        $this->slide_count++;
        
        // shift slides if we're inserting in the middle
        if ($slide_index < ($this->slide_count-1)) {
            for ($i=$this->slide_count; $i>$slide_index; $i--) {
                $this->moveSlide($i-1, $i);
            }
        }
        // add new slide and rel file
        $this->setSlide($slide_index, $new_slide_xml, $slide_xml_file);
        $this->setMetadata('ppt/slides/_rels/' . $slide_xml_file . '.rels', $new_slide_rel_xml);


        // get [Content_Types].xml to add a new <ContentType="application/vnd.openxmlformats-officedocument.presentationml.slide+xml"/>
        $xml = simplexml_load_string($this->getMetadata('[Content_Types].xml'));

        $el = $xml->addChild('Override');
        $el->addAttribute('PartName','/ppt/slides/' . $slide_xml_file);
        $el->addAttribute('ContentType','application/vnd.openxmlformats-officedocument.presentationml.slide+xml');
        $this->setMetadata('[Content_Types].xml', $xml->asXML());

        // get /ppt/_rels/presentation.xml.rels and add a new <Relationship Id="rId14" Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide" Target="slides/slide9.xml"/>
        $max_rel_id = 0;
        $xml = simplexml_load_string($this->getMetadata('ppt/_rels/presentation.xml.rels'));
        foreach ($xml->Relationship as $el) {
            $rel_id = preg_replace('/[^0-9]/', '', (string)$el['Id']);
            if ($rel_id > $max_rel_id) $max_rel_id = $rel_id;
            if ((string)$el['Target'] == 'slides/slide' . $after_index . '.xml') {
                $after_slide_rel_id = (string)$el['Id'];
            }
        }
        $slide_rel_id = 'rId' . ($max_rel_id+1);

        $el = $xml->addChild('Relationship');
        $el->addAttribute('Id',$slide_rel_id);
        $el->addAttribute('Type','http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide');
        $el->addAttribute('Target','slides/' . $slide_xml_file);
        $this->setMetadata('ppt/_rels/presentation.xml.rels', $xml->asXML());

        // get ppt/presentation.xml and add a new <p:sldIdLst>  <p:sldId id="284" r:id="rId2"/>
        // easier to traverse with namespaces by dom
        $max_id = 0;
        $found_attr = false;
        $el_after = null; 
        $xml = simplexml_load_string($this->getMetadata('ppt/presentation.xml'));
        $dom = dom_import_simplexml($xml);
        $els = $dom->getElementsByTagName('sldId'); 
        foreach ($els as $el) {
            $id = preg_replace('/[^0-9]/', '', (string)$el->getAttribute('id'));
            if ($id > $max_id) {
                $max_id = $id;
            }
            if ($found_attr === true && empty($el_after)) {
                $el_after = $el;
            }
            if ((string)$el->getAttribute('r:id') == $after_slide_rel_id) {
                $found_attr = true;
            }
        }
        $el = $el_after->parentNode->insertBefore($dom->ownerDocument->createElement('p:sldId', null), $el_after);
        $el->setAttribute('id',($max_id+1));
        $el->setAttribute('r:id', $slide_rel_id);
        $xml = simplexml_import_dom($dom);
        $this->setMetadata('ppt/presentation.xml', $xml->asXML());
    }
    
    public function addMediaFromFile($file, $name = null) {
        if (empty($name)) $name = pathinfo($file, PATHINFO_FILENAME) . '.' . pathinfo($file, PATHINFO_EXTENSION);
        if (file_exists($file) && is_readable($file)) {
            $this->oZip->addFile($file, 'ppt/media/' . $name);
        }
    }

    public function removeMediaFromFile($file) {
        $name = pathinfo($file, PATHINFO_FILENAME) . '.' . pathinfo($file, PATHINFO_EXTENSION);
        if ($this->oZip->getFromName('ppt/media/' . $name)) {
            $this->oZip->deleteName('ppt/media/' . $name);
        }
    }

    public function addMediaFromString($name, $string) {
        $this->oZip->addFromString('ppt/media' . $name, $string);
    }
    
    public function save() {
        foreach ($this->slides as $idx => $slide) {
            $this->oZip->addFromString($slide['name'], $slide['xml']);
        } 
        foreach ($this->metadata as $path => $xml) {
            $this->oZip->addFromString($path, $xml);
        } 
        $this->oZip->close();
    }
}
