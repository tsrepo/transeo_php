<?php
namespace Transeo\Tools\HttpClient;

trait DomParser
{
    public function GetDOMXPath($html = null)
    {
        $dom = $this->GetDOM($html);

        return new \DomXPath($dom);
    }

    public function GetElementContentsByID($element_id, $html = null)
    {
        $el = $this->GetElementByID($element_id, $html);

        return (empty($el)) ? false : $el->ownerDocument->saveHTML($el);
    }

    public function GetElementTextByID($element_id, $html = null)
    {
        $el = $this->GetElementByID($element_id, $html);

        return (empty($el)) ? false : $el->textContent;
    }

    public function GetElementValueByID($element_id, $html = null)
    {
        $el = $this->GetElementByID($element_id, $html);

        return (empty($el)) ? false : $el->getAttribute('value');
    }

    public function GetElementAttributeByID($element_id, $attribute_name, $html = null)
    {
        $el = $this->GetElementByID($element_id, $html);

        return (empty($el)) ? false : $el->getAttribute($attribute_name);
    }

    public function GetElementByID($element_id, $html = null)
    {
        $dom = $this->GetDOM($html);

        $el = $dom->getElementById($element_id);

        return (empty($el)) ? false : $el;
    }

    public function GetElementsByType($element_type, $html = null)
    {
        $dom = $this->GetDOM($html);

        return $dom->getElementsByTagName($element_type);
    }

    public function GetInputElementValueByName($element_name, $html = null)
    {
        $dom = $this->GetDOM($html);

        $els = $dom->getElementsByTagName('input');
        if ($els->length == 0) {
            return false;
        }
        foreach ($els as $el) {
            if ($el->getAttribute('name') == $element_name) {
                return $el->getAttribute('value');
            }
        }

        return false;
    }

    public function GetElementsByTypeByAttribute($element_type, $element_attribute, $element_value, $html = null)
    {
        $dom = $this->GetDOM($html);

        $els = $dom->getElementsByTagName($element_type);
        if ($els->length == 0) {
            return [];
        }

        $return = [];
        foreach ($els as $el) {
            if ($el->getAttribute($element_attribute) == $element_value) {
                $return[] = $el;
            }
        }

        return $return;
    }

    public function GetFormElementAttributeByName($element_name, $attribute_name, $html = null)
    {
        $dom = $this->GetDOM($html);

        $els = $dom->getElementsByTagName('form');
        if ($els->length == 0) {
            return false;
        }
        foreach ($els as $el) {
            if ($el->getAttribute('name') == $element_name) {
                return $el->getAttribute($attribute_name);
            }
        }

        return false;
    }

    public function GetDOMXPathItemAttribute($xpath_query, $attribute_name, $html = null)
    {
        $_el = $this->GetDOMXPathItem($xpath_query, $html);
        if ($_el) {
            return $_el->getAttribute($attribute_name);
        }

        return false;
    }

    public function GetDOMXPathItem($xpath_query, $html = null)
    {
        $xpath = $this->GetDOMXPath($html);

        $query = $xpath->query($xpath_query);

        return $query->item(0);
    }

    public function GetDOMXPathItemList($xpath_query, $html = null)
    {
        $xpath = $this->GetDOMXPath($html);

        $query = $xpath->query($xpath_query);

        return $query;
    }
}