<?php
namespace Transeo\Tools\HttpClient;

class CurlExec extends Exec_Base
{
    public function __construct()
    {
        parent::__construct();
        $this->exec = "curl --header \"Accept-encoding: identity\" --header \"User-Agent: %s\" -o %s \"%s\"";
    }
}