<?php
namespace Transeo\Tools\HttpClient;

use \Transeo\Tools\HttpClient\DomParser as DomParser;

class Exec_Base
{
    use DomParser { }

    public $response;
    public $response_htmlbody;
    public $url;
    public $exec;
    public $path;
    public $exec_evaluated;

    public function __construct()
    {
        $this->path = APPPATH . '/cache/';
    }

    protected function sprintf_exec($ua, $path, $url)
    {
        return sprintf($this->exec, $ua, $path, $url);
    }

    public function URLFetch($url, $post = NULL, $headers = NULL, $debug = FALSE)
    {
        $path = $this->path . \Transeo\Helpers\Strings::randomString(10) . '-' . (microtime(TRUE) * 1000);
        $ua = \Transeo\Tools\Curl::GetRandomUserAgent(2);

        $this->exec_evaluated = $this->sprintf_exec($ua, $path, $url);

        exec($this->exec_evaluated);

        if (!file_exists($path)) {
            return '';
        }

        $fh = fopen($path, 'r');

        $content = '';

        while ($line = fgets($fh)) {
            $content .= $line;
        }

        $this->response = $content;
        $this->response_htmlbody = $content;
        $this->url = $url;

        unset($path);

        return $content;
    }

    public function GetInfo()
    {
        return [
            'http_code' => 200,
        ];
    }

    public function GetError()
    {
        return [];
    }

    public function GetDOM($html = null)
    {
        if (empty($html)) {
            $html = $this->response_htmlbody;
        }

        $dom = new \DOMDocument();
        $dom->strictErrorChecking = false;
        @$dom->loadHTML($html); // don't throw HTML parsing errors

        return $dom;
    }
}