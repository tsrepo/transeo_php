<?php
namespace Transeo\Tools\HttpClient;

class Factory
{
    const ENGINE_TRANSEO_CURL = 'ts_curl';
    const ENGINE_CURL = 'curl';
    const ENGINE_WGET = 'wget';
    const ENGINE_CHROME_HEADLESS = 'chrome_headless';

    public function build($engine = self::ENGINE_TRANSEO_CURL)
    {
        switch ($engine)
        {
            case self::ENGINE_CURL:
                return new CurlExec();
            case self::ENGINE_WGET:
                return new WgetExec();
            case self::ENGINE_CHROME_HEADLESS:
                return new ChromeHeadlessExec();
            case self::ENGINE_TRANSEO_CURL:
            default:
                return new \Transeo\Tools\Curl();
        }
    }
}

