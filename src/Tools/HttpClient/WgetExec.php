<?php
namespace Transeo\Tools\HttpClient;

class WgetExec extends Exec_Base
{
    public function __construct()
    {
        parent::__construct();
        $this->exec = "wget -q --header 'Accept-encoding: identity' -U \"%s\" -O %s \"%s\"";
    }

}
