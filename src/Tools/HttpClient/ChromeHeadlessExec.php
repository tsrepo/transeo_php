<?php
namespace Transeo\Tools\HttpClient;

class ChromeHeadlessExec extends Exec_Base
{
    public function __construct()
    {
        parent::__construct();
        $this->exec = "timeout %s /usr/lib64/chromium-browser/headless_shell --timeout=%s --disable-gpu --disable-plugins --no-sandbox --headless --user-agent=\"%s\" --dump-dom \"%s\" >> %s";
    }

    protected function sprintf_exec($ua, $path, $url, $timeout = 45)
    {
        return sprintf($this->exec, $timeout+2, $timeout*1000, $ua, $url, $path);
    }

}
