<?php

// a wrapper for JQGraph

// to make this work http://localhost/library_php/library-2-0/libraries/jpgraph/Examples/testsuit.php
// you'll need to add http://localhost/library_php/library-2-0/libraries/ to the include path in php.ini
// for linux deployments, don't forget to config and install fonts http://jpgraph.net/download/manuals/chunkhtml/ch03s04.html

// look at other charting libs
//  http://matplotlib.org/index.html
//  http://www.paulhammond.org/webkit2png/
//  

namespace Transeo\Tools;

class Chart
{
    private $_PRIMARY_SERIES_COLOR = '#1c73a5';
    private $_SECONDARY_SERIES_COLOR = '#D9534F';
    private $_SERIES_COLOR_ARRAY = array(0,'#3366cc','#dc3912','#ff9900','#109618','#990099','#0099c6','#dd4477','#aaaa11','#22aa99','#994499');
    private $_LINE_CHART_LINE_WEIGHT = 3;
    private $_AXIS_FONT_SIZE = 14; // 12
    private $_LEGEND_FONT_SIZE = 14; // 12
    private $_DATA_LABEL_FONT_SIZE = 12; //11
    private $_FRAME_RGB = array(210,83,60); // orange

    public $graph;
    public $graph_type  = 'percent';
    public $width       = 1200;
    public $height      = 720;
    public $show_all_datalabels = FALSE; // show datalabels for all series
    public $legend_label_chars = 0;      // length of legend label for positioning right legend 
    public $show_only_datalabel = NULL;

    private $_data_series_count = 1;
    private $_active_plots = array();
    private $_small = FALSE;
    private $_graph_scale_auto = TRUE;
    private $_xaxis_labels = array();
    private $_data_series = array();
    private $_stack_active_plots = FALSE;
    private $_stack_active_plots_type = NULL;
    
    // expects the values in the vals
    public static function format_array_as_date($array, $format = 'M y')
    {
        $_array = $array;
        array_walk($_array, function(&$value, &$key, $format) { $value = date($format,strtotime($value)); }, $format);
        return $_array;
    }
    
    public function __construct()
    {
        require_once 'jpgraph/jpgraph.php';
        require_once 'jpgraph/jpgraph_line.php';
        require_once 'jpgraph/jpgraph_bar.php';
    }
    
    public static function currency_format($num)
    {
        return '$' . number_format($num, 0);
    }

    public static function currency_format2($num)
    {
        return '$' . number_format($num, 2);
    }

    public static function percent_format2($num)
    {
        return number_format($num, 2) . ' %';
    }

    public static function decimal_format($num)
    {
        return number_format($num,1);
    }

    // public gets/sets
    public function SetFrameRGB($rgb = 'orange')
    {
        if (!is_array($rgb)) {
            if (strpos($rgb, '#') !== FALSE) {
                list($r, $g, $b) = sscanf($rgb, "#%02x%02x%02x");
                $rgb = array($r, $g, $b);
            }
            switch ($rgb) {
                case 'green':
                    $rgb = array(84,130,53);
                    break;
                case 'orange':
                    $rgb = array(210,83,60);
                    break;
            }
        }
        $this->_FRAME_RGB = $rgb;
    }

    public function SetSeriesColors($colors = array())
    {
        if (!is_array($colors)) {
            switch ($colors) {
                case 'green':
                    $colors = array(0,'#548235','#454545','#777','#109618','#990099','#0099c6','#dd4477','#aaaa11','#22aa99','#994499');
                    break;
                case 'orange':
                    $colors = array(0,'#3366cc','#dc3912','#ff9900','#109618','#990099','#0099c6','#dd4477','#aaaa11','#22aa99','#994499');
                    break;
                // something with #
                default:
                    $colors = array(0,$colors,'#454545','#777','#109618','#990099','#0099c6','#dd4477','#aaaa11','#22aa99','#994499');
            }
        }
        $this->_SERIES_COLOR_ARRAY = $colors;
    }

    // takes all of the configuration and data and actually creates the chart
    private function _build_chart()
    {
        $this->_initialize_chart();
        
        $this->_setXAxisLabels($this->_xaxis_labels);
        foreach ($this->_data_series as $series) {
            $this->_addDataSeries($series['values'], $series['name'], $series['type'], $series['fillval']);
        }

        if ($this->_stack_active_plots) {
            $this->_StackActivePlots($this->_stack_active_plots_type);
        }
    }

    private function _initialize_chart_scale($values = array())
    {
        // default to auto scale
        $min_scale = $max_scale = 'auto';

        // we might have fixed min and max
        if (strpos($this->graph_type, ':') !== FALSE) {
            $_type = explode(':', $this->graph_type);
            $this->graph_type = $_type[0];
            $_num = explode(',', $_type[1]);
            $min_scale = $_num[0];
            $max_scale = $_num[1];
        }

        $maxval = $minval = 0;
        foreach ($this->_data_series as $data) {
            $_maxval = max($data['values']);
            $maxval = max($_maxval, $maxval);

            $_minval = min($data['values']);
            $minval = min($_minval, $minval);
        }

        switch (substr($max_scale, 0, 1)) {
            // if the max is something like >10, check the values and if its less than 10, use 10, otherwise auto
            case '>':
                $max_scale = substr($max_scale, 1);
                $max_scale = ($maxval > $max_scale) ? 'auto' : $max_scale;
                break;
        }

        $this->_graph_scale_auto = ($max_scale === 'auto' || $min_scale === 'auto');

        // set default max/min for percents, which are special
        if (strpos($this->graph_type, 'percent') !== FALSE) {
            if (!isset($min_scale)) {
                $min_scale = 0;
            }
            if (!isset($max_scale)) {
                $max_scale = 100;
            }

            // if percent and auto scaling, set 0 as a min and 100 as a min max
            if ($max_scale === 'auto') {
                $max_scale = max($maxval, 100);
            }
            if ($min_scale === 'auto') {
                $min_scale = min($minval, 0);
            }
        }

        // jpgraph only supports auto on both bounds, so set these if that's the case
        // but not for percent graph types
        elseif ($max_scale == 'auto' || $min_scale == 'auto') {
            $min_scale = $max_scale = 1;

            // if the whole set is 0, autoscaling fails
            if ($maxval == 0 && $minval == 0) {
                $min_scale = 0;
                $max_scale = 100;
            }

        }

        switch ($this->graph_type) {
            case 'percent':
            case 'percent2':
                $this->graph->SetScale('intlin', $min_scale, $max_scale); // for y-axis
                $this->graph->yaxis->SetLabelFormatCallback('number_format');
                $this->graph->yaxis->SetLabelFormat('%s%%');
                $this->graph->yaxis->SetTextTickInterval(2);
                if ($max_scale - $min_scale > 200) {
                    $this->graph->yaxis->SetTextLabelInterval(intval(($max_scale - $min_scale)/100));
                    $this->graph->yaxis->SetTextTickInterval(intval(($max_scale - $min_scale)/100));
                }
                elseif ($min_scale != $max_scale) {
                    $this->graph->ygrid->Show(true, true);
                }
                break;
            case 'number':
                $this->graph->SetScale('textlin', $min_scale, $max_scale); // for y-axis
                $this->graph->yaxis->SetLabelFormatCallback('number_format');
                break;
            case 'decimal':
                $this->graph->SetScale('textlin', $min_scale, $max_scale); // for y-axis
                $this->graph->yaxis->SetLabelFormatCallback('\Transeo\Tools\Chart::decimal_format');
                break;
            case 'currency':
            case 'currency2':
                $this->graph->SetScale('textlin', $min_scale, $max_scale); // for y-axis
                $this->graph->yaxis->SetLabelFormatCallback('\Transeo\Tools\Chart::currency_format');
                $this->graph->yaxis->SetTextLabelInterval(2);
                break;
        }

        // y-axis settings
        $this->graph->yaxis->SetFont(FF_ARIAL, FS_NORMAL, $this->_AXIS_FONT_SIZE);
        $this->graph->yaxis->SetColor('white', 'black'); // border, font
        $this->graph->yaxis->SetLabelMargin(15);
        $this->graph->ygrid->SetColor('gray', 'lightgray@0.5');
        $this->graph->yaxis->HideFirstTickLabel();
        // $graph->yaxis->title->Set('# of calls');
        
        // set up the legend
        $this->graph->legend->Pos(0.03,0.68); //, "right","bottom"); // absolute x/y, anchor
        $this->graph->legend->SetFont(FF_ARIAL,FS_NORMAL,$this->_LEGEND_FONT_SIZE);
        $this->graph->legend->SetLineSpacing(15);
        $this->graph->legend->SetShadow('white',0); // shadow
        $this->graph->legend->SetColor('black','white'); // font, border
        $this->graph->legend->SetFillColor('white');
        $this->graph->legend->SetLeftMargin(25);
        $this->graph->legend->SetLineWeight(3);

        // override some stuff if its a small graph
        // this has some value/scale dependency, which is why it isn't in initialize()
        if ($this->_small) {
            if ($this->graph_type == 'currency') {
                $left_margin = 55;
            }
            elseif ($this->graph_type == 'percent' && $this->_graph_scale_auto === TRUE) {
                $left_margin = 55;
            }
            else {
                $left_margin = 40;
            }
            $this->graph->yaxis->SetLabelMargin(5);
            $this->graph->SetMargin($left_margin, 25, 15, 20); // left, right, top, bottom
            // $this->graph->xaxis->HideFirstTickLabel();
            $this->graph->legend->hide();
        }
    }

    private function _initialize_chart($values = array())
    {
        // change some things if we have a small chart
        if ($this->width <= 400 && $this->height <= 240) $this->_small = TRUE;
    
        // override some stuff if its a small graph
        if ($this->_small) {
            $this->_AXIS_FONT_SIZE = 9;
        }
    
        // Create the graph and set a scale.
        $this->graph = new \Graph($this->width, $this->height);

        // generic graph settings
        $this->graph->SetMargin(100, ($this->legend_label_chars > 0) ? $this->legend_label_chars*17 : 200, 100, 100); // top, right, bottom, left
        $this->graph->SetMarginColor('white');
        $this->graph->SetFrame(true, $this->_FRAME_RGB, 1);
        // $this->graph->img->SetAntiAliasing();
        // $graph->SetShadow();
        // $graph->title->Set('');
        // $graph->subtitle->Set('');

        // this needs to be init'd before we can do the label config (stupid graphing tool)
        $this->_initialize_chart_scale($values);

        // x-axis settings
        $this->graph->xaxis->SetLabelFormat('M Y', TRUE);
        $this->graph->xaxis->SetFont(FF_ARIAL, FS_NORMAL, $this->_AXIS_FONT_SIZE);
        $this->graph->xscale->ticks->SupressZeroLabel();
        $this->graph->xaxis->HideZeroLabel();
        $this->graph->xaxis->SetPos('min');
        // $this->graph->xaxis->SetLabelAngle(30);
        // $graph->xaxis->scale->SetDateFormat('M Y');
        // $graph->xaxis->title->Set('Operator');
        
    }
    
    public function setXAxisLabels($values) {
        $this->_xaxis_labels = $values;
    }

    private function _setXAxisLabels($values) {
        $this->graph->xaxis->SetTickLabels($values);
    }
    
    public function addDataSeries($values, $name, $type = 'line', $fillval = 0) {
        $this->_data_series[] = array(
            'values'    => $values,
            'name'      => $name,
            'type'      => $type,
            'fillval'   => $fillval,
        );
    }

    private function _addDataSeries($values, $name, $type = 'line', $fillval = 0) {
        // show plotmark if there is only one point on the chart
        $_count = 0;
        $_showdatalabelidx = $this->show_only_datalabel;
        foreach ($values as &$val) {
            if ($fillval !== FALSE && empty($val)) $val = $fillval;
            if ($fillval !== FALSE || !empty($val)) $_count++;
        }

        if ($_showdatalabelidx == 'last') $_showdatalabelidx = ($_count == 1 ? 0 : (sizeof($values)-1));
        $color = $this->_SERIES_COLOR_ARRAY[$this->_data_series_count];
        
        switch ($type) {
            case 'line':
                $plot = new \LinePlot($values);
                $plot->SetWeight($this->_LINE_CHART_LINE_WEIGHT); // px
                $plot->SetColor($color);

                if ($_count == 1) {
                    $plot->mark->SetSize(5);
                    $plot->mark->Show(true);
                    $plot->mark->SetColor($color);
                    $plot->mark->SetFillColor($color);
                    $plot->mark->SetType(MARK_FILLEDCIRCLE);
                }
                
                // show datapoints for the first series of a line chart
                if ($this->_data_series_count == 1 || $this->show_all_datalabels || $_showdatalabelidx) {
                    $plot->value->Show();
                    if ($_showdatalabelidx !== NULL) {
                        $plot->value->ShowOnlyPoint($_showdatalabelidx);
                    }
                    if ($this->show_all_datalabels) {
                        $plot->value->SetColor($color); // same as series color
                    }
                    else {
                        $plot->value->SetColor("#444444"); // dark-ish gray
                    }
                    $plot->value->SetFont(FF_ARIAL,FS_NORMAL,$this->_DATA_LABEL_FONT_SIZE);

                    // if we're showing only a specific data label, move the data label x=35
                    // otherwise, keep it at x=0
                    // alternate y between 25 and -25
                    $_y = $this->_small ? -20 : (($this->_data_series_count % 2 == 0 ? -1 : 1)*20);
                    $_x = $this->_small ? -20 : (isset($_showdatalabelidx) ?  35 : 0);
                    $plot->value->SetMargin($_y,$_x);

                    switch ($this->graph_type) {
                        case 'percent':
                            $plot->value->SetFormat('%.0f %%');
                            break;
                        case 'percent2':
                            $plot->value->SetFormatCallback('\Transeo\Tools\Chart::percent_format2');
                            break;
                        case 'number':
                            $plot->value->SetFormatCallback('number_format');
                            break;
                        case 'currency':
                            $plot->value->SetFormatCallback('\Transeo\Tools\Chart::currency_format');
                            break;
                        case 'currency2':
                            $plot->value->SetFormatCallback('\Transeo\Tools\Chart::currency_format2');
                            break;
                    }
                }
                break;
            case 'bar':
            case 'stackedbar':
                $plot = new \BarPlot($values);
                $plot->SetFillColor($color);
                $plot->SetColor('gray');
                break;
        }
        
        $plot->SetLegend($name);

        switch ($type) {
            case 'stackedbar':
                $this->_active_plots[] = $plot;
                break;
            default:
                $this->graph->Add($plot);
                break;
        }
        
        $this->_data_series_count++;
    }
    
    public function StackActivePlots($type = 'stackedbar') {
        $this->_stack_active_plots = TRUE;
        $this->_stack_active_plots_type = $type;
    }

    private function _StackActivePlots($type = 'stackedbar') {
        switch ($type) {
            case 'stackedbar':
                $plot = new \AccBarPlot($this->_active_plots);
                break;
        }
        
        $this->graph->Add($plot);
        $this->_active_plots = array();
    }
            
    public function saveImage($file) {
        $this->_build_chart();

        if (file_exists($file)) unlink($file);
        return $this->graph->Stroke($file);
    }
}
