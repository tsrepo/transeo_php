DROP TABLE IF EXISTS `dim_time`;
CREATE TABLE `dim_time` (
	`id` int(11) NOT NULL,
	`db_date` date NOT NULL,
	`year` int(11) NOT NULL,
	`month` int(11) NOT NULL,
	`day` int(11) NOT NULL,
	`quarter` int(11) NOT NULL,
	`week` int(11) NOT NULL,
	`day_name` varchar(9) NOT NULL,
	`month_name` varchar(9) NOT NULL,
	`holiday_flag` char(1) DEFAULT 'f',
	`weekend_flag` char(1) DEFAULT 'f',
	`event` varchar(50) DEFAULT NULL,
	`wk_date_sun_fmt` char(7) DEFAULT NULL,
	`wk_date_mon_fmt` char(7) DEFAULT NULL,
	`month_start_dt` date DEFAULT NULL,
	`day_of_week` tinyint(3) unsigned DEFAULT NULL,
	`wk_start_date_sun` date DEFAULT NULL,
    
	PRIMARY KEY (`id`),
	UNIQUE KEY `td_ymd_idx` (`year`,`month`,`day`),
	UNIQUE KEY `td_dbdate_idx` (`db_date`),
    
	KEY `wk_date_sun_fmt` (`wk_date_sun_fmt`),
	KEY `wk_date_mon_fmt` (`wk_date_mon_fmt`),
	KEY `wk_start_date_sun` (`wk_start_date_sun`),
	KEY `w,d` (`wk_date_sun_fmt` ASC, `db_date` ASC),
	KEY `month_start_dt` (`month_start_dt`),
    KEY `m,d` (`month_start_dt` ASC, `db_date` ASC)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP PROCEDURE IF EXISTS fill_date_dimension;
DELIMITER //
CREATE PROCEDURE fill_date_dimension(IN startdate DATE,IN stopdate DATE)
BEGIN
    DECLARE currentdate DATE;
    SET currentdate = startdate;
    WHILE currentdate < stopdate DO
        INSERT INTO dim_time VALUES (
                        YEAR(currentdate)*10000+MONTH(currentdate)*100 + DAY(currentdate),
                        currentdate,
                        YEAR(currentdate),
                        MONTH(currentdate),
                        DAY(currentdate),
                        QUARTER(currentdate),
                        WEEKOFYEAR(currentdate),
                        DATE_FORMAT(currentdate,'%W'),
                        DATE_FORMAT(currentdate,'%M'),
                        'f',
                        CASE DAYOFWEEK(currentdate) WHEN 1 THEN 't' WHEN 7 then 't' ELSE 'f' END,
                        NULL,
                        DATE_FORMAT(db_date, '%XW%V'),
						DATE_FORMAT(db_date, '%xW%v'),
                        DATE_FORMAT(db_date, '%Y-%m-01'),
                        DAYOFWEEK(db_date),
                        DATE_ADD(db_date, INTERVAL -day_of_week+1 DAY)
		);
        SET currentdate = ADDDATE(currentdate,INTERVAL 1 DAY);
    END WHILE;
END
//
DELIMITER ;

TRUNCATE TABLE dim_time;

CALL fill_date_dimension('2000-01-01','2100-01-01');
OPTIMIZE TABLE dim_time;