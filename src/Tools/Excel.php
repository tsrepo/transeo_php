<?php

namespace Transeo\Tools;

// deprecate internal usage of excel parsing in favor of either phpspreadsheet or openbox
class Excel
{
    public $oZip;
    public $worksheet_count;
    public $worksheets = array();

    private $_sharedStrings;
    private $_filename;
    
    public function __construct($file)
    {
        $this->oZip = new \ZipArchive();
        $this->_filename = $file;

        if ($this->oZip->open(realpath($file))) {
            
            $this->_sharedStringsArray = json_decode(json_encode(simplexml_load_string($this->oZip->getFromName('xl/sharedStrings.xml'), 'SimpleXMLElement', LIBXML_COMPACT | LIBXML_PARSEHUGE)), TRUE);

            // sometimes a ref doesn't have a value in it
            foreach ($this->_sharedStringsArray['si'] as $idx => $ref) {
                if (!isset($ref['t'])) {
                    $this->_sharedStringsArray['si'][$idx]['t'] = '';
                }
            }
            $this->_sharedStringsArray = \Transeo\Helpers\ArrayDatasets::extractValuesAsArray($this->_sharedStringsArray['si'], 't');

            // and sometimes a weird array shows up
            foreach($this->_sharedStringsArray as $key => $val) {
                if (is_array($val)) {
                    $this->_sharedStringsArray[$key] = '';
                }
            }


            for ($i = 0; $i < $this->oZip->numFiles; $i++) {
                $stat = $this->oZip->statIndex($i);
                if (strpos($stat['name'], 'xl/worksheets/sheet') !== FALSE) {
                    $this->worksheet_count++;
                }
            }
            for ($i = 0; $i < $this->worksheet_count; $i++) {
                $this->worksheets[$i+1] = array(
                    'name'  => 'xl/worksheets/sheet' . ($i+1) . '.xml',
                    'xml'   => $this->oZip->getFromName('xl/worksheets/sheet' . ($i+1) . '.xml'),
                    'csv'   => $this->_generateWorksheetCSV($this->oZip->getFromName('xl/worksheets/sheet' . ($i+1) . '.xml')),
                );
            }
        }
        else {
            new \Exception('Cannot open file');
        }
    }

    // #TODO upgrade to openspout/openspout:v3
    // https://github.com/openspout/openspout
    public static function largeCSVToArray($csv_data, $params)
    {
        $ignore_blank_lines = $params['ignore_blank_lines'] ?? TRUE;
        $header_start_line = $params['header_start_line'] ?? 1;
        $data_start_line = $params['data_start_line'] ?? 2;
        $file_type = $params['file_type'] ?? 'csv';
        $worksheet_filter = $params['worksheet_name_filter'] ?? NULL;

        switch ($file_type) {
            case 'csv':
                $reader = \Box\Spout\Reader\Common\Creator\ReaderEntityFactory::createCSVReader();
                break;
            case 'xlsx':
                $reader = \Box\Spout\Reader\Common\Creator\ReaderEntityFactory::createXLSXReader();
                break;
            case 'ods':
                $reader = \Box\Spout\Reader\Common\Creator\ReaderEntityFactory::createODSReader();
                break;
        }
        
        $file = stream_get_meta_data(tmpfile());
        $fh = fopen($file['uri'], 'w');
        fwrite($fh, $csv_data);

        $csv_data = [];
        $headers = [];

        $reader->open($file['uri']);

        foreach ($reader->getSheetIterator() as $sheet) {
            if (!empty($worksheet_filter) && strpos($sheet->getName(), $worksheet_filter) !== 0) {
                echo $sheet->getName();
                continue;
            }

            foreach ($sheet->getRowIterator() as $row_idx => $row) {
                if ($row_idx < $header_start_line) {
                    continue;
                }

                $headers = $row->toArray();

                // make headers usable
                foreach ($headers as $key => $val) {
                    $val = str_replace("\n", '', $val);
                    $val = str_replace("\r", '', $val);

                    if (empty(trim($val))) {
                        unset($headers[$key]);
                        continue;
                    }

                    $headers[$key] = $val;
                }

                break;
            }

            foreach ($sheet->getRowIterator() as $row_idx => $row) {
                if ($row_idx < $data_start_line) {
                    continue;
                }

                $cells = $row->toArray();

                $_row = [];
                $has_value = FALSE;

                foreach ($cells as $col => $val) {
                    if (!empty($val)) {
                        $has_value = TRUE;
                    }
                    if (empty($headers[$col])) {
                        continue;
                    }

                    if (!empty($_row[$headers[$col]])) {
                        if (!is_array($_row[$headers[$col]])) {
                            $_row[$headers[$col]] = [$_row[$headers[$col]]];
                        }
                        $_row[$headers[$col]][] = $val;
                    }
                    else {
                        $_row[$headers[$col]] = $val;
                    }
                }

                if (!$ignore_blank_lines || $has_value) {
                    $csv_data[] = $_row;
                }
            }
        }

        $reader->close();
        
        return $csv_data;
    }

    // uses PhpSpreadsheet, specifically to handle removal of line breaks inside header rows
    public static function spreadsheetToArray($csv_data, $params)
    {
        $ignore_blank_lines = $params['ignore_blank_lines'] ?? TRUE;
        $header_start_line = $params['header_start_line'] ?? 1;
        $data_start_line = $params['data_start_line'] ?? 2;
        $worksheet_filter = $params['worksheet_name_filter'] ?? NULL;
        $lowercase_headers = $params['lowercase_headers'] ?? FALSE;

        $file = stream_get_meta_data(tmpfile());
        $fh = fopen($file['uri'], 'w');
        fwrite($fh, $csv_data);

        $spreadsheet_data = [];

        if (!empty($cache)) {
            \PhpOffice\PhpSpreadsheet\Settings::setCache($cache);
        }
        
        $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file['uri']);

        try {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($file['uri'], ['Xlsx','Xls','Ods','Csv']);
        }
        catch (Exception $e) {
            return FALSE;
        }
        
        $reader->setReadDataOnly(TRUE);
        $spreadsheet = $reader->load($file['uri']);

        $worksheets = $reader->listWorksheetInfo($file['uri']);

        foreach ($worksheets as $worksheet) {
            if (!empty($worksheet_filter) && strpos($worksheet['worksheetName'], $worksheet_filter) !== 0) {
                continue;
            }

            $headers = $spreadsheet->getSheetByName($worksheet['worksheetName'])
                ->rangeToArray(
                    'A' . $header_start_line . ':' . $worksheet['lastColumnLetter'] . $header_start_line,     // The worksheet range that we want to retrieve
                    NULL,        // Value that should be returned for empty cells
                    FALSE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                    FALSE,       // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                    TRUE         // Should the array be indexed by cell row and cell column
                )[$header_start_line];
                
            // make headers usable
            foreach ($headers as $key => $val) {
                $val = str_replace("\n", '', $val);

                if (empty(trim($val))) {
                    unset($headers[$key]);
                    continue;
                }

                if ($lowercase_headers === TRUE) {
                    $headers[$key] = strtolower($val);
                }
                else {
                    $headers[$key] = $val;
                }
            }

            // and real data starts wherever we were told
            $data = $spreadsheet->getSheetByName($worksheet['worksheetName'])
                ->rangeToArray(
                    'A' . $data_start_line . ':' . $worksheet['lastColumnLetter'] . $worksheet['totalRows'],     // The worksheet range that we want to retrieve
                    NULL,        // Value that should be returned for empty cells
                    FALSE,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                    FALSE,       // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                    TRUE         // Should the array be indexed by cell row and cell column
                );

            $sheet_data = [];

            foreach ($data as $row) {
                $_row = [];
                $has_value = FALSE;

                foreach ($row as $col => $val) {
                    if (!empty($val)) {
                        $has_value = TRUE;
                    }
                    if (empty($headers[$col])) {
                        continue;
                    }

                    if (!empty($_row[$headers[$col]])) {
                        if (!is_array($_row[$headers[$col]])) {
                            $_row[$headers[$col]] = [$_row[$headers[$col]]];
                        }
                        $_row[$headers[$col]][] = $val;
                    }
                    else {
                        $_row[$headers[$col]] = $val;
                    }
                }

                if (!$ignore_blank_lines || $has_value) {
                    $sheet_data[] = $_row;
                }
            }

            $spreadsheet_data[$worksheet['worksheetName']] = $sheet_data;
        }

        return $spreadsheet_data;
    }

    public static function csvToArray($csv_data, $ignore_blank_lines = TRUE)
    {
        $rows = str_getcsv($csv_data, "\n");

        $data = [];
        $header = NULL;

        foreach ($rows as $row) {
            if ($ignore_blank_lines === TRUE && empty($row)) {
                continue;
            }
            
            $row = str_getcsv($row, ',');

            if (!isset($header)) {
                $header = $row;
                continue;
            }

            foreach ($row as $idx => $val) {
                $_data[$header[$idx]] = $val;
            }

            $data[] = $_data;
        }

        return $data;
    }

    public static function tabToArray($csv_data, $ignore_blank_lines = TRUE)
    {
        $rows = str_getcsv($csv_data, "\n");

        $data = [];
        $header = NULL;

        foreach ($rows as $row) {
            if ($ignore_blank_lines === TRUE && empty($row)) {
                continue;
            }
            
            $row = str_getcsv($row, "\t");

            if (!isset($header)) {
                $header = $row;
                continue;
            }

            foreach ($row as $idx => $val) {
                $_data[$header[$idx]] = $val;
            }

            $data[] = $_data;
        }

        return $data;
    }

    public static function getColumnIndex($coordinate, $zero_index = FALSE)
    {
        $coordinate = preg_replace('/[^A-Z]/', '', strtoupper($coordinate));
        $chars = str_split(strrev($coordinate));
        $index = 0;
        $ord_idx = 0;
        foreach ($chars as $char) {
            if ($ord_idx == 0) {
                $index += (ord($char) - 64);
            }
            else {
                $index += (pow(26, $ord_idx)*(ord($char) - 64));
            }

            $ord_idx++;
        }
        return$zero_index ? $index-1 : $index;
    }

    public static function getColumnCoordinate($number)
    {
        $n = $number;
        for($r = ""; $n >= 0; $n = intval($n / 26) - 1) {
            $r = chr($n%26 + 0x41) . $r;
        }
        return $r;
    }

    public static function getColumnsInbetween($coorA, $coordB)
    {
        $coordA = preg_replace('/[^A-Z]/', '', strtoupper($coordA));
        $coordB = preg_replace('/[^A-Z]/', '', strtoupper($coordB));
    }
    
    private function _generateWorksheetCSV($xml)
    {
        $csv = NULL;

        $spreadsheet_array = json_decode(json_encode(simplexml_load_string($xml)), TRUE);

        if (empty($spreadsheet_array['sheetData']['row'])) {
            return '';
        }
        
        foreach ($spreadsheet_array['sheetData']['row'] as $idx => &$row) {
            $strings = array();

            $row_blank_cols = 0;

            // sometimes there's no c at all
            if (!empty($row['c'])) {
                // if there's only one column in the row, there isn't an array of columns (dumb)
                $keys = array_keys($row['c']);
                if (sizeof($row['c']) === 2 && reset($keys) === '@attributes') {
                    $row['c'] = array($row['c']);
                }

                foreach ($row['c'] as $col_idx => $row_data) {
                    if (!isset($row_data['@attributes']['r'])) {
                        continue;
                    }
                    $coordinate = $row_data['@attributes']['r'];
                    $coordinate_idx = Excel::getColumnIndex($coordinate, TRUE);
                    if ($col_idx+$row_blank_cols < $coordinate_idx) {
                        $missing = $coordinate_idx-$col_idx-$row_blank_cols;
                        for ($i=0; $i<$missing; $i++) {
                            $row_blank_cols++;
                            $strings[] = '';
                        }
                    }

                    if (isset($row_data['@attributes']['t']) && $row_data['@attributes']['t'] == 's') {
                        $strings[] = str_replace('"', '""', $this->_sharedStringsArray[$row_data['v']]);
                    }
                    elseif (isset($row_data['v'])) {
                        $str = is_array($row_data['v']) ? implode('', $row_data['v']) : $row_data['v'];
                        $strings[] = str_replace('"', '""', $str);
                    }
                    else {
                        $strings[] = '';
                    }
                }
            }
            
            if (!empty($strings)) {
                $csv[] = '"' . implode('","', $strings) . '"';
            }
        }
        return $csv;
    }

    public function getWorksheet($index)
    {
        return $this->worksheets[$index]['xml'];
    }

    public function getWorksheetCSVArray($index)
    {
        return $this->worksheets[$index]['csv'];
    }

    public function getWorksheetCSV($index)
    {
        return implode(PHP_EOL, $this->getWorksheetCSVArray($index));
    }

    public function getFilename()
    {
        return $this->_filename;
    }
}
