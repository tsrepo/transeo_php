<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * APIController interface
 *
 * @abstract 
 * @copyright RetailDataPartners
 */
interface IAPIController {
    public function get($id, $params = []);
    public function search($params);
    public function patch($id, $data);
    public function post($data);
    public function delete($id);
}

