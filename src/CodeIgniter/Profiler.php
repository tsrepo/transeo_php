<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

// Extend default CI_Profiler to add sections that display:
// - all of the php files that have been included during execution
// - all log_message() messages. technically not ALL, but all log_message called AFTER CI_Controller is loaded
class Profiler extends \CI_Profiler
{
    public function __construct($config = array())
    {
        // add our sections
        $this->_available_sections[] = 'files';
        $this->_available_sections[] = 'logs';

        parent::__construct();
    }

    protected function _compile_files() // @codingStandardsIgnoreLine, CI naming convention required for this method
    {
        $output = $this->render_heading('files', 'INCLUDED FILES');

        $files = get_included_files();
        sort($files);

        foreach ($files as $file) {
            $output .= '<tr><td style="vertical-align:top;width:50%;padding:5px;color:#900;background-color:#ddd;">'.$file."&nbsp;&nbsp;</td></tr>\n";
        }

        return $output."</table>\n</fieldset>";
    }

    protected function _compile_logs() // @codingStandardsIgnoreLine, CI naming convention required for this method
    {
        $output = $this->render_heading('logs', 'LOG MESSAGES');

        $logs = Log::get_logs();

        foreach ($logs as $log) {
            $output .= '<tr>
                            <td style="vertical-align:top;width:50%;padding:5px;color:#900;background-color:#ddd;">'.$log['level'].'&nbsp;&nbsp;</td>
                            <td style="vertical-align:top;width:50%;padding:5px;color:#900;background-color:#ddd;">'.$log['msg'].'&nbsp;&nbsp;</td>
                        </tr>';
        }

        return $output."</table>\n</fieldset>";
    }

    private function render_heading($section, $title)
    {
        return "\n\n"
                    .'<fieldset id="ci_profiler_'.$section.'" style="border:1px solid #000;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee;">'
                    ."\n"
                    .'<legend style="color:#000;">&nbsp;&nbsp;'.$title.'&nbsp;&nbsp;'
                    .'(<span style="cursor: pointer;" onclick="var s=document.getElementById(\'ci_profiler_'.$section
                    ."_table').style;s.display=s.display=='none'?'':'none';this.innerHTML=this.innerHTML=='"
                    .$this->CI->lang->line('profiler_section_show')."'?'".$this->CI->lang->line('profiler_section_hide')."':'"
                    .$this->CI->lang->line('profiler_section_show').'\';">'.$this->CI->lang->line('profiler_section_show')."</span>)</legend>\n\n\n"
                    .'<table style="width:100%;display:none;" id="ci_profiler_'.$section.'_table">'."\n";
    }
}
