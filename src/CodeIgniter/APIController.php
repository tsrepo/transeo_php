<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') or exit('No direct script access allowed');

use Transeo\CodeIgniter\AuthController;
use Firebase\JWT\JWT;

// general format
                // http://jsonapi.org/

// expanding    // reference the API Controller (i.e. purchase_order) that you want to link to
                // RELATIONSHIP in the main model must be defined for it

// includes     // reference the API Controller (i.e. purchase_order) that you want to include in the results
                // RELATIONSHIP in the INCLUDED model must be defined for the current entity
                // this will create sub-objects of the linked objects


// filtering    // http://discuss.jsonapi.org/t/share-propose-a-filtering-strategy/257
                // but adopting the facebook format of field.operator(value)
                // as in order_id.gt(100) for order_id > 100

// expects
                // controller to be in /controllers/api/{whatever}
                // controller to have a MODEL const that's the actual {thing_model} name
                // model to have API_COLUMN_MAP const that maps columns to returned data elements
                // model to have RELATIONSHIPS const if it has them
                // model to have api_by_params and api_cont_by_params methods (can be from core model)
                // model to have an const API_ENTITY_TYPE

class APIController extends Controller
{
    use AuthController {
        AuthController::__construct as authConstruct;
    }

    protected $input_data;
    protected $format;
    protected $access_log_id = NULL;
    public $public_api_user = NULL;

    // no user needed, but at least a jwt token needed.
    protected const OPEN_PATHS = [];

    // no login or token needed whatsoever
    protected const NO_AUTH_PATHS = [];

    // general format for api calls is
    //      /api/{method}?apikey={key}&apisecret={secret}

    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

            exit(0);
        }
        
        parent::__construct();

        $this->input_data = file_get_contents('php://input');
        $querystring = $_SERVER['QUERY_STRING'] ?? '';
        parse_str($querystring,$querystring);

        if (strpos(($_SERVER['CONTENT_TYPE'] ?? ''), 'multipart/form-data') === 0) {
            $this->input_data = $this->input->post('json');
        }
        // do some basic validation
        if (!empty($this->input_data)) {
            try {
                $this->input_data = json_decode($this->input_data, TRUE);

                // if there's raw data and form data, merge the two
                // raw data params will supercede the form data
                if (!empty($querystring)) {
                    $this->input_data = array_merge($querystring, is_array($this->input_data) ? $this->input_data : []);
                }
            }
            catch (Exception $e) {
                $this->render_error(['Invalid JSON input']);
            }
        }
        else {
            $this->input_data = $querystring;
        }

        if ($this->is_no_auth_endpoint()) {
            return;
        }
        
        $this->load->library('ion_auth');
		$this->load->model('custom_ion_auth_model');

        $auth_header = $this->input->get_request_header('Authorization');

        if (!empty($this->input_data['apikey']) && !empty($this->input_data['apisecret'])) {
            $this->custom_ion_auth_model->login(
                $this->input_data['apikey'],
                $this->input_data['apisecret']
            );
        }
        else if (empty($this->custom_ion_auth_model->session->userdata('identity'))) {
            if (empty($auth_header)) {
                $this->render_error(['API authentication required']);
            }
            
            $parts = explode(' ', $auth_header);

            if (count($parts) != 2) {
                $this->render_error(['API authentication required']);
            }

            $auth_header_type = strtolower($parts[0]);

            if ($auth_header_type == 'bearer') {
                $key = $this->config->item('api')['secret'];
                $token = $parts[1];
                try {
                    $payload = (array) JWT::decode($token, $key, array('HS256'));
                }
                catch (\Firebase\JWT\ExpiredException $e) {
                    $this->render_error(['API authentication expired']);
                }
                catch (\Exception $e) {
                    $this->render_error(['API authentication required']);
                }
                if ($payload['iss'] != base_url()) {
                    $this->render_error(['Token not issued by this site.']);
                }
                if (empty($payload['aud'])) {
                    $this->render_error(['Token does not have an audience.']);
                }

                if (!empty($payload['usr']) && $payload['aud'] == base_url()) {
                    $this->custom_ion_auth_model->set_api_user($payload['usr']);
                }
                else {
                    $this->public_api_user = TRUE;
                }
            }
            elseif ($auth_header_type == 'basic') {
                if (empty($parts[1])) {
                    $this->render_error(['API authentication header invalid']);
                }

                $token = $parts[1];
                $token = base64_decode($token);
                $token = explode(':', $token, 2);

                if (count($token) != 2) {
                    $this->render_error(['API authentication header invalid']);
                }

                $this->custom_ion_auth_model->login(
                    $token[0],
                    $token[1],
                );
            }
            else {
                $this->render_error(['API authentication header invalid']);
            }
        }

        // gotta do this down here so that it sets the local vars appropriately
        if (empty($this->public_api_user) || $this->is_login_required()) {
            $this->authConstruct();

            if ($this->is_authenticated !== TRUE) {
                $this->render_error(['API authentication required']);
            }
        }

        // remove api auth info from payload
        unset($this->input_data['apikey']);
        unset($this->input_data['apisecret']);
    }

    // return TRUE if the current request needs a user
    protected function is_login_required()
    {
        // prefix paths with class name
        $prefix = strtolower(get_class($this)) . '\/';
        $paths = [];
        $paths = static::OPEN_PATHS;
        foreach (static::OPEN_PATHS as $key => $values) {
            $paths[$key] = preg_filter('/^/', $prefix, $values);
        }

        return !$this->is_endpoint_in_paths($paths);
    }

    // return TRUE if current request requres no auth whatsoever
    protected function is_no_auth_endpoint()
    {
        // prefix paths with class name
        $prefix = strtolower(get_class($this)) . '\/';
        $paths = static::NO_AUTH_PATHS;
        foreach (static::NO_AUTH_PATHS as $key => $values) {
            $paths[$key] = preg_filter('/^/', $prefix, $values);
        }

        return $this->is_endpoint_in_paths($paths);
    }

    private function is_endpoint_in_paths($path_patterns)
    {
        $actual_path = strtolower($this->router->class . '/' . $this->router->method);
        $method = strtoupper($_SERVER['REQUEST_METHOD']);
        if (!empty($path_patterns[$method])) {
            foreach ($path_patterns[$method] as $path_pattern) {
                if (preg_match('/' . $path_pattern . '/', $actual_path) === 1) {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }

    /**
     *  Conforms to RFC 7519 (https://datatracker.ietf.org/doc/html/rfc7519)
     *  Private claim names:
     *    usr:  the username of the logged in user. (OPTIONAL)
     */ 
    protected function generate_access_token($credentials = [], $params = [])
    {
        $key = $this->config->item('api')['secret'];

        $refresh_token = FALSE;
        $this->load->library('ion_auth');
        $this->load->model('custom_ion_auth_model');
        if (!empty($credentials['apikey']) && !empty($credentials['apisecret'])) {
            $refresh_token = $this->custom_ion_auth_model->login($credentials['apikey'], $credentials['apisecret'], 'api');
            if (!empty($refresh_token)) {
                $params['usr'] = $credentials['apikey'];
                $params['aud'] = base_url();
            }
            else {
                $this->render_error(['Invalid credentials.']);
            }
        }
        else if (!empty($credentials['refresh_token']) && $credentials['apikey']) {
            $success = $this->custom_ion_auth_model->api_login_by_refresh_token($credentials['apikey'], $credentials['refresh_token']);
            if ($success) {
                $params['usr'] = $credentials['apikey'];
                $params['aud'] = base_url();
            }
            else {
                $this->render_error(['Invalid refresh token for the given identity.']);
            }
        }

        $payload = $this->create_access_token_payload($params);
        
        $jwt = JWT::encode($payload, $key);
        
        $response = [
            'tokens'     => ['access_token'  => $jwt],
            'expires_in' => (int)$this->config->item('api')['jwt_valid_seconds']
        ];
        if (!empty($refresh_token)) {
            $response['tokens']['refresh_token'] = $refresh_token;
        }
        $this->render_json($response, 200);
        die;
    }

    protected function create_access_token_payload($params)
    {
        $payload = array(
            'iss' => base_url(),
            'iat' => time(),
            'exp' => strtotime('+' . ((int)$this->config->item('api')['jwt_valid_seconds']) . ' seconds')
        );

        $payload = array_merge($params, $payload);

        return $payload;
    }

    protected static function validate_class($class)
    {
        $class = ucfirst($class);

        // if the api entity doesn't exist, skip
        if (!class_exists($class)) {
            return FALSE;
        }

        return $class;
    }

    protected static function validate_api_entity($class)
    {
        $class = self::validate_class($class);

        if ($class === FALSE) {
            return FALSE;
        }

        // if the api entity doesn't exist, skip
        if (!defined($class . '::MODEL')) {
            return FALSE;
        }

        // if the api entity model is bad, bail
        if (self::validate_api_model($class::MODEL) === FALSE) {
            return FALSE;
        }

        return $class;
    }

    protected static function validate_api_model($class)
    {
        $class = self::validate_class($class);

        // if model doesn't exist, die
        if ($class === FALSE) {
            return FALSE;
        }

        // if the model of the api entity doesn't exist, skip
        if (!defined($class . '::TABLE_NAME') || !defined($class . '::API_COLUMN_MAP') || !defined($class . '::API_ENTITY_TYPE')) {
            return FALSE;
        }

        if (!method_exists($class, 'api_by_params') || !method_exists($class, 'api_count_by_params')) {
            return FALSE;
        }

        return $class;
    }

    protected static function map_error_object($ex)
    {
        if (is_string($ex)) {
            return (object) [
                'code'      => 0,
                'message'   => $ex,
                'reason'    => $ex,
            ];
        }
        elseif (is_array($ex)) {
            return (object) [
                'code'      => isset($ex['code']) ? $ex['code'] : 0,
                'message'   => $ex['message'],
                'reason'    => isset($ex['reason']) ? $ex['reason'] : $ex['message'],
            ];
        }
        elseif (is_object($ex)) {
            return $ex;
        }
    }

    public function render_error($errors)
    {
        $this->render([], FALSE, $errors);
    }

    public function render_data($data, $total)
    {
        $this->render(['data' => $data, 'meta' => ['count' => $total]]);
    }

    public function render($data = [], $success = TRUE, $errors = [])
    {
        $json['success'] = $success;
        $json['errors'] = $errors;
        $json['meta'] = [
            'method' => strtoupper($_SERVER['REQUEST_METHOD']),
            'uri' => uri_string()
        ] + $this->input_data;

        if (!empty($data['meta'])) {
            $json['meta'] += $data['meta'];
        }
        $json += $data;

        foreach ($errors ?? [] as &$error) {
            $error = self::map_error_object($error);
        }
        
        if (file_exists(APPPATH . '/controllers/api/version')) {
            $json['version'] = file_get_contents(realpath(APPPATH . '/controllers/api/version'));
            $json['timestamp'] = time();
            $json['logid'] = $this->access_log_id;
        }

        $this->render_json($json);

        die();
    }

    // override this so no other remapping happens
    public function _remap($method, $params = [])
    {
        // stupid codeigniter is passing different $method and $params() depending on the root level of the app
        // screw that noise
        $params = (array)$this->uri->segments;
        if (!isset($params[1]) || $params[1] !== 'api') {
            $this->render_error(['API URI is invalid']);
        }

        $request_method = strtoupper($_SERVER['REQUEST_METHOD']);

        $method = $params[2];
        unset($params[1]);
        unset($params[2]);
        $params = array_values($params);
        // end codeigniter douchebaggery

        try {
            $search_params = new \Transeo\Helpers\URIEntityParsing();
            $search_params->setFromInputArray($this->input_data);

            $search_params = self::parse_params($search_params->toArray());
        }
        catch (Exception $e) {
            $this->render_error([$e->getMessage()]);
        }

        switch ($request_method) {
            // POSTs go to "post" if no params or the most granular method if params
            case 'POST':
                if (!empty($params)) {
                    $method = strtolower($params[sizeof($params)-1]);
                }
                else {
                    $method = strtolower($request_method);
                }

                $params = [$this->input_data];

                break;

            // always in the format of method/1
            case 'DELETE':
                $method = strtolower($request_method);
                break;

            case 'PATCH':
                if (count($params) !== 1 || !is_numeric($params[0])) {
                    $this->render_error(['API format is invalid']);
                }

                $method = strtolower($request_method);
                $params = [$params[0], $this->input_data];

                break;

            case 'GET':
                // for things like /channel/1
                if (count($params) === 1 && is_numeric($params[0])) {
                    $method = 'get';
                    $params = [$params[0], $search_params];
                }

                // for custom things like /channel/1/start
                // the method signature of the receiving method MUST match exactly to the number of params sent
                elseif (count($params) >= 2 && is_numeric($params[0])) {
                    $method = $params[1];
                    unset($params[1]);
                }

                // for things like /channel
                elseif (count($params) === 0) {
                    $method = 'search';
                    $params = [$search_params];
                }

                // for custom things like /channel/composite_view
                elseif (count($params) === 1) {
                    $method = $params[0];
                    $params = [$search_params];
                }
                
                break;

            default:
                $this->render_error(['API method is invalid']);
        }

        $method = strtolower($method);

        if (!method_exists($this, $method))
        {
            $this->render_error(['API method is invalid']);
        }

        if (method_exists($this, $method))
        {
            if ((new \ReflectionMethod($this, $method))->getNumberOfRequiredParameters() > sizeof($params)) {
                $this->render_error(['API method number of parameters is invalid']);
            }
        }

        return call_user_func_array(array($this, $method), $params);
    }

    public static function get_field_alias($field)
    {
        if (strpos($field, '.') !== FALSE) {
            $parts = explode('.', $field, 2);
            $table = $parts[0];
            $field = $parts[1];
        }
        else {
            $table = strtolower(get_called_class());
        }

        $class = self::validate_api_entity($table);
        if ($class === FALSE) {
            return FALSE;
        }
        $model = self::validate_api_model($class::MODEL);

        // and that the column map has this field in it
        if (!array_key_exists($field, $model::API_COLUMN_MAP)) {
            return FALSE;
        }

        // if the column map has params, respect them
        if (is_array($model::API_COLUMN_MAP[$field]) && array_key_exists('alias', $model::API_COLUMN_MAP[$field])) {
            return $model::API_COLUMN_MAP[$field]['alias'];
        }
        elseif (is_array($model::API_COLUMN_MAP[$field])) {
            // something else
        }
        else {
            $field_name = $model::TABLE_NAME . '.' . $model::API_COLUMN_MAP[$field];
            return $field_name;
        }

    }

    // if the field name is fully qualified {table}.{field}
    // then we'll replace the table name with the join source
    // otherwise, we'll drop it
    public static function map_field($field, $alias = TRUE)
    {
        if (strpos($field, '.') !== FALSE) {
            $parts = explode('.', $field, 2);
            $table = $parts[0];
            $field = $parts[1];
        }
        else {
            $table = strtolower(get_called_class());
        }

        $class = self::validate_api_entity($table);

        if ($class === FALSE) {
            return FALSE;
        }

        $model = self::validate_api_model($class::MODEL);

        // and that the column map has this field in it
        if (!array_key_exists($field, $model::API_COLUMN_MAP)) {
            return FALSE;
        }

        // if the column map has params, respect them
        if (is_array($model::API_COLUMN_MAP[$field]) && array_key_exists('alias', $model::API_COLUMN_MAP[$field])) {
            return $model::API_COLUMN_MAP[$field]['source'] . 
                    ($alias === TRUE ? ' AS `' . $model::API_COLUMN_MAP[$field]['alias'] . '`' : '');
        }
        elseif (is_array($model::API_COLUMN_MAP[$field])) {
            // something else
        }
        else {

            $field_name = $model::TABLE_NAME . '.' . $model::API_COLUMN_MAP[$field];
            if ($alias === FALSE) {
                return $field_name;
            }

            return $model::TABLE_NAME . '.' . $model::API_COLUMN_MAP[$field] . ' AS `' . $field_name . '`';

        }
    }

    // generic search requires
    // inheriting controller to set $model
    // model to have methods
    //      api_by_params
    //      api_count_by_params
    //      map_api_response
    // model to have const
    //      API_COLUMN_MAP
    protected function parse_params($params)
    {
        if (defined('static::MODEL')) {
            $model = self::validate_api_model(static::MODEL);
            if ($model === FALSE) {
                $this->render_error(['API model could not be loaded']);
         
                return;
            }

            $this->load->model(strtolower($model));
            $this->{strtolower($model)}->format = 'array';

        }

        $errors = [];

        $_joins = [];
        if (!empty($params['expand']) && is_array($params['expand'])) {
            foreach ($params['expand'] as $join) {
                $join = self::validate_api_entity($join);

                if (
                    $join === FALSE
                    || !defined($model . '::RELATIONSHIPS')
                    || !array_key_exists(strtolower($join::MODEL), $model::RELATIONSHIPS)
                ) {
                    $errors[] = $join . ' is not a valid entity to expand';
                    continue;
                }

                $_joins[] = $join::MODEL;
            }
        }
        $params['joins'] = $_joins;

        $_includes = [];
        if (!empty($params['include']) && is_array($params['include'])) {
            foreach ($params['include'] as $join) {
                $join = self::validate_api_entity($join);

                if (
                    $join === FALSE
                    || !defined($model . '::RELATIONSHIPS')
                    || !array_key_exists(strtolower($model), ($join::MODEL)::RELATIONSHIPS)
                ) {
                    $errors[] = $join . ' is not a valid entity to include';
                    continue;
                }

                $_includes[] = $join;
            }
        }
        $params['includes'] = $_includes;

        // replace api field names with db field names
        $_filters = [];
        if (!empty($params['filters'])) {
            foreach ($params['filters'] as $filter) {
                $filter['alias'] = $this->get_field_alias($filter['field']);
                $_field = $this->map_field($filter['field'], FALSE); // don't alias the names
                if (empty($_field)) {
                    $errors[] = $filter['field'] . ' is not a valid field to filter on';
                    continue;
                }

                $filter['field'] = $_field;
                $_filters[] = $filter;
            }
        }
        $params['filters'] = $_filters;

        // replace api field names with db field names
        $_fields = [];
        if (!empty($params['fields']) && is_array($params['fields'])) {
            foreach ($params['fields'] as $field) {
                $field = trim($field);

                $_field = $this->map_field($field);
                if (empty($_field)) {
                    $errors[] = $field . ' is not a valid field';
                    continue;
                }

                $_fields[] = $_field;
            }
        }
        $params['fields'] = $_fields;

        $_groups = [];
        if (!empty($params['group']) && is_array($params['group'])) {
            foreach ($params['group'] as $field) {
                $field = trim($field);

                $_field = $this->map_field($field, FALSE);
                if (empty($_field)) {
                    $errors[] = $field . ' is not a valid field';
                    continue;
                }

                $_groups[] = $_field;
            }
        }
        $params['group'] = $_groups;

        // replace api order names with db field names
        $_order = [];
        if (!empty($params['order']) && is_array($params['order'])) {
            foreach ($params['order'] as $field) {
                if (strpos($field, '!') === 0) {
                    $order = 'DESC';
                    $field = substr($field, 1);
                }
                else {
                    $order = 'ASC';
                }

                $_field = $this->map_field($field, FALSE);
                if (empty($_field)) {
                    $errors[] = $field . ' is not a valid field';
                    continue;
                }

                $_order[] = [$_field, $order];
            }
        }
        $params['order'] = $_order;

        if (!empty($errors)) {
            $this->render_error($errors);
        }

        return $params;
    }

    protected function get($id, $params = [])
    {
        if (!defined('static::MODEL')) {
            $this->render_error(['API model is not defined']);
            return;
        }

        $model = static::MODEL;

        $this->load->model($model);

        if (empty($this->{$model})) {
            $this->render_error(['API model could not be loaded']);
            return;
        }

        $data = $this->{$model}->get_data_by_id($id);

        if ($data === FALSE) {
            $this->render_error(['Invalid ID']);
        }

        $model = static::MODEL;

        $pk = $this->{$model}->primary_key;

        $params['filters'][] = [
            'field'     => $model::TABLE_NAME . '.' . $pk,
            'operator'  => '=',
            'value'     => $id,
        ];

        $data = $this->{$model}->api_by_params($params, 'array');

        if ($data === FALSE) {
            $this->render_error(['ID not found']);
        }

        $results = static::map_api_response($data, $params['joins'] ?? [], $params);
        $results = reset($results);

        if (!empty($params['includes'])) {
            $local_model = strtolower($model);
            foreach ($params['includes'] as $include) {
                $class = self::validate_api_entity($include);

                if ($class === FALSE) {
                    continue;
                }

                $include_model = self::validate_api_model($class::MODEL);

                $this->load->model($include_model);

                // we have to assume that the link is on our id
                $include_table_col = is_array($include_model::RELATIONSHIPS[$local_model]) ? key($include_model::RELATIONSHIPS[$local_model]) : $include_model::RELATIONSHIPS[$local_model];
                $table_col = is_array($include_model::RELATIONSHIPS[$local_model]) ? $include_model::RELATIONSHIPS[$local_model][$include_table_col] : $include_model::RELATIONSHIPS[$local_model];
                $api_col = array_search($table_col, $model::API_COLUMN_MAP);
                if ($api_col && !isset($results[$api_col])) {
                    continue;
                }
                $data = $this->{$include_model}->api_by_params(['filters' => [[
                    'operator'  => '=',
                    'field'     => $include_table_col,
                    'value'     => $results[$api_col],
                ]]], 'array');

                $results[$include_model::API_ENTITY_TYPE] = $include::map_api_response($data);
            }
        }

        if (($params['return'] ?? FALSE) == TRUE) {
            return $results;
        }

        $this->render(['data' => $results]);
    }

    protected function search($params)
    {
        if (!defined('static::MODEL')) {
            $this->render_error(['API model is not defined']);
            return;
        }

        $model = static::MODEL;

        $this->load->model($model);

        if (empty($this->{$model})) {
            $this->render_error(['API model could not be loaded']);
            return;
        }

        $data = $this->{$model}->api_by_params($params);
        $count = NULL;
        if (!in_array('no_count', $params['adjustments'] ?? [])) {
            $count = $this->{$model}->api_count_by_params($params);
        }

        if ($data === FALSE) {
            $this->render_data([], 0);
        }

        // rekey our results by pk so that we can match them to the includes
        $data = \Transeo\Helpers\ArrayDatasets::rekeyAsDataset($data, $model::TABLE_NAME . '.' . $this->{$model}->primary_key);

        $include_data = [];
        $include_results = [];

        if (!empty($params['includes'])) {
            $local_model = strtolower($model);
            $sample_data = current($data);
            foreach ($params['includes'] as $include) {
                $class = self::validate_api_entity($include);

                if ($class === FALSE) {
                    continue;
                }

                $include_model = self::validate_api_model($class::MODEL);

                $this->load->model($include_model);

                // we have to assume that the link is on our id
                $include_table_col = is_array($include_model::RELATIONSHIPS[$local_model]) ? key($include_model::RELATIONSHIPS[$local_model]) : $include_model::RELATIONSHIPS[$local_model];
                $table_col = is_array($include_model::RELATIONSHIPS[$local_model]) ? $include_model::RELATIONSHIPS[$local_model][$include_table_col] : $include_model::RELATIONSHIPS[$local_model];
                $table_col = $model::TABLE_NAME . '.' . $table_col;

                if (!isset($sample_data[$table_col])) {
                    continue;
                }

                $include_data = $this->{$include_model}->api_by_params(['filters' => [[
                    'operator'  => 'in',
                    'field'     => $include_table_col,
                    'value'     => array_column($data, $table_col),
                ]]]);

                $include_results[$include_model::API_ENTITY_TYPE] = \Transeo\Helpers\ArrayDatasets::rekeyAsDatasetArray($include_data, $include_table_col);
            }
        }

        $results = [];
        
        foreach ($data as $table_id => $row) {
            $_result = static::map_api_response([$row], $params['joins'] ?? [], $params);

            // ignore empty rows that got filtered out
            if (empty($_result)) {
                continue;
            }

            $_result = $_result[0];
            foreach ($include_results as $type => $include_rows) {

                $class = self::validate_api_entity($type);

                if ($class === FALSE) {
                    continue;
                }
                $include_model = self::validate_api_model($class::MODEL);
                $table_col = (is_array($include_model::RELATIONSHIPS[$local_model]) ? $include_model::RELATIONSHIPS[$local_model][$include_table_col] : $include_model::RELATIONSHIPS[$local_model]);
                $table_col = $model::TABLE_NAME . '.' . $table_col;
                
                if (empty($row[$table_col])) {
                    continue;
                }
                $_result[$type] = $type::map_api_response($include_rows[$row[$table_col]] ?? [], [], $params);
            }

            $results[] = $_result;
        }

        if (($params['return'] ?? FALSE) == TRUE) {
            return [
                'results' => $results,
                'count'   => $count
            ];
        }
        
        $this->render_data($results, $count);
    }

    protected function patch($id, $data)
    {
        // get the existing data
        $model = static::MODEL;

        $this->load->model($model);

        if (empty($this->{$model})) {
            $this->render_error(['API model could not be loaded']);
            return;
        }

        if ($this->{$model}->get_data_by_id($id) === FALSE) {
            $this->render_error(['Invalid ID']);
        }
        
        $this->{$model}->set_data_by_id($id, $data);
        
        $data = $this->{$model}->get_data_by_id($id);
        
        $this->render(['data' => $data]);
    }

    protected function post($data)
    {
        // get the existing data
        $model = static::MODEL;

        $this->load->model($model);

        if (empty($this->{$model})) {
            $this->render_error(['API model could not be loaded']);
            return;
        }

        $id = $this->{$model}->insert_data($data);

        $data = $this->{$model}->get_data_by_id($id);

        if ($data === FALSE) {
            $this->render_error(['Error POSTing']);
        }

        $this->render(['data' => $data]);
    }

    // default method that maps a dataset to an api response object set
    // can now be used from patch, post, etc.
    // can be overridden in a controller
    protected static function map_api_response($_data, $joins = [], $params = NULL)
    {
        $model = static::MODEL;

        if (empty($_data)) {
            return [];
        }

        $result = [];

        // map all of our fields
        foreach ($_data as $row) {
            $_result = $model::map_api_response($row, $params);
            if ($_result === FALSE) {
                continue;
            }

            // add the auto-expanded models so that the relevant fields get mapped
            if (defined($model . '::API_AUTO_EXPAND')) {
                $joins = array_unique(array_merge($joins, $model::API_AUTO_EXPAND));
            }

            if (!empty($joins)) {
                foreach ($joins as $child) {
                    $class = ucfirst($child);
                    if (!class_exists($class) || !defined($class . '::TABLE_NAME') || !defined($class . '::API_COLUMN_MAP') || !defined($class . '::API_ENTITY_TYPE')) {
                        continue;
                    }

                    $child_table = $child::TABLE_NAME;

                    // all child / expand fields will always start with [table_name].
                    // so we filter the row to those fields and map_api_request handles mapping them to the api keys
                    $_result[$child::API_ENTITY_TYPE][] = $child::map_api_response(
                        array_filter(
                            $row,
                            function($key) use ($child_table) {
                                return (strpos($key, $child_table) === 0);
                            },
                            ARRAY_FILTER_USE_KEY
                        )
                    );
                }
            }

            $result[] = $_result;
        }
        return $result;
    }
}