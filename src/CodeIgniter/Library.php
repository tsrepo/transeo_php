<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

abstract class Library
{
    protected $errors = [];

    // Allows library to access CI's loaded classes using the same syntax as controllers.
    // e.g. $this->load->model('') will work now
    public function __get($key)
    {
        return get_instance()->$key;
    }

    public function set_error($error)
    {
        $this->errors[] = $error;

        return $error;
    }

    public function errors()
    {
        $output = '';

        foreach ($this->errors as $error) {
            $message = $this->lang->line($error) ? $this->lang->line($error) : '##' . $error . '##';
            $output .= '<p>' . $message . '</p>';
        }

        return $output;
    }
}
