<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller trait that adds authentication and authorization functionality provided by ion_auth
 */
trait AuthController
{
    public $is_authenticated = FALSE;
    public $is_admin = FALSE;
    public $user;
    public $user_groups = [];

    private static $sensitive_user_fields = ['password', 'salt', 'forgotten_password_code', 'activation_code', 'remember_code'];

    public function __construct()
    {
        $this->load->library('ion_auth');
        $this->load->model('custom_ion_auth_model');

        $this->is_authenticated = $this->custom_ion_auth_model->recheck_session();

        if ($this->is_authenticated) {
            $user_id = $this->custom_ion_auth_model->api_user ? $this->custom_ion_auth_model->api_user->id : NULL;
            // remove sensitive fields like password
            $this->user = (object) array_diff_key((array) $this->ion_auth->user($user_id)->row(), array_flip(self::$sensitive_user_fields));
            if (isset($this->user->id)) {
                $this->user_groups = \Transeo\Helpers\ArrayDatasets::extractValuesAsArray($this->ion_auth_model->get_users_groups($this->user->id)->result('array'), 'name');
            }

            $this->is_admin = in_array('admin', is_array($this->user_groups) ? $this->user_groups : []);
        }
    }

    public function authorize_admin()
    {
        if (!$this->is_admin)
        {
            $this->show_not_authorized();
        }
    }

    public function authorize_group($group = '', $redir = TRUE)
    {
        if ($this->is_admin) {
            return TRUE;
        }
        if (empty($group) || empty($this->user_groups) || !in_array($group, $this->user_groups)) {
            if ($redir) {
                $this->show_not_authorized();
            }
            else {
                return FALSE;
            }
        }
        else {
            return TRUE;
        }
    }

    public function show_not_authenticated()
    {
        show_error('Not authenticated. You need to login.', 401);
    }

    public function show_not_authorized()
    {
        if (!$this->is_authenticated) {
            $this->show_not_authenticated();
        }

        show_error('Not authorized. You are not allowed to perfom this action.', 403);
    }
}
