<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

use League\Plates\Engine;

/**
 * Controller trait that adds Plates PHP functionality
 */
trait PlatesController
{
    protected $templates;

    public function __construct()
    {
        $this->templates = new Engine(APPPATH.'views');
    }

    public function render($template, $data = [])
    {
        echo $this->templates->render($template, $data);
    }

    public function load_errors($errors = NULL)
    {
        if (validation_errors()) {
            $this->templates->addData([
                'message' => validation_errors(),
                'message_style' => 'danger'
            ]);
        }

        if (!empty($errors)) {
            $this->templates->addData([
                'message' => $errors,
                'message_style' => 'danger'
            ]);
        }
    }
}
