<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Extends CI_Config, checking the database first to see if its there
 *
 */
class Config extends \CI_Config
{
	public function __construct()
    {
        parent::__construct();
    }

	// this takes the default signature of $item, $index and changes it to $item, $scope, so sub-index configs are no longer supported
    public function item($item, $scope = '')
	{
		if (!class_exists('Configurations_model') || !class_exists('CI_Model')) {
			return parent::item($item, $scope);
		}

		$ci->load->model('configuration');

		if ($index == '')
		{
			$val = $ci->configuration_model->get_global_value_by_key($item);

			return !empty($val) ? $val : parent::item($item, $scope);
		}

		$val = $ci->configuration_model->get_value_by_key($scope, $item);

		return !empty($val) ? $val : parent::item($item, $scope);
	}
}