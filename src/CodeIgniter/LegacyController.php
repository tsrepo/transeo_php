<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller trait that adds legacy _tpl templating
 */
trait LegacyController
{
    // if set to TRUE, will ensure that no headers, footers, or other crap is written with the response
    public $is_raw = FALSE;
    public $tpl_data = [];
    public $post_data = [];
    public $get_data = [];
    public $form_data = [];

    public function __construct()
    {
        // default to a blank array so that we can use += for adding new data in local controllers
        if (empty($this->tpl_data)) {
            $this->tpl_data = [];
        }

        $this->querystring = isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
        parse_str($this->querystring,$this->get_data);
        
        if (!empty($_POST)) {
            $this->post_data = $_POST;
        }

        $this->form_data = array_merge($this->get_data, $this->post_data);

    }

    /**
     * Method to load the "view" and add private template variables to the
     * public template variables.
     *
     * @param string $tpl_name Path to the template file.
     * @param array $private_tpl_data Array of private template variables.
     *
     * @return void
     */
    public function tpl($tpl_name, $private_tpl_data = [])
    {
        if (empty($this->tpl_data)) {
            $this->tpl_data = [];
        }

        if (empty($private_tpl_data)) {
            $private_tpl_data = [];
        }

        $this->load->vars($this->tpl_data + $private_tpl_data);

        if (!$this->is_raw) {
            $this->load->view('common/site_header_tpl');
        }

        $this->load->view($tpl_name);

        if (!$this->is_raw) {
            $this->load->view('common/site_footer_tpl');
        }
    }

    /**
     * Method to return CSV to client
     *
     * @param array|object $data Data that will be returned
     */
    public function render_csv($tpl_name, $data, $filename = NULL)
    {
        if (empty($filename)) {
            $filename = 'export.csv';
        }

        // $filename = preg_replace('/[^a-zA-Z0-9]/', '', $filename);

        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Pragma: no-cache');
        header('Expires: 0');

        if (empty($this->tpl_data)) {
            $this->tpl_data = [];
        }

        $this->load->view($tpl_name, $this->tpl_data + $data);

        die();
    }
}
