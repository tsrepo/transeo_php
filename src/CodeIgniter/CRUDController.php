<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Controller trait that adds auto-crud as a manage method
 * Override the model name in the controller class with a const MODEL = 'Model', without the _model suffix
 */

trait CRUDController
{
    public function manage($crud = NULL)
    {
        $model_name = defined('self::MODEL') ? self::MODEL : (get_class($this) . 's_model');
        $model_name = ucfirst($model_name);
        $controller = str_replace('_model', '', $model_name);
        $table_name = $model_name::TABLE_NAME;
        $title = str_replace('_', ' ', ucfirst($controller));

        $crud = is_object($crud) ? $crud : (new \grocery_CRUD());
        // $crud->set_theme('bootstrap-v4');

        $this->load->model($model_name);

        $crud
            ->set_table($table_name)
            ->set_subject($title)
        ;

        if (defined($model_name.'::GROCERY_CRUD_COLUMN_TYPES')) {
            foreach ($model_name::GROCERY_CRUD_COLUMN_TYPES as $column_name => $column_type_details) {
                if ($column_type_details === 'bool') {
                    $crud->field_type($column_name, 'true_false', ['0' => 'No', '1' => 'Yes']);
                    $this->set_boolean_ajax_values($crud, [$column_name]);
                }
            }
        }

        $output = $crud->render();
        $output->title = $title;

        $this->render('templates/crud', (array) $output);
    }
    
    /**
     * Allows column to be searched in ajax list by Yes/No
     */
    public function set_boolean_ajax_values($crud, $columns)
    {
        if (!is_array($_POST['search_field'] ?? '')) {
            // non-column-specific search used
            // we can't change the search for only bool columns so bail
            return;
        }

        foreach ($columns as $column) {
            // hack to convert ajax search to db value.  
            // not sure why, but this can't work like a bool col when the column name has the table name in it
            if ($crud->getState() == 'ajax_list') {
                $idx = array_search($column, $_POST['search_field']);
                if ($idx !== FALSE && isset($_POST['search_text'][$idx])) {
                    if (strtolower($_POST['search_text'][$idx]) == 'yes') {
                        $_POST['search_text'][$idx] =  1;
                    }
                    else if (strtolower($_POST['search_text'][$idx]) == 'no') {
                        $_POST['search_text'][$idx] =  0;
                    }
                }
            }
        }
    }

    /**
     * Set Yes/No for a renamed column and allows it to work in ajax search.
     */
    public function set_renamed_boolean_column($crud, $column_name, $alias)
    {
        $crud
            // 
            ->callback_column($alias, function($value, $row) use ($column_name) {
                return $row->$column_name ? 'Yes' : 'No';
        });

        $this->set_boolean_ajax_values($crud, [$alias]);
    }
}
