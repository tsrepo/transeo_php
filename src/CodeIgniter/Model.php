<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends \CI_Model
{
    /**
     *  Init & set defaults
     */
    public $db_table        = '';
    public $primary_key     = '';
    public $format          = 'array'; // controls if get methods return an assoc array or an object
    public $form_validation = [];
    
    public $page_number = NULL;
    public $page_size = NULL;
    public $total_rows = NULL;
    public $total_pages = NULL;

    protected $paging_enabled = FALSE;
    protected $view_name = NULL;

    // supported observer events, more to come....
    // each contains an array of method names to be called when the event fires
    public $before_insert = [];     // called before data is inserted into db, passes argument $data that can be modified & returned
    public $before_get = [];        // called before every get_() CI DB query is executed, can add to query via $this->db

    public static $valid_columns = [];
        
    // --------------------------------------------------------------------

    /**
     *  Model class constructor.
     *
     *  @author mfountain
     */
    public function __construct()
    {
        parent::__construct();
      
        $this->initialize_table_data();
    }
    
    /**
     *  This sets valid columns and primary key based on $this->db_table
     *  Abstracted because if the table name is changed for a model, this needs to be reset
     */
    public function initialize_table_data($reset = FALSE)
    {
        if ($reset) {
            unset(static::$valid_columns);
            unset($this->primary_key);
        }
        
        // an array of the fields in this model/table are necessary for the default
        // update/insert statements, but we don't want to have to manually delcare them in every model
        // so if we haven't declared them...
        if (!isset(static::$valid_columns) || empty(static::$valid_columns)) {
            // first, try to get them from validation rules, which should be declared in any model that gets input from a controller (or more)
            if (isset($this->form_validation)) {
                static::$valid_columns = $this->get_valid_fields_from_form_validation_rules($this->form_validation);
            }
        }
        
        if ((!isset(static::$valid_columns) || empty(static::$valid_columns)) && !empty($this->db_table)) {
            // second, try to get them from list of fields in the table
            // but only if we have a table name to work with

            if ($this->db->table_exists($this->db_table)) {
                $fields = $this->db->list_fields($this->db_table);
            } else {
                $fields = $this->db->list_fields($this->prefix_table($this->db_table));
            }
            static::$valid_columns = $fields;
        }

        // if the primary key for this model isn't set, try to set it manually if we have a table name
        if ((!isset($this->primary_key) || empty($this->primary_key)) && !empty($this->db_table)) {
            if ($this->db->table_exists($this->db_table)) {
                $this->primary_key = $this->db->primary($this->db_table);
            }
            else {
                $this->primary_key = $this->db->primary($this->prefix_table($this->db_table));
            }
        }

        // otherwise, don't do a damn thing
    }
   
    // --------------------------------------------------------------------
    // CREATE METHODS
    // --------------------------------------------------------------------

    /**
     *  generic method to handle inserting a new record
     *
     *  @author mfountain
     *
     *  @param  array   The assoc array of data to insert
     *  @return mixed   The last insert id or False on failure
     */
    public function insert_data($data = [])
    {
        $data = $this->trigger('before_insert', $data);

        $data = $this->remove_invalid_columns($data);

        if (empty($data)) {
            return FALSE;
        }

        if (!$this->db->insert($this->db_table, $data)) {
            return FALSE;
        }
        
        return $this->db->insert_id();
    }

    // an alias for easier discovery
    public function insert($data = [])
    {
        return $this->insert_data($data);
    }

    public function insert_batch($data = [])
    {
        if (empty($data) || !is_array($data)) {
            return FALSE;
        }
        
        foreach ($data as $key => &$item) {
            $item = $this->trigger('before_insert', $item);
            
            $item = $this->remove_invalid_columns($item);

            if (empty($item)) {
                unset($data[$key]);
            }
        }

        return $this->db->insert_batch($this->db_table, $data);
    }

    // --------------------------------------------------------------------
    // GET METHODS
    // --------------------------------------------------------------------

    /**
     * generic method to count the number of records in a table
     *
     * @return  int     The number of records
     */
    public function count_records()
    {
        return $this->db->count_all($this->db_table);
    }
                
    /**
     * generic method to handle getting a single record's data
     *
     * @param   int         $id     The primary key's $id
     *
     * @return  array|bool          row or FALSE
     */
    public function get_data_by_id($id)
    {
        return $this->get_row_by_filters([
            $this->primary_key => (int) $id
        ]);
    }

    /**
     * generic method to handle getting a single row that meets simple field filters
     *
     * @param   array       $filters    Array of field => values
     *
     * @return  array|bool              row or FALSE
     */
    public function get_row_by_filters($filters = [])
    {
        $query = $this->get_data_core($filters);

        if ($query->num_rows() > 1) {
            return FALSE;
        }

        return $this->get_first_row_or_false($query);
    }

    /**
     * Paginates the data returned in the next ->get_data_by_filters() or ->get_all() call
     *
     * @param   int         $page_number    Page number of data you desire
     * @param   int         $page_size      Number of rows per page of data
     *
     * @return  object                      Returns $this model, inteded to be chained with ->get_data_by_filters()
     *                                      or ->get_all()
     */
    public function paginate($page_number = 1, $page_size = 10)
    {
        // set local variables which will trigger pagination next time
        // ->get_data_by_filters() or any dervived methods are called
        $this->page_number = $page_number;
        $this->page_size = $page_size;
        $this->paging_enabled = TRUE;

        // return this model so user can chain ->get_data_by_filters(), or ->get_all(), etc
        return $this;
    }

    /**
     * Tells the next ->get_*() call to query from the supplied $view_name instead of $db_table
     *
     * @param   string  $view_name      (optional) Name of the alternate view to pull data from
     *
     * @return  object                  Returns $this model, inteded to be chained with any of the ->get_*() methods
     */
    public function from_view($view_name = NULL)
    {
        $this->view_name = $view_name ?? $this->db_table . '_expanded';

        return $this;
    }

    /**
     * generic method to handle getting data that meets simple field filters
     *
     * @param  array    $filters    (optional) Array of field => values
     * @param  string   $order_by   (optional) order by statement, NULL for default ordering
     *
     * @return array|bool           Array of rows, or FALSE
     */
    public function get_data_by_filters($filters = [], $order_by = NULL)
    {
        $query = $this->get_data_core($filters, $order_by);

        return $this->get_data_or_false($query);
    }

    // shitty alias for getting all data w/ no filters
    public function get_all()
    {
        return $this->get_data_by_filters();
    }

    public function get_all_for_dropdown($callback = NULL)
    {
        $data = $this->get_all();

        if ($data === FALSE) {
            return FALSE;
        }

        $dropdown = array();

        // if no field or callback specified, display primary key as fallback :(
        if (is_null($callback)) {
            $callback = $this->primary_key;
        }

        if (is_string($callback)) {
            $callback = function ($val) use ($callback) {
                return $val[$callback];
            };
        }

        foreach ($data as $key => $value)
        {
            $dropdown[$value[$this->primary_key]] = $callback($value);
        }

        return $dropdown;
    }

    /**
     * Common get_data logic used by every get_*() method, making adding functionality easier
     *
     * @param array     $filters    (optional) Array of where filters
     * @param string    $order_by   (optional) Order by string, NULL for default ordering
     *
     * @return CI_DB_result
     */
    protected function get_data_core($filters = [], $order_by = NULL)
    {
        if ($this->paging_enabled) {
            // start query caching because we are going to run same query twice
            // once w/ no paging to count total_rows and another w/ paging in place
            $this->db->start_cache();
        }

        $this->db->from($this->view_name ?? $this->db_table);

        foreach($filters as $key => $value) {
            if ($value === NULL) {
                $this->db->where($key, NULL, FALSE);
            } else {
                $this->db->where($key, $value);
            }
        }

        if (!empty($order_by)) {
            $this->db->order_by($order_by);
        }

        if ($this->paging_enabled) {
            $this->db->stop_cache();
            
            $this->set_total_rows($this->db->count_all_results());

            $this->db->limit($this->page_size, ($this->page_number - 1) * $this->page_size);
        }

        $this->trigger('before_get');

        $query = $this->db->get();

        // echo $this->db->last_query();

        if ($this->paging_enabled) {
            $this->db->flush_cache();
        }

        return $query;
    }


    // --------------------------------------------------------------------
    // SET & UPDATE METHODS
    // --------------------------------------------------------------------
   
   
    /**
     *  generic method to handle updating a single record's data
     *
     *  @author mfountain
     *
     *  @param  array   An array of name=value pairs for the where clause
     *  @param  array   The assoc array of data to update
     *
     *  @return bool    True on success, false on failure
     */
    public function set_data($where, $data = [])
    {
        $data = $this->remove_invalid_columns($data);

        if (empty($data)) {
            return FALSE;
        }

        return $this->db->update($this->db_table, $data, $where);
    }
    
    /**
     *  generic method to handle updating a single record's data by ID
     *
     *  @author swc
     *  @date   5/17/2013
     *
     *  @param  mixed   primary key id to update
     *  @param  array   The assoc array of data to update
     *
     *  @return boolean True on success, false on failure
     */
    public function set_data_by_id($id, $data = [])
    {
        $where = array(
            $this->primary_key => (int) $id
        );
      
        return $this->set_data($where, $data);
    }

    // alias for set_data_by_id & set_data
    public function update($id, $data)
    {
        if (is_array($id)) {
            return $this->set_data($id, $data);
        }

        return $this->set_data_by_id($id, $data);
    }

    /**
     *
     *  Insert array of data on update on duplicate key.  Batches into updates of 100
     *  REQUIRES that each row of data has the same keys
     *  Supports either providing a table name, or if not provided, will use the db->table_name
     *
     *  @param  string  the name of the table to act on; if empty, will take the current table and assume the first param is the data     *  @param  array   an associative array of rows=>fields=>values to insert
     *  @return bool    TRUE if update was successful
     */
    public function upsert_batch($table = '', $set = NULL, $db = NULL, $batch_size = 100)
    {
        if (is_array($table) && $set === NULL) {
            $set = $table;
            $table = $this->db_table;
        }

        if (empty($db)) {
            $db = $this->db;
        }

        for ($i = 0, $total = count($set); $i < $total; $i = $i + $batch_size)
        {
            $sql = NULL;
            $cols = NULL;

            $rows = array_slice($set, $i, $batch_size);

            foreach ($rows as $row) {
                // set the SQL insert statement for this batch of $batch_size
                if (!isset($sql)) {
                    $cols = array_keys($row);
                    $escaped_cols = [];
                    foreach ($cols as $col) {
                        $escaped_cols[] = $db->protect_identifiers($col);
                    }
                    $sql = 'INSERT INTO ' . $db->protect_identifiers($table) . ' (' . implode(', ', $escaped_cols) . ') VALUES ';
                }

                $escaped_vals = [];
                foreach ($row as $key => $val)
                {
                    $escaped_vals[] = $db->escape($val);
                }

                $sql .= '(' . implode(', ', $escaped_vals) . '), ';
            }

            switch ($db->dbdriver) {
                case 'mysqli':
                case 'mysql':
                    $sql = trim($sql, ', ') . ' ON DUPLICATE KEY UPDATE ';
                    foreach ($escaped_cols as $col) {
                        $sql .= $col . ' = VALUES(' . $col . '), ';
                    }
                    break;
                case 'postgre':
                case 'postgres':
                    // we assume in postgres that the nk name will be
                    // schema.table_nk
                    $sql = trim($sql, ', ') . ' ON CONFLICT ON CONSTRAINT "' . $table . '_nk" DO UPDATE SET';
                    foreach ($escaped_cols as $col) {
                        $sql .= $col . ' = excluded.' . $col . ', ';
                    }
                    break;
            }
            $sql = trim($sql, ', ') . ';';
        
            $db->query($sql);
        }

        return TRUE;
    }

    /**
     *
     *  Insert with update on duplicate key
     *
     *  @param  string  the name of the table to act on; if empty, will take the current table and assume the first param is the data
     *  @param  array   an associative array of fields=>values to insert
     *  @param  array   an array of keys fields=>values to update
     *
     *  @return bool    TRUE if update was successful
     */
    public function upsert($table = '', $insert_data = NULL, $update_data = NULL)
    {
        if (is_array($table)) {
            $update_data = $insert_data;
            $insert_data = $table;
            $table = $this->db_table;
        }

        if (empty($insert_data)) {
            return FALSE;
        }

        if (empty($update_data)) {
            $update_data = $insert_data;
        }

        $sql = $this->db->insert_string($table, $insert_data) . ' ON DUPLICATE KEY UPDATE ';
        
        foreach ($update_data as $col => $val) {
            $col = $this->db->protect_identifiers($col);
            $sql .= $col . ' = VALUES(' . $col . '), ';
        }

        $sql = trim($sql, ', ') . ';';

        return $this->db->query($sql);
    }

    /**
     *
     *  Insert with update on duplicate key, and return inserted/updated primary key
     *
     *  @param  array         an associative array of fields=>values to insert
     *  @param  array         an associative array of fields=>values to update
     *  @param  array         an array of keys fields=>values to update
     *  @param  string|array  the name of the key(s) the upsert is based on 
     *
     *  @return int           The primary key of the inserted or updated row
     */
    public function upsert_get_id($keys, $insert_data, $update_data = NULL)
    {
        if (!$this->upsert($this->db_table, $insert_data, $update_data)) {
            return FALSE;
        }

        $last_id = $this->db->insert_id();
        if (empty($last_id)) {
            if (!is_array($keys)) {
                $keys = [$keys];
            }
            $filters = [];
            foreach ($keys as $key) {
                $filters[$key] = $insert_data[$key];
            }
            $this->db->select([$this->primary_key]);
            $row = $this->get_row_by_filters($filters);
            $last_id = $row[$this->primary_key] ?? FALSE;
        }

        return $last_id;
    }

    /**
     *
     *  Inserts data and on primary key conflict, will delete existing row and insert a new one
     *
     *  @param  array   an associative array of fields=>values to insert
     *
     *  @return bool    TRUE if replace was successful
     */
    public function replace($data = [])
    {
        $data = $this->remove_invalid_columns($data);

        if (empty($data)) {
            return FALSE;
        }

        return $this->db->replace($this->db_table, $data);
    }
    

    // --------------------------------------------------------------------
    // DELETE METHODS
    // --------------------------------------------------------------------

    /**
     *  generic method to handle deleting records by $where filters
     *
     *  @author swc
     *  @date   5/17/2013
     *
     *  @param  array   assoc array of field->values to filter by
     *
     *  @return bool    True on success, false on failure
     */
    public function delete_data_by_filters($where)
    {
        return $this->db->delete($this->db_table, $where);
    }

    // alias for delete_data_by_filters
    public function delete_data($where)
    {
        return $this->delete_data_by_filters($where);
    }

    /**
     *  generic method to handle deleting a single record's data by ID
     *
     *  @author swc
     *  @date   5/17/2013
     *
     *  @param  mixed   primary key id to delete
     *
     *  @return boolean True on success, false on failure
     */
    public function delete_data_by_id($id)
    {
        $where = array(
            $this->primary_key => (int) $id
        );

        return $this->delete_data($where);
    }

    // alias for delete_data_by_id
    public function delete_by_id($id, $set_prefix = TRUE)
    {
        return $this->delete_data_by_id($id, $set_prefix);
    }
   
    // alias for delete_data_by_id
    public function delete($id)
    {
        return $this->delete_by_id($id, FALSE);
    }

   
    // --------------------------------------------------------------------
    // HELPER METHODS
    // --------------------------------------------------------------------

   
    public function remove_invalid_columns($data = [])
    {
        if (empty($data)) {
            return [];
        }

        $valid_data = [];
        
        foreach ($data as $key => $value) {
            if (in_array($key, static::$valid_columns)) {
                $valid_data[$key] = $value;
            }
        }

        return $valid_data;
    }

    public function get_data_or_false($query)
    {
        return ($query->num_rows() > 0) ? $query->result($this->format) : FALSE;
    }

    public function get_first_row_or_false($query)
    {
        return ($query->num_rows() > 0) ? $query->row(0, $this->format) : FALSE;
    }
    
    /**
     *  set the valid fields from the form validation array
     *
     *  @param  array   The form validation array
     *
     *  @return array   The valid fields for this model
     */
    public function get_valid_fields_from_form_validation_rules($form_validation_rules)
    {
        $fields = [];

        foreach ($form_validation_rules as $rule) {
            $fields[] = $rule['field'];
        }
            
        return $fields;
    }

    public function get_model_from_input($method = 'post')
    {
        static $valid_methods = ['post', 'get', 'post_get', 'get_post'];
        $data = [];

        if (empty($this->form_validation) || !in_array($method, $valid_methods, TRUE)) {
            return $data;
        }

        foreach ($this->form_validation as $field) {
            if (!isset($field['field'])) {
                continue;
            }

            $name = $field['field'];

            if (empty($name)) {
                continue;
            }
            
            $data[$name] = $this->input->{$method}($name);
        }

        return $data;
    }

    /**
     * Converts form_validation fields into an array of data usable by CI form_helpers
     *
     * @param   array   $values     Optional, associative array of values to populate the form fields with (e.g. [id => values])
     *
     * @return  array[]             Array of associated arrays containing form field information usable by CI form_helpers
     */
    public function get_form_fields($values = [])
    {
        $data = [];

        if (empty($this->form_validation)) {
            return $data;
        }

        foreach ($this->form_validation as $validation) {
            $field = $validation;

            // turn 'field' into id/name
            $field['id'] = $field['field'];
            $field['name'] = $field['field'];
            unset($field['field']);

            // lookup label if its a language key
            if (isset($field['label']) && strpos($field['label'], 'lang:') !== FALSE) {
                $field['label'] = lang(substr($field['label'], 5));
            }

            // if a label wasn't supplied, make one out of the ID
            if (!isset($field['label'])) {
                $field['label'] = ucwords(str_replace('_', ' ', $field['id']));
            }

            // if there is an array of display properties, merge them in
            if (isset($field['display']) && is_array($field['display'])) {
                $field += $field['display'];
                unset($field['display']);
            }

            // if the field is a dropdown, populate its lookup data
            if (isset($field['type']) && $field['type'] == 'dropdown') {
                $model = $field['lookup_model'];

                $this->load->model($model);
                $lookup = $this->{$model}->get_all_for_dropdown($field['lookup_display']);

                // by default, add an empty selection to top of dropdown lookup data
                // behavior can be opted out of by setting the display property 'lookup_no_default' to TRUE
                if (!isset($field['lookup_no_default']) || $field['lookup_no_default'] !== TRUE) {
                    // if we were to array_merge instead, the index #s would reset and wreck stuff
                    $lookup = array('' => '') + $lookup;
                }

                $field['lookup'] = $lookup;
            }

            // determine if field is required so UI can render it differently
            $field['required'] = (isset($field['rules']) && strpos($field['rules'], 'required') !== FALSE);

            // set the value, if supplied, otherwise NULL
            $field['value'] = (is_array($values) && isset($values[$field['id']])) ? $values[$field['id']] : NULL;

            $data[$field['id']] = $field;
        }

        return $data;
    }

    public function set_total_rows($total_rows)
    {
        $this->total_rows = $total_rows;
        $this->total_pages = ceil($total_rows / $this->page_size) ?? 1;
    }

    // used to trigger events and initiate observer callbacks
    protected function trigger($event, $data = [])
    {
        if (isset($this->$event) && is_array($this->$event)) {
            foreach ($this->$event as $method) {
                $data = call_user_func_array(array($this, $method), array($data));
            }
        }

        return $data;
    }
    
    // --------------------------------------------------------------------
}
