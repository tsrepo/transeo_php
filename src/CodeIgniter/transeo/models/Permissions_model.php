<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions_model extends Transeo\CodeIgniter\Model
{
    const TABLE_NAME = 'permissions';
    
    public $db_table = self::TABLE_NAME;
    public $primary_key = 'id';

    public static $valid_columns  = [
        'id',
        'key',
        'description'
    ];

    public function get_permissions_for_user($user_id = NULL)
    {
        $query = $this->db
                        ->select('permissions.key')
                        ->from('permissions')
                        ->join('groups_permissions', 'groups_permissions.permission_id = permissions.id')
                        ->join('users_groups', 'users_groups.group_id = groups_permissions.group_id', 'left')
                        ->join('groups', 'groups.id = groups_permissions.group_id')
                        ->group_start()
                            ->where('users_groups.user_id', $user_id)
                            ->or_where('groups.name', 'anonymous')
                        ->group_end()
                        ->get();

        //echo $this->db->last_query();

        if ($query->num_rows() <= 0) {
            return NULL;
        }

        return array_flip(array_column($query->result('array'), 'key'));
    }
}
