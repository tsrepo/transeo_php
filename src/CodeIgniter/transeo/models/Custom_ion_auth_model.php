<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Ion Auth but with support to login without sessions using 
*
*/

class Custom_ion_auth_model extends Ion_auth_model
{
	/**
	 * The logged in api user
	 * 
	 * @var object
	 */
	public $api_user;

	/**
     * Overwrite login so that if $remember == 'api' it will not use session and will return a refresh token.
	 *
	 * @return bool|string
	 **/
	public function login($identity, $password, $remember=FALSE)
	{
		$this->trigger_events('pre_login');

		if (empty($identity) || empty($password))
		{
			$this->set_error('login_unsuccessful');
			return FALSE;
		}

		$this->trigger_events('extra_where');

		$query = $this->db->select($this->identity_column . ', email, id, password, active, last_login')
		                  ->where($this->identity_column, $identity)
		                  ->limit(1)
		    			  ->order_by('id', 'desc')
		                  ->get($this->tables['users']);

		if($this->is_time_locked_out($identity))
		{
			// Hash something anyway, just to take up time
			$this->hash_password($password);

			$this->trigger_events('post_login_unsuccessful');
			$this->set_error('login_timeout');

			return FALSE;
		}

		if ($query->num_rows() === 1)
		{
			$user = $query->row();

			$password = $this->hash_password_db($user->id, $password);

			if ($password === TRUE)
			{
				if ($user->active == 0)
				{
					$this->trigger_events('post_login_unsuccessful');
					$this->set_error('login_unsuccessful_not_active');

					return FALSE;
				}

                if ($remember === 'api') {
                    // set the api_user instead of session
                    $this->api_user = $user;
                }
                else {
                    $this->set_session($user);
                }

				$this->update_last_login($user->id);

				$this->clear_login_attempts($identity);

				if ($remember && $remember !== 'api' && $this->config->item('remember_users', 'ion_auth'))
				{
					$this->remember_user($user->id);
				}

				$this->trigger_events(array('post_login', 'post_login_successful'));
				$this->set_message('login_successful');

                if ($remember === 'api') {
                    // set refresh token to remember user
                    return $this->api_set_refresh_token($user->id);
                }

				return TRUE;
			}
		}

		// Hash something anyway, just to take up time
		$this->hash_password($password);

		$this->increase_login_attempts($identity);

		$this->trigger_events('post_login_unsuccessful');
		$this->set_error('login_unsuccessful');

		return FALSE;
	}

	/**
	 * Log the user in from the given refresh token
	 *
	 * @return bool
	 **/
	public function api_login_by_refresh_token($identity, $refresh_token)
	{
		// check for valid data
		if (empty($refresh_token)) {
			return FALSE;
		}
		
		// get the user
		$query = $this->db->select($this->identity_column.',id, email, active, last_login')
		                  ->where($this->identity_column, $identity)
		                  ->limit(1)
		                  ->get($this->tables['users']);

		// if the user was found, check the token
		if ($query->num_rows() == 1)
		{
			$user = $query->row();

			if ($user->active == 0)
			{
				$this->set_error('login_unsuccessful_not_active');

				return FALSE;
			}

			if (!$this->hash_refresh_token_db($user->id, $refresh_token)) {

				return FALSE;
			}

			$this->update_last_login($user->id);

			// instead of setting the session, set the api user.
			$this->api_user = $user;

			return TRUE;
		}

		return FALSE;
	}
    
	/**
	 * Set the refresh token for this user
	 *
	 * @return string
	 **/
	public function api_set_refresh_token($id)
	{
		if (!$id)
		{
			return FALSE;
		}

		$user = $this->user($id)->row();

		if (!$user) {
			return FALSE;
		}

		$refresh_token = $this->refresh_token();
		
		$query = $this->db->select('id')
		                  ->where('user_id', $id)
		    			  ->order_by('created_dtm', 'asc')
		                  ->get($this->tables['users_refresh_tokens']);
		
		// limit number of refresh tokens
		if ($query->num_rows() >= $this->config->item('api')['max_refresh_tokens']) {
			$row = $query->row();
			$this->db->delete($this->tables['users_refresh_tokens'], array('id' => $row->id));
		}

		$salt = $this->store_salt ? $this->salt() : FALSE;
		$this->db->insert($this->tables['users_refresh_tokens'], [
			'user_id'       => $id,
			'refresh_token' => $this->hash_password($refresh_token, $salt)
		]);
		
		return $refresh_token;
	}

	/**
	 * This function takes a refresh_token and validates it
	 * against an entry in the users table.
	 *
	 * @return bool
	 **/
	public function hash_refresh_token_db($id, $refresh_token)
	{
		if (empty($id) || empty($refresh_token))
		{
			return FALSE;
		}

		$tbl_name_urt = $this->tables['users_refresh_tokens'];
		$query = $this->db->select('refresh_token, salt')
						  ->join($tbl_name_urt, $tbl_name_urt.'.user_id = ' . $this->tables['users'].'.id', 'INNER JOIN')
		                  ->where($this->tables['users'].'.id', $id)
		                  ->get($this->tables['users']);

		if ($query->num_rows() == 0)
		{
			return FALSE;
		}

		foreach ($query->result() as $row) {
			if ($this->bcrypt->verify($refresh_token,$row->refresh_token))
			{
				return TRUE;
			}
		}

		return FALSE;
	}

    /**
     * Return a random string
     */
	public function refresh_token()
	{
		$bytes = random_bytes(20);
		return bin2hex($bytes);
	}

    /**
     * If api_user is set, the expiration time should've been checked when the token was checked.  so skip.
     * Otherwise, call the original function.
     */
    public function recheck_session()
    {
		if (!empty($this->api_user)) {
			return (bool) $this->api_user->id;
		}

        return parent::recheck_session();
    }

	public function set_api_user($identity)
	{
		if (empty($identity)) {
			$this->api_user = NULL;
			return FALSE;
		}

		$query = $this->db->select($this->identity_column.',id, email, last_login')
		                  ->where($this->identity_column, $identity)
		                  ->limit(1)
		                  ->get($this->tables['users']);

		if ($query->num_rows() == 1)
		{
			$user = $query->row();

			$this->api_user = $user;

			return TRUE;
		}

		return FALSE;
	}
}