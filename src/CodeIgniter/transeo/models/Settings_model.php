<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends Transeo\CodeIgniter\Model
{
    const TABLE_NAME = 'settings';
    public $db_table = self::TABLE_NAME;
    public $primary_key = 'id';

    protected static $cached_results = [];

    public static $valid_columns  = [
		'scope',
        'scope_id',
		'key',
		'value',
		'updated_dtm',
    ];

    public function get($scope, $scope_id, $key)
    {
        $cache_key = $this->create_cache_key(__FUNCTION__, func_get_args());
        if (isset(self::$cached_results[$cache_key])) {
            return self::$cached_results[$cache_key];
        }

        $query = $this->db
                    ->from($this->db_table)
                    ->where('scope', $scope)
                    ->where_in('scope_id', is_array($scope_id) ? $scope_id : [$scope_id])
                    ->where('key', $key)
                    ->get();

        $this->format = 'array';

        $data = $this->get_data_or_false($query);
        self::$cached_results[$cache_key] = $data;
        return $data;
    }

    public function get_set($scope, $scope_id, $key)
    {
        $cache_key = $this->create_cache_key(__FUNCTION__, func_get_args());
        if (isset(self::$cached_results[$cache_key])) {
            return self::$cached_results[$cache_key];
        }
        $query = $this->db
                    ->from($this->db_table)
                    ->where('scope', $scope)
                    ->where_in('scope_id', is_array($scope_id) ? $scope_id : [$scope_id])
                    ->like('key', $key, 'AFTER')
                    ->get();

        $this->format = 'array';

        $data = $this->get_data_or_false($query);
        self::$cached_results[$cache_key] = $data;
        return $data;
    }

    public function get_global($key)
    {
        return $this->get('global', 0, $key);
    }

    public function get_global_set($key)
    {
        return $this->get_set('global', 0, $key);
    }

    public function get_value($scope, $scope_id, $key)
    {
        $results = $this->get($scope, $scope_id, $key);
        if ($results === FALSE) {
            return FALSE;
        }

        $results = reset($results);
        return $results['value'];
    }

    public function get_global_value($key)
    {
        return $this->get_value('global', 0, $key);
    }

    public function get_by_params($params = [], $format = 'object')
    {
        $this->db->from($this->db_table);

        if (!empty($params['scope_data']) && is_array($params['scope_data'])) {
            foreach ($params['scope_data'] as $scope => $scope_id) {
                $this->db->or_group_start();
                    $this->db->where('scope', $scope);
                    $this->db->where('scope_id', $scope_id);
                $this->db->group_end();
            }
        }

        if (!empty($params['scope'])) {
            $this->db->where_in('scope', is_array($params['scope']) ? $params['scope'] : [$params['scope']]);
        }

        if (!empty($params['scope_id']) && empty($params['scope_data'])) {
            $this->db->where_in('scope_id', is_array($params['scope_id']) ? $params['scope_id'] : [$params['scope_id']]);
        }

        if (!empty($params['key_startswith'])) {
            if (is_array($params['key_startswith'])) {
                foreach ($params['key_startswith'] as $idx => $key) {
                    if ($idx === 0) {
                        $this->db->like('key', $key, 'AFTER');
                    }
                    else {
                        $this->db->or_like('key', $key, 'AFTER');
                    }
                }
            }
            else {
                $this->db->like('key', $params['key_startswith'], 'AFTER');
            }
        }

        $query = $this->db->get();

        $this->format = $format;
        return $this->get_data_or_false($query);
    }

    public function set($scope, $scope_id, $key, $value)
    {
        $exists = $this->get_value($scope, $scope_id, $key);

        ;

        if ($exists === FALSE) {
            $this->insert([
                'scope'     => $scope,
                'scope_id'  => $scope_id,
                'key'       => $key,
                'value'     => $value,
            ]);
        }
        else {
            $this->db
                ->where('scope', $scope)
                ->where_in('scope_id', is_array($scope_id) ? $scope_id : [$scope_id])
                ->where('key', $key)
                ->set('value', $value)
                ->set('updated_dtm', date('Y-m-d H:i:s'))
                ->update(self::TABLE_NAME)
            ;
        }
        // clear only what we set
        $cache_key = $this->create_cache_key('get', [$scope, $scope_id, $key]);
        unset(self::$cached_results[$cache_key]);
        $cache_key = $this->create_cache_key('get_set', [$scope, $scope_id, $key]);
        unset(self::$cached_results[$cache_key]);
    }

    public function set_global($key, $value)
    {
        return $this->set('global', 0, $key, $value);
    }

    protected function create_cache_key($function_name, $params)
    {
        return $function_name . '-' . json_encode($params);
    }

}
