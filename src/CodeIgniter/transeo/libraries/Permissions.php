<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions extends Transeo\CodeIgniter\Library
{
    public function __construct()
    {
        $this->load->model('permissions_model');
        $this->load->library('ion_auth');
    }

    public function has_permission($permission_key)
    {
        // permission key for entire controller that we will check as fallback
        $controller_key = explode('__', $permission_key)[0];

        $user_id = $this->ion_auth->logged_in() ? $this->ion_auth->user()->row()->id : NULL;
        
        $permissions = $this->permissions_model->get_permissions_for_user($user_id);
        if (is_null($permissions) || empty($permissions)) {
            return FALSE;
        }

        return array_key_exists($permission_key, $permissions) || array_key_exists($controller_key, $permissions);
    }

    public function in_group($group)
    {
        $groups = array_flip(array_column($this->ion_auth->get_users_groups()->result('array'), 'name'));

        if (empty($group) || empty($groups)) {
            return FALSE;
        }
        
        return array_key_exists($group, $groups) || array_key_exists('admin', $groups);
    }
}
