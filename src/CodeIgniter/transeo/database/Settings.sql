DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`scope` varchar(50) NOT NULL,
	`scope_id` varchar(50) NOT NULL,
	`key` varchar(100) NOT NULL,
	`value` TEXT NOT NULL,
	`updated_dtm` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`),
	INDEX `key` (`key` ASC),
	INDEX `scope` (`scope` ASC)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `settings` 
    (`scope`, `scope_id`, `key`, `value`)
VALUES
     ('group', 'admin', 'feature', 'dashboard-main'),
     ('group', 'admin', 'feature', 'api')
 ;
