<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

use Aws\Ses\SesClient;
use Google\Client;
use Google\Service\Gmail;

/**
 * Extends CI_Email adding a protocol of "amazon" to send email via Amazon SES Web API
 *
 * HOW TO USE:
 * 1) Create a MY_Email.php in your application/libraries folder
 * 2) Have MY_Email.php inherit from this \Transeo\CodeIgniter\Email
 * 3) Modify your /config/email.php to set $config['protocol'] = 'amazon';
 * 4) Modify your /config/email.php to add following AWS SES config entries w/ their real values
 *      $config['aws_key'] = '';
 *      $config['aws_secret'] = '';
 * 5) You may also optionally supply aws_region, aws_version and aws_debug configuration values as well
 * 
 * To use Google for emails:
 * 1) Modify your /config.php to add;
 *     $config['google_api_creds'] = [ ... JSON file in PHP array format ... ]
 *     $config['email_from'] = <the_email_you_want_to_send_as>;
 *     $config['google_user_id'] = $config['email_from'];
 * 2) Modify your /config/email.php to add:
 *     $CI =& get_instance();
 *     $config['google_api_creds'] = $CI->config->item('google_api_creds');
 *     $config['google_user_id'] = $CI->config->item('google_user_id');
 * 3) You can use either a service account or user-grant access.
 */
class Email extends \CI_Email
{
    // override defaults adding amazon as an allowed protocol
    protected $_protocols = ['mail', 'sendmail', 'smtp', 'amazon', 'google'];

    // our custom configuration items
    private $aws_key;
    private $aws_secret;
    private $aws_region = 'us-east-1';
    private $aws_version = '2010-12-01';
    private $aws_debug = FALSE;

    protected $access_token;
    protected $google_api_creds;

    protected $api_response;

    protected $thread_id;
    
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        if (isset($config['aws_key'])) {
            $this->aws_key = $config['aws_key'];
        }

        if (isset($config['aws_secret'])) {
            $this->aws_secret = $config['aws_secret'];
        }

        if (isset($config['aws_region'])) {
            $this->aws_region = $config['aws_region'];
        }

        if (isset($config['aws_version'])) {
            $this->aws_version = $config['aws_version'];
        }

        if (isset($config['aws_debug'])) {
            $this->aws_debug = $config['aws_debug'];
        }

        if (isset($config['google_api_creds'])) {
            $this->google_api_creds = $config['google_api_creds'];
        }
        if (isset($config['google_user_id'])) {
            $this->google_user_id = $config['google_user_id'];
        }
    }

    public function set_access_token($access_token)
    {
        $this->access_token = $access_token;
    }

    public function get_api_response()
    {
        $protocol = $this->_get_protocol();
        if ($protocol == 'google') {
            return [
                'id'        => $this->api_response->getId(),
                'historyId' => $this->api_response->getHistoryId(),
                'labelIds'  => $this->api_response->getLabelIds(),
                'threadid'  => $this->api_response->getThreadId()
            ];
        }

        return $this->api_response;
    }

    public function set_access_token_with_refresh($access_token)
    {
        $this->access_token = $access_token;
        $protocol = $this->_get_protocol();
        if ($protocol == 'google') {
            $client = new Client();
            $client->setAuthConfig($this->google_api_creds);
            $client->setAccessToken($access_token);
            if ($client->isAccessTokenExpired()) {
                $new_access_token = $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                if (!empty($new_access_token['access_token']) && empty($new_access_token['error']) && !empty($new_access_token['refresh_token'])) {
                    $this->access_token = json_encode($new_access_token);
                }
                else {
                    log_message('error', 'Google gmail fetchAccessTokenWithRefreshToken error: ' . $new_access_token['error'] ?? 'unknown');
                    $this->_set_error_message('Google gmail fetchAccessTokenWithRefreshToken error:' . $new_access_token['error'] ?? 'unknown');
                    return FALSE;
                }
            }
        }
        return $this->access_token;
    }

    public function set_thread_id($thread_id)
    {
        $this->thread_id = $thread_id;
    }

    // inner workings of CI_Email dynamically calls function "_send_with_{$protocol}"
    protected function _send_with_amazon()
    {
        if (empty($this->aws_key) || empty($this->aws_secret)) {
            log_message('error', 'AWS SES config key or secret missing from config/email.php');
            $this->_set_error_message('AWS SES config key or secret missing from config/email.php');
            return FALSE;
        }

        $client = new SesClient([
            'debug' => $this->aws_debug,
            'credentials' => [
                'key' => $this->aws_key,
                'secret' => $this->aws_secret
            ],
            'region' => $this->aws_region,
            'version' => $this->aws_version
        ]);

        $subject = !empty($this->_headers['Subject']) ? $this->_headers['Subject'] : $this->_subject;

        // see more optional params @ http://docs.aws.amazon.com/aws-sdk-php/v3/api/api-email-2010-12-01.html#sendemail
        $message = [
            'Source' => $this->_headers['From'],

            'Destination' => [
                'ToAddresses' => $this->_recipients
            ],

            'Message' => [
                'Subject' => [
                    'Data' => $subject,
                    'Charset' => $this->charset,
                ],

                'Body' => []
            ]
        ];

        if (isset($this->_headers['Bcc'])) {
            $message['Destination']['BccAddresses'] = explode(', ', $this->_headers['Bcc']);
        }

        // for plain text, just use the body
        if ($this->_get_content_type() == 'plain') {
            $message['Message']['Body'] = [
                'Text' => [
                    'Data' => $this->_body,
                    'Charset' => $this->charset,
                ]
            ];
        }

        // for html emails, just use body for html and alt message for text
        elseif ($this->_get_content_type() == 'html') {
            $message['Message']['Body'] = [
                'Text' => [
                    'Data' => $this->_get_alt_message(),
                    'Charset' => $this->charset,
                ],
                'Html' => [
                    'Data' => $this->_body,
                    'Charset' => $this->charset,
                ]
            ];
        }
        else {
            unset($message['Message']);
            $message['RawMessage']['Data'] = $this->_header_str . $this->_finalbody;
        }

        // FYI, Amazon SES limits are:
        //      email cannot exceed 10MB
        //      # of recipients (to + cc + bcc) cannot exceed 50
        //      each individual recipient (to + cc + bcc) contributes toward SES daily access limits
        if ($this->_get_content_type() == 'html-attach') {
            try {
                $this->api_response = $client->SendRawEmail($message);
            }
            catch (\Exception $ex) {
                log_message('error', 'AWS SES sendEmail error: ' . $ex->getMessage());
                $this->_set_error_message('AWS SES sendEmail error:' . $ex->getMessage());
                return FALSE;
            }

        }
        else {
            try {
                $result = $client->sendEmail($message);
            }
            catch (\Exception $ex) {
                log_message('error', 'AWS SES sendEmail error: ' . $ex->getMessage());
                $this->_set_error_message('AWS SES sendEmail error:' . $ex->getMessage());
                return FALSE;
            }
        }
        
        // $result is an assoc. array containing a MessageId, a unique message identifier
        // log it for debugging purposes incase we want to cross-ref w/ AWS SES console
        log_message('info', 'Sent AWS SES Email Message ' . print_r($message, TRUE));

        return TRUE;
    }

    // inner workings of CI_Email dynamically calls function "_send_with_{$protocol}"
    protected function _send_with_google()
    {
        $client = $this->get_client();

        if (empty($client)) {
            return FALSE;
        }
        
        $service = new Gmail($client);
        $message = new Gmail\Message();
        $rawMessageString = $this->_header_str . $this->_finalbody;
        $rawMessage = rtrim(strtr(base64_encode($rawMessageString), '+/', '-_'), '=');;
        $message->setRaw($rawMessage);

        if (!empty($this->thread_id)) {
            $message->setThreadId($this->thread_id);
        }

        try {
            $this->api_response = $service->users_messages->send($this->google_user_id, $message);
        }
        catch (Exception $e) {
            log_message('error', 'Gmail send error: ' . $e->getMessage());
            $this->_set_error_message('Gmail send error:' . $e->getMessage());
            return FALSE;
        }
        
        // $result is an assoc. array containing a MessageId, a unique message identifier
        // log it for debugging purposes incase we want to cross-ref w/ AWS SES console
        log_message('info', 'Sent Gmail Email Message ' . print_r($message, TRUE));

        return TRUE;

        
    }

    protected function get_client()
    {
        if ($this->protocol == 'google') {
            if (empty($this->google_api_creds)) {
                log_message('error', 'Google credentials are missing from config/email.php');
                $this->_set_error_message('Google credentials are missing from config/email.php');
                return NULL;
            }

            $client = new Client();
            $client->setAuthConfig($this->google_api_creds);
            $client->setScopes([
                Gmail::GMAIL_READONLY,
                Gmail::GMAIL_SEND
            ]);
            $client->setIncludeGrantedScopes(TRUE);

            if (($this->google_api_creds['type'] ?? '') == 'service_account') {
                $client->setSubject($this->google_user_id);
            }
            else {
                $client->setAccessToken($this->access_token);
            }

            return $client;
        }

        return NULL;

    }
}
