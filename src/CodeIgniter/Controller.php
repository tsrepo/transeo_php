<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends \CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        // allow query string variable to enable profiler
        if ($this->input->get('debug')) {
            if (empty($this->config->item('debug_key')) || $this->input->get('debug') == $this->config->item('debug_key')) {
                $this->output->enable_profiler(TRUE);
                error_reporting(-1);
                ini_set('display_errors', 1);
                defined('DEBUG') OR define('DEBUG', TRUE);
            }
        }
        else {
            defined('DEBUG') OR define('DEBUG', FALSE);
        }

        // some apache seems to not recognize that this is on by default, so we wrap ALL output in another buffer
        ob_start();
    }

    public function __destruct()
    {
        // some apache seems to not recognize that this is on by default, so we wrap ALL output in another buffer
        if (ob_get_level() > 0) {
            ob_flush();
            ob_end_flush();
        }
    }

    public function is_get()
    {
        return $this->input->method(TRUE) === 'GET';
    }

    public function is_post()
    {
        return $this->input->method(TRUE) === 'POST';
    }

    public function is_cli_request()
    {
        return $this->input->is_cli_request() || PHP_SAPI === 'cli' || !isset($_SERVER['HTTP_HOST']);
    }
    
    /**
     * Method to return JSON (or JSONP) to client
     *
     * @param array|object $data Data that will be returned
     */
    public function render_json($data)
    {
        header('Content-Type: application/json');
        $json = json_encode($data);
        if (!empty($this->input->get('callback'))) {
            echo $this->input->get('callback').'('.$json.');';
        } else {
            echo $json;
        }
        
        exit;
    }

    public function _remap($method, $params = array()) // @codingStandardsIgnoreLine
    {
        // remap request to post method if one exists
        if ($this->is_post() && method_exists($this, $method.'_post'))
        {
            $method = $method.'_post';
        }

        // auto map /controller/{id} to /controler/details/{id} to make things RESTy
        if (count($params) === 0 && is_numeric($method) && method_exists($this, 'details')) {
            $params[] = $method;
            $method = 'details';
        }

        if (!method_exists($this, $method))
        {
            show_404();
        }

        return call_user_func_array(array($this, $method), $params);
    }

    // ---------------------------------------------------------------

    /**
     * Method to start an event stream
     */
    function _eventstream_start()
    {
        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');
        
        if (ob_get_level() > 0) {
            ob_flush();
            ob_end_flush();
        }
        flush();
    }

    /**
     * Method to finish an event stream
     */
    function _eventstream_end($message = NULL, $data = array())
    {
        $this->_eventstream_progress($message, $data, 'complete');
    }

    /**
     * Method to send a progress notification for an event stream
     */
    function _eventstream_progress($message = NULL, $data = array(), $type = 'progress')
    {
        echo 'id: ' . microtime() . PHP_EOL;
        echo 'data: ' . json_encode(array(
                    'event'     => $type,
                    'message'   => $message,
                    'data'      => $data,
        )) . PHP_EOL;
        echo PHP_EOL;
    
        if (ob_get_level() > 0) {
            ob_flush();
            ob_end_flush();
        }
        flush();
    }
}
