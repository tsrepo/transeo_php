<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

use Monolog\Logger;
use Monolog\ErrorHandler;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\ChromePHPHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Processor\WebProcessor;

// Extends default CI_LOG to call Monolog logging library to support file and syslogup logging
class Log extends \CI_Log
{
    protected static $logs = [];

    protected $levels = [
        'OFF' => '0',
        'ERROR' => '1',
        'DEBUG' => '2',
        'INFO' => '3',
        'ALL' => '4'
    ];

    protected $config = [];
    protected $monolog = NULL;

    public function __construct()
    {
        $this->config =& get_config();
        
        // if there are no monolog handlers configured, just use the built-in CI logging
        if (empty($this->config['monolog_handlers'])) {
            parent::__construct();
            return;
        }

        // init monologger
        $this->monolog = new Logger($this->config['monolog_channel']);

        // detect and register all PHP errors in this log hence forth
        //ErrorHandler::register($this->monolog);

        if ($this->config['monolog_introspection_processor']) {
            // add controller and line number info to each log message
            // 2 = depth in the stacktrace to ignore. This gives us the file
            // making the call to log_message();
            $this->monolog->pushProcessor(new IntrospectionProcessor(Logger::DEBUG, [], 2));
        }

        if (!empty($this->config['monolog_web_processor']) && $this->config['monolog_web_processor']) {
            // add controller and line number info to each log message
            // 2 = depth in the stacktrace to ignore. This gives us the file
            // making the call to log_message();
            $this->monolog->pushProcessor(new WebProcessor(NULL, ['url' => 'REQUEST_URI']));
        }
        
        // decide which handler(s) to use
        foreach ($this->config['monolog_handlers'] as $value) {
            switch ($value) {
                case 'chrome':
                    $handler = new ChromePHPHandler();
                    break;
                case 'file':
                    $handler = new RotatingFileHandler(
                        $this->config['monolog_file_logfile'],
                        isset($this->config['monolog_file_maxfiles']) ? $this->config['monolog_file_maxfiles'] : 0
                    );
                    
                    $formatter = new LineFormatter(NULL, NULL, $this->config['monolog_file_multiline']);
                    $formatter->includeStacktraces(!empty($this->config['monolog_file_stacktrace']) && $this->config['monolog_file_stacktrace']);

                    $handler->setFormatter($formatter);
                    
                    break;
                case 'ci_file':
                    $handler = new RotatingFileHandler(
                        $this->config['monolog_ci_file_logfile'],
                        isset($this->config['monolog_ci_file_maxfiles']) ? $this->config['monolog_ci_file_maxfiles'] : 0
                    );
                    
                    $formatter = new LineFormatter("%level_name% - %datetime% --> %message% %extra%\n", NULL, $this->config['monolog_ci_file_multiline']);
                    $formatter->includeStacktraces(!empty($this->config['monolog_ci_file_stacktrace']) && $this->config['monolog_ci_file_stacktrace']);

                    $handler->setFormatter($formatter);
                    
                    break;
                case 'stderr':
                    $handler = new StreamHandler('php://stderr');
                    break;
                case 'syslogudp':
                    $handler = new SyslogUdpHandler(
                        $this->config['monolog_syslogudp_host'],
                        $this->config['monolog_syslogudp_port'],
                        LOG_USER,
                        Logger::DEBUG,
                        TRUE,
                        $this->config['monolog_channel']
                    );

                    // reformat message so the %extra% (web URL, file name, line number, etc) is displayed before the %message%
                    // This way we can see the %extra% info even on large messages that truncate in syslog
                    $formatter = new LineFormatter('%channel%.%level_name%: %extra% %message% %context%', NULL, $this->config['monolog_syslogudp_multiline']);
                    $formatter->includeStacktraces(!empty($this->config['monolog_syslogudp_stacktrace']) && $this->config['monolog_syslogudp_stacktrace']);

                    $handler->setFormatter($formatter);
                    
                    break;
                default:
                    exit('log handler not supported: ' . $value . "\n");
            }

            $this->monolog->pushHandler($handler);
        }
    }

    public function write_log($level, $msg)
    {
        // if no monolog, use the built-in CI logging
        if ($this->monolog === NULL) {
            return parent::write_log($level, $msg);
        }

        $level = strtoupper($level);

        // verify error level
        if (!isset($this->levels[$level])) {
            $this->monolog->error('unknown error level: ' . $level);
            $level = 'ALL';
        }

        // filter out anything in $this->config['exclusion_list']
        if (!empty($this->config['monolog_exclusion_list'])) {
            foreach ($this->config['monolog_exclusion_list'] as $findme) {
                $pos = strpos($msg, $findme);
                if ($pos !== FALSE) {
                    // just exit now - we don't want to log this error
                    return TRUE;
                }
            }
        }

        if ($this->levels[$level] <= $this->config['monolog_threshold']) {
            switch ($level) {
                case 'ERROR':
                    $this->monolog->error($msg);
                    break;
                case 'DEBUG':
                    $this->monolog->debug($msg);
                    break;
                case 'ALL':
                case 'INFO':
                    $this->monolog->info($msg);
                    break;
            }
        }

        return TRUE;
    }

    public static function get_logs()
    {
        return self::$logs;
    }

    // logs can be written even before the CI_Controller is initialized which
    // is required for get_instance() to work properly
    // if (class_exists('CI_Controller')) {
    //     $ci =& get_instance();
        
    //     // if tracing is enabling, keep all messages around in a static array
    //     if (!empty($ci) && property_exists($ci, 'output') && $ci->output->enable_profiler) {
    //         self::$logs[] = ['level' => $level, 'msg' => $msg];
    //     }
    // }
}
