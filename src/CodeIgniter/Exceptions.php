<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') OR exit('No direct script access allowed');

// Override CI_Exceptions behavior and ensure 404s are NOT logged as errors
class Exceptions extends \CI_Exceptions
{
    public function show_404($page = '', $log_error = TRUE)
    {
        parent::show_404($page, FALSE);
    }
}
