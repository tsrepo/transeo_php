<?php 

namespace Transeo\CodeIgniter;

use Transeo\Loggers\CodeIgniterLogger;
use Transeo\Loggers\DatabaseApiLogger;

defined('BASEPATH') OR exit('No direct script access allowed');

/***
 * CodeIgniter library that wraps Payment gateways and adds factory, CI config and logging functionality
 */
class Payments extends Library
{
    // we support the ability to have different gateways for the different payment use cases
    // TODO: something better to allow one gateway handling > 1 use case without creating multiple instances of same class
    private $payment_gateway_name;
    private $payout_gateway_name;
    private $subscription_gateway_name;

    private $payment_gateway;
    private $payout_gateway;
    private $subscription_gateway;

    public function __construct()
    {
        // get short_name of desired gateways
        $this->payment_gateway_name = $this->config->item('payment_gateway');
        $this->payout_gateway_name = $this->config->item('payout_gateway');
        $this->recurring_gateway_name = $this->config->item('recurring_gateway');

        $logger = new CodeIgniterLogger();
        $api_logger = new DatabaseApiLogger();

        // create gateways
        $this->payment_gateway = $this->create_gateway($this->payment_gateway_name);
        $this->payment_gateway->setLogger($logger);
        $this->payment_gateway->setApiLogger($api_logger);

        $this->payout_gateway = $this->create_gateway($this->payout_gateway_name);
        $this->payment_gateway->setLogger($logger);

        $this->recurring_gateway = $this->create_gateway($this->recurring_gateway_name);
        $this->payment_gateway->setLogger($logger);
    }

    public function get_payment_gateway_name()
    {
        return $this->payment_gateway_name;
    }

    private function create_gateway($name)
    {
        // load config for this specific gateway
        // expects $config[$gateway_name] to be an associative array of parameters
        $parameters = $this->config->item($name);

        // locate desired payout gateway's class
        $class = 'Transeo\\Integrations\\Payments\\' . ucfirst($name) . 'Gateway';
        if (!class_exists($class)) {
            throw new \RuntimeException("Class '${class}' not found");
        }

        // construct desired gateway
        $gateway = new $class();

        // initialize gateway w/ parameters
        $gateway->initialize($parameters);

        return $gateway;
    }

    public function do_batch_payout($payments)
    {
        if (!isset($this->payout_gateway) || !$this->payout_gateway->canDoPayout()) {
            $this->set_error('payout_gateway is not set or does not implement PayoutGatewayInterface');
            return FALSE;
        }

        return $this->payout_gateway->doBatchPayout($payments);
    }

    public function create_subscription($request)
    {
        if (!isset($this->recurring_gateway) || !$this->recurring_gateway->canDoRecurring()) {
            $this->set_error('recurring_gateway is not set or does not implement RecurringGatewayInterface');
            return FALSE;
        }

        return $this->recurring_gateway->createSubscription($request);
    }

    public function update_subscription($request)
    {
        if (!isset($this->recurring_gateway) || !$this->recurring_gateway->canDoRecurring()) {
            $this->set_error('recurring_gateway is not set or does not implement RecurringGatewayInterface');
            return FALSE;
        }

        return $this->recurring_gateway->updateSubscription($request);
    }

    public function cancel_subscription($request)
    {
        if (!isset($this->recurring_gateway) || !$this->recurring_gateway->canDoRecurring()) {
            $this->set_error('recurring_gateway is not set or does not implement RecurringGatewayInterface');
            return FALSE;
        }

        return $this->recurring_gateway->cancelSubscription($request);
    }

    public function get_access_token($code)
    {
        return $this->payment_gateway->getAccessToken($code);
    }

    public function get_user_info($access_token)
    {
        return $this->payment_gateway->getUserInfo($access_token);
    }
}
