<?php

// Adds a MD5 hash of the /asset directory to a URL for cache busting purposes
// To be used in combination w/ grunt-cachebuster @ https://github.com/felthy/grunt-cachebuster
// Also requires the following .htaccess rewrite
//      RewriteCond %{REQUEST_FILENAME} !-f
//      RewriteRule ^(assets)[\\\/]([a-fA-F0-9]+)[\\\/](.+)$ $1\/$3 [L]
if (!function_exists('asset_url'))
{
    function asset_url($url)
    {
        static $hash = NULL;

        $url = base_url($url);

        // load hash from grunt-cachebuster generated file if its not cached
        if ($hash === NULL) {
            $file_path = FCPATH . 'assets/cache.php';

            if (!file_exists($file_path)) {
                return $url;
            }
            
            $hashes = include $file_path;

            // the hash for the entire asset directory has an empty key
            $hash = $hashes[''];
        }

        // turn /asset/css/site.css into /asset/MD5HASH/css/site.css for cache busting purposes
        $url = str_replace('assets/', 'assets/' . $hash . '/', $url);

        return $url;
    }
}

// helper that create <link /> tag for a given css
// 1) references either dev assets via webpack dev server or prod assets depending on CI Config
// 2) cache busts in prod by adding md5 hash to URL
// 3) TODO: includes SRI hash on prod URL
// 4) TODO: reference CDN
if (!function_exists('include_css'))
{
    function include_css($url)
    {
        $dev_assets = get_instance()->config->item('dev_assets');

        // if dev assets are configured, don't render any css, it'll get inserted by JS
        if (!empty($dev_assets)) {
            return;
        }

        $asset = get_hashed_url($url);

        return "<link rel=\"stylesheet\" href=\"{$asset}\" />";
    }
}

// helper that create <script /> tag for a given JS
// 1) references either dev assets via webpack dev server or prod assets depending on CI Config
// 2) cache busts in prod by adding md5 hash to URL
// 3) TODO: includes SRI hash on prod URL
// 4) TODO: reference CDN
if (!function_exists('include_js'))
{
    function include_js($url)
    {
        $dev_assets = get_instance()->config->item('dev_assets');

        $asset = get_hashed_url($url);

        // if dev assets are configured, use those
        if (!empty($dev_assets)) {
            $asset = "{$dev_assets}{$url}";
        }

        return "<script src=\"{$asset}\"></script>";
    }
}

if(!function_exists('get_hashed_url'))
{
    function get_hashed_url($url)
    {
        static $manifest = NULL;

        // load hash manifest if it isn't cached already
        if ($manifest === NULL) {
            $file_path = FCPATH . 'assets/manifest.php';

            if (!file_exists($file_path)) {
                return base_url($url);
            }

            $manifest = include $file_path;
        }

        // if manifest has a key for this file, add the corresponding md5 hash to URL
        $key = preg_replace('/assets\/(js|css)\//i', '', $url);

        if (!array_key_exists($key, $manifest)) {
            return base_url($url);
        }

        return base_url('assets' . str_replace('./', '/', $manifest[$key]));
    }
}

// Get full URI of current page since CI's uri->uri_string() function doesn't include the query string
if(!function_exists('full_uri_string'))
{
    function full_uri_string()
    {
        $ci =& get_instance();

        $url = $ci->uri->uri_string();
        $query = $ci->input->server('QUERY_STRING');
        
        return $query ? $url. '?' . $query : $url;
    }
}

if (!function_exists('class_if'))
{
    function class_if($condition, $class, $render_attribute = FALSE)
    {
        if (!$condition) {
            return NULL;
        }

        if ($render_attribute) {
            return 'class="'.$class.'"';
        }

        return $class;
    }
}

if(!function_exists('debug_trace'))
{
    function debug_trace($message = NULL, $force = FALSE)
    {
        if (!class_exists('CI_Controller')) {
            return;
        }

        $ci =& get_instance();
        
        if ($force === FALSE && (empty($ci) || !property_exists($ci, 'output') || !$ci->output->enable_profiler)) {
            return;
        }

        if ($ci->input->is_cli_request() && defined('STDOUT')) {
            fwrite(STDOUT, date('H:m:s:i', time()) . ' - ' . $message . PHP_EOL);
            return;
        }

        echo '<div style="clear:both; font-family:Courier; margin:30px; color:#333; font-size:11px;">';
        echo date('H:m:s:i', time()) . '<br/>';
        $backtrace = debug_backtrace();
        $backtrace  = $backtrace[0];
        //print_r($backtrace);
        echo (empty($backtrace['file'])) ? $backtrace['class'] : $backtrace['file'];
        echo ' / ' . $backtrace['line'] . '<br />';
        echo $message;
        echo '</div>';
    }
}

// for when you want to link to the current URL but want to change or add a query string value without
// altering/removing other query string parameters
if(!function_exists('update_query_string'))
{
    function update_query_string($key, $value)
    {
        $ci =& get_instance();
        $params = $ci->input->get();
        
        if ($params === FALSE)
        {
            $params = array();
        }

        $params[$key] = $value;
        
        return '?' . http_build_query($params);
    }
}

if(!function_exists('date_to_utc'))
{
    function date_to_utc($date, $offset = '+0:00', $format = 'Y-m-d H:i:s')
    {
        if ($timestamp = strtotime($date) && !empty($offset)) {
            $new_date = date('Y-m-d H:i:s', $timestamp);
            $new_date = new \DateTime($date . ' ' . $offset);
            $new_date->setTimezone(new DateTimeZone('UTC'));
            $date = $new_date->format($format);
        }

        return $date;
    }
}

if(!function_exists('date_to_local'))
{
    function date_to_local($date, $offset = '+0:00', $format = 'Y-m-d H:i:s')
    {
        if ($timestamp = strtotime($date) && !empty($offset)) {
            $new_date = date('Y-m-d H:i:s', strtotime($timestamp));
            $new_date = new \DateTime($date);
            $new_date->setTimezone(new DateTimeZone($offset));
            $date = $new_date->format($format);
        }

        return $date;
    }
}

