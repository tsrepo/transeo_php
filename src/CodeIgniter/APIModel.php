<?php

namespace Transeo\CodeIgniter;

defined('BASEPATH') or exit('No direct script access allowed');


// to specify a relationship, the model needs to have a RELATIONSHIP const
// each key in that array is another model this one links to
// the value of the key can be the shared id field or a key/value pair of source->destination
//
// so if this table is customers and links to contacts via customers.contact_id = contacts.id
// RELATIONSHIPS['contacts'] = [contact_id => id]
//
// and if this table is customers and links to contacts via customers.contact_id = contacts.contact_id
// RELATIONSHIPS['contacts'] = contact_id

// and if this table requires a not-inner-join
// RELATIONSHIPS['contacts'] = ['join' => [contact_id => id], 'type' => 'LEFT OUTER']

// auto-expansion must also define the fields in the API_COLUMN_MAP with an alias
// AND something like
//      const API_AUTO_EXPAND = ['contacts_model'];
//      where 'contacts_model' is the model to auto-join to
//      the relationship MUST also be defined
//      and then normal table.field selectors can be used as if it were "expanded"-to
//      auto-expanded tables are joined BEFORE any other joins

trait APIModel
{
    // this maps a database field to the API key according to the API_COLUMN_MAP in the model for a particular row of data ($data)
    // it supports data keys in $data of the table column name or fully qualified as table.column
    // any custom mapping needs to be put in that entity's model
    public static function map_api_response($data, $params = NULL)
    {
        $return = ['type' => static::API_ENTITY_TYPE];
        foreach (static::API_COLUMN_MAP as $api_key => $data_key) {
            if (is_array($data_key) && array_key_exists('alias', $data_key)) {
                $data_key = $data_key['alias'];
            }
            elseif (is_array($data_key)) {
                // TODO
                continue;
            }

            if (!array_key_exists($data_key, $data) && !array_key_exists(static::TABLE_NAME . '.' . $data_key, $data)) {
                continue;
            }

            $return[$api_key] = array_key_exists($data_key, $data) ? $data[$data_key] : $data[static::TABLE_NAME . '.' . $data_key];
        }

        return $return;
    }

    public function api_by_params($params, $no_return = FALSE)
    {
        $order_query_string = FALSE;
        if (!empty($params['order']) && in_array('order_subquery', $params['adjustments'])) {
             $this->api_by_params_query($params, FALSE, TRUE);
             $order_query_string =$this->db->get_compiled_select();
        }

        $this->api_by_params_query($params, $no_return, $order_query_string);

        if ($no_return === TRUE) {
            return;
        }

        $query = $this->db->get();

        $this->format = 'array';

        return $this->get_data_or_false($query);

    }

    protected function api_by_params_query($params, $no_return = FALSE, $order_query_string = FALSE)
    {
        // figure out our select
        if (empty($params['fields'])) {
            // $params['fields'][] = '*';
            $params['fields'][] = static::TABLE_NAME . '.*';
        }
        else {
            // we need the pk of this table for joining on includes, even if the user doesn't request it
            if (is_array($params['fields'])) {
                $pk = (static::TABLE_NAME . '.' . $this->primary_key . ' AS `' . (static::TABLE_NAME . '.' . $this->primary_key) . '`');
                if (!in_array($pk, $params['fields'])) {
                    $params['fields'][] = $pk;
                }
            }
        }

        if ($order_query_string !== TRUE ) {
            // we want the uniqueness to be here unless we hear otherwise
            $this->db
                ->select($params['fields'] ?? NULL, FALSE)
                ->from($this->db_table)
            ;
        }
        else {
            // when ordering, we only want the id
            $this->db
                ->distinct()
                ->select($this->db_table . '.id')
                ->from($this->db_table)
            ;
        }

        if (!isset($params['group'])) {
            $this->db
                ->group_by($this->db_table . '.id');
        }
        elseif (!empty($params['group'])) {
            $this->db
                ->group_by($params['group']);
        }

        // we might auto-expand some tables, like with products+product_manual_data
        // and if we do, it might be an outer join, so we need to re-order the select list
        if (defined('static::API_AUTO_EXPAND') && !empty(static::API_AUTO_EXPAND)) {
            $joins = array_map(
                function($value) { return ucfirst($value); },
                array_merge(static::API_AUTO_EXPAND, $params['joins'] ?? [])
            );
            $params['joins'] = array_unique($joins);
        }

        if (!empty($params['joins'])) {
            foreach ($params['joins'] as $join) {
                $join = strtolower($join);
                if (array_key_exists($join, self::RELATIONSHIPS)) {
                    $ucjoin = ucfirst($join);
                    $join_table = ucfirst($join)::TABLE_NAME;
                    $join_ons = [];
                    $join_type = 'INNER';

                    if (is_array(self::RELATIONSHIPS[$join])) {
                        $join_relationships = self::RELATIONSHIPS[$join]['join'] ?? self::RELATIONSHIPS[$join];
                        foreach ($join_relationships as $table_col => $join_table_col) {
                            if (strpos($table_col, '.') === FALSE) {
                                $table_col = $this->db_table . '.' . $table_col;
                            }
                            $join_ons[] = $table_col . ' = ' . $join_table . '.' . $join_table_col;
                        }

                        // outer joins need love, too
                        if (array_key_exists('type', self::RELATIONSHIPS[$join])) {
                            $join_type = self::RELATIONSHIPS[$join]['type'];
                        }
                    }
                    else {
                        $join_ons[] = $this->db_table . '.' . self::RELATIONSHIPS[$join] . '=' . $join_table . '.' . self::RELATIONSHIPS[$join];
                    }

                    // support multi-level joins where the source is not the root table
                    // i.e. ability to go from reservation -> person -> contact from reservation
                    // #TODO think about multi-level expansion natively

                    $this->db->join(
                        $join_table,
                        implode(' AND ', $join_ons),
                        $join_type
                    );
                }
            }
        }

        if (!empty($params['filters'])) {
            foreach ($params['filters'] as $filter) {
                if (substr($filter['operator'], 0, 4) == 'null') {
                    $filter['operator'] = substr($filter['operator'], 4);
                    $default = is_array($filter['value']) ? reset($filter['value']) : $filter['value'];
                    $filter['field'] = 'IFNULL(' . $filter['field'] . ", '" . $default . "')";
                }
        
                // this needs to type-check, or else a FALSE (0) will == 'null', which is no good
                if ($filter['value'] === 'null') {
                    $clause = ($filter['operator'] == '!=') ? ' IS NOT ' : ' IS ';
                    $this->db->where($filter['field'] . $clause . 'NULL');
                }
                elseif ($filter['operator'] == 'in') {
                    $val = is_array($filter['value']) ? $filter['value'] : explode(',', $filter['value']);
                    $this->db->where_in($filter['field'], $val);

                }
                elseif ($filter['operator'] == 'or_like') {
                    $this->db->or_like($filter['field'], $filter['value']);
                }
                elseif ($filter['operator'] == 'like') {
                    $this->db->like($filter['field'], $filter['value']);
                }
                elseif ($filter['operator'] == 'or_where') {
                    $this->db->or_where($filter['field'], $filter['value']);
                }
                else {
                    $this->db->where($filter['field'] . ' ' . $filter['operator'], $filter['value']);
                }
            }
        }

        if ($no_return === TRUE) {
            return;
        }

        if (!empty($params['limit'])) {
            $this->db->limit($params['limit']);
        }
        if (!empty($params['offset'])) {
            $this->db->offset($params['offset']);
        }
        if (!empty($params['order'])) {
            if (is_bool($order_query_string)) {
                if (!is_array($params['order'])) {
                    $params['order'] = [$params['order']];
                }
                
                foreach ($params['order'] as $order) {
                    $this->db->order_by($order[0], $order[1], FALSE);
                }
            }
            else {
                // we have a querystring to handle the ordering.  join it now.
                $this->db->join('(' . $order_query_string . ') as o_query', 'o_query.id = ' . ($this->db_table . '.id'), 'INNER');
            }
        }
    }


    public function api_count_by_params($params)
    {
        unset($params['limit']);
        unset($params['offset']);
        unset($params['order']);
        $params['fields'] = ['COUNT(*) as count'];

        $this->api_by_params($params, TRUE);

        $query = $this->db
            ->get();

        $count = $this->get_data_or_false($query);

        if ($count === FALSE) {
            return 0;
        }

        $count = reset($count);
        return $count['count'];
    }
}