# Transeo PHP Library #

## Requirements ##
* PHP >= 7.0.0 (7.1.0 is okay)
* PHP Extensions
    * php-intl >= 1.0.0
* `composer` command [installed](https://getcomposer.org/doc/00-intro.md)
* `git` setup w/ access to this bitbucket repository

## How to Use ##

If you do not have a composer.json in the root of your application, create it with an empty { } in it
If this is the first time you're using this, you'll need to create BitBucket keys per https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html

Run command below to add this private repository to your composer.json file

```
$ composer config repositories.transeo_php git https://bitbucket.org/tsrepo/transeo_php.git
```

Manually add following lines to composer.json

```
"scripts": {
    "post-install-cmd": [
        "Transeo\\Installer::postInstall"
    ],
    "post-update-cmd": [
        "Transeo\\Installer::postUpdate"
    ]
}
```

Run one of the commands below to `require` our package as a dependency, depending on which branch you want to pull from.


Develop branch
```
$ composer require tsrepo/transeo_php:dev-develop
```

Master branch
```
$ composer require tsrepo/transeo_php:dev-master
```

If the repo has not yet Run composer install to add all of the required composer packages to the repo 
```
$ composer install
```

***

## Development ##

### Code Coverage ###

To generate code coverage, you must have xdebug installed locally. PHPUnit will display the following erorr if xdebug is not installed/configured properly.
 > No code coverage driver is available

### To Install xdebug ###

1. Follow the [xdebug wizard](https://xdebug.org/wizard.php) instructions to download and install the proper version of xdebug

2. In addition to the wizard instructions, ensure the following lines are also in your php.ini
    ```
    [XDebug]
    xdebug.default_enable = 1
    ```
